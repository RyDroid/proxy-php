<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


require_once('kernel.inc.php');

$configuration = SessionCapturedConfigurationGeneral::get();
if($configuration->hasFilePath())
{
    $filePath = $configuration->getFilePath();
    if(!empty($filePath) &&
       file_exists($filePath) &&
       is_readable($filePath) &&
       filesize($filePath) > 0)
    {
        header('Content-type: text/xml');
        echo file_get_contents($configuration->getFilePath());
    }
}
