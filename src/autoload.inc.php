<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


define('ABSOLUTE_PATH', realpath(dirname(__FILE__)) .'/');


class ClassLoader
{
    private static $classPaths = array('include/php/class');
    
    public static function
        getPath(string $name) : string
    {
        foreach(self::$classPaths as $path)
        {
            if($path != '' && $path != '.')
            {
                $path .= '/';
            }
            $path = ABSOLUTE_PATH .'/'. $path . $name .'.class.php';
            if(file_exists($path))
            {
                return $path;
            }
        }
        return '';
    }
    
    public static function
        load(string $name)
    {
        $path = self::getPath($name);
        if($path == null || $path == '')
        {
            return false;
        }
        require_once($path);
        return true;
    }
}


spl_autoload_register('ClassLoader::load');
