<?php require_once('kernel.inc.php'); ?><!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>RyDroid Web Proxy</title>
    
    <link
    	rel="stylesheet" media="all" type="text/css"
    	href="include/css/design.css" />
    <link rel="icon" type="image/png" href="favicon.png" />
  </head>
  
  <body>
    <header>
      <h1>RyDroid Web Proxy</h1>
    </header>
    
    <nav>
      <ul>
        <li><a href="proxy-form.php">Proxy</a></li>
        <?php if(SessionIdentifierUtils::isInitialized()) { ?>
        <li><a href="data.html">Get data</a></li>
        <li><a href="data-raw.php">Get raw data</a></li>
        <li><a href="session-destroy.php?redirect=yes">
          Destroy current session
        </a></li>
        <li><a href="session-reinitialize.php?redirect=yes">
          Create a new session
        </a></li>
        <?php } else if(SessionCapturedConfigurationGeneral::get()
                        ->hasToCreateAutomatically()->isFalse()) { ?>
        <li><a href="session-initialize.php?redirect=yes">
          Create a session
        </a></li>
        <?php } ?>
        <li><a href="configuration.php">Configuration</a></li>
        <?php if(LogsConfigurationGeneral::canBeShown() &&
                 file_exists(LogsConfigurationGeneral::getFilePath()) &&
                 is_readable(LogsConfigurationGeneral::getFilePath()) &&
                 filesize(LogsConfigurationGeneral::getFilePath()) > 0) { ?>
        <li><a href="#logs">Logs</a></li>
        <?php } ?>
        <li><a href="documentation.html">Documentation</a></li>
        <li><a href="credits.html">Credits</a></li>
      </ul>
    </nav>
    
    <hr />
    
    <h2 id="configuration">Configuration</h2>
    
    <?php
    ConfigurationPrinterUtils::printAsHtmlList(new ConfigurationGeneral());
    ?>
    
    <?php if(LogsConfigurationGeneral::canBeShown() &&
             file_exists(LogsConfigurationGeneral::getFilePath()) &&
             is_readable(LogsConfigurationGeneral::getFilePath()) &&
             filesize(LogsConfigurationGeneral::getFilePath()) > 0) { ?>
    <hr />
    <h2 id="logs">Logs</h2>
    <pre><?php
      file_get_contents(LogsConfigurationGeneral::getFilePath());
    ?></pre>
    <?php } ?>
    
    <?php if(UserInterfaceConfigurationGeneral::useClientScripts()) { ?>
    <script type="text/javascript"
            src="include/js/specific/homepage-main.js"></script>
    <?php } ?>
  </body>
</html>
