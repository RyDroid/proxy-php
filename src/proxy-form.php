<?php require_once('kernel.inc.php'); ?><!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Session Web Capture</title>
    
    <link
    	rel="stylesheet" media="all" type="text/css"
    	href="include/css/design.css" />
    <link rel="icon" type="image/png" href="favicon.png" />
  </head>
  
  <body>
    <header>
      <a href="./" title="back">←</a>
      <h1>Session Web Capture</h1>
    </header>
    
    <?php DependenciesUtils::printErrorsAsHtmlUnorderedList(); ?>
    
    <form action="proxy.php" method="get">
      <p>
        <label id="url-to-proxify">URL</label>
        <input type="url" id="url-to-proxify" name="url-to-proxify"
               required="required"
               placeholder="https://example.net/" />
      </p>
      
      <?php if(UserInterfaceConfigurationGeneral::
               showProxyGetParametersConfiguration()) { ?>
      <p>
        <label id="proxy-url">URL of proxy</label>
        <input type="url" id="proxy-url" name="proxy-url"
               placeholder="https://proxy.net/" />
      </p>
      
      <p>
        <label id="proxy-port">Port number of proxy</label>
        <input type="number" id="proxy-port" name="proxy-port"
               min="0" max="65535" step="1" />
      </p>
      <?php } ?>
      
      <input type="submit" value="Go" />
    </form>
    
    <?php DependenciesUtils::printWarningsAsHtmlUnorderedList(); ?>
    <?php
    if(!LoggerUtils::createLogFileIfNeeded())
    {
        echo '<p>No log file!</p>';
    }
    ?>
    
    <hr />
    
    <h2 id="configuration">Configuration</h2>
    
    <?php
    ConfigurationPrinterUtils::printAsHtmlList(new ConfigurationGeneral());
    ?>
  </body>
</html>
