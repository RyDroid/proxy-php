<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class GeneralContentGetterUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        getContentGetterForUrl(string $url) : AbstractContentGetter
    {
        if(CurlContentGetter::canWork() &&
           (UrlProtocolStringUtils::isWeb($url) ||
            StringUtils::startsWith($url, '//')))
        {
            return new CurlContentGetter();
        }
        return new FileContentGetter();
    }
    
    public static function
        get(string $url, HttpHeaders $headers) : ContentRequestResponse
    {
        $getter = self::getContentGetterForUrl($url);
        return $getter->get($url, $headers);
    }
    
    public static function
        getContent(string $url, HttpHeaders $headers) : string
    {
        return self::get($url, $headers)->getContent();
    }
}
