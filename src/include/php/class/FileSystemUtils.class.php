<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class FileSystemUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        getSystemTemporaryDirectory() : string
    {
        if(function_exists('sys_get_temp_dir'))
        {
            return sys_get_temp_dir();
        }
        return '/tmp/';
    }
}
