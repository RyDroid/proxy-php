<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class HttpSessionCapturedConfigurationPrinterUtils
{
    const DNT_LINK = (
        'https://developer.mozilla.org/'.
        'en-US/docs/Web/HTTP/Headers/DNT'
    );
    
    
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        printAsHtmlListItems(
            HttpSessionCapturedConfigurationAbstract $configuration,
            string $linePrefix = ''
        )
    {
        echo '</li>'. PHP_EOL;
        echo $linePrefix ."\t". '<li>Save cookies: ';
        echo OptionnalBooleanUtils::toYesNoUndefined(
            $configuration->hasToSaveCookies()
        );
        echo '</li>'. PHP_EOL;
        echo $linePrefix ."\t". '<li>Save user-agent: ';
        echo OptionnalBooleanUtils::toYesNoUndefined(
            $configuration->hasToSaveUserAgent()
        );
        echo '</li>'. PHP_EOL;
        echo $linePrefix ."\t". '<li>Save ';
        echo '<a href="https://en.wikipedia.org/wiki/HTTP_referer">referer</a>';
        echo ': ';
        echo OptionnalBooleanUtils::toYesNoUndefined(
            $configuration->hasToSaveReferer()
        );
        echo '</li>'. PHP_EOL;
        echo $linePrefix ."\t". '<li>Save language: ';
        echo OptionnalBooleanUtils::toYesNoUndefined(
            $configuration->hasToSaveLanguage()
        );
        echo '</li>'. PHP_EOL;
        echo $linePrefix ."\t". '<li>Save encoding: ';
        echo OptionnalBooleanUtils::toYesNoUndefined(
            $configuration->hasToSaveEncoding()
        );
        echo '</li>'. PHP_EOL;
        echo $linePrefix ."\t". '<li>';
        echo 'Save <a href="https://en.wikipedia.org/wiki/HTTP_ETag">ETag</a>';
        echo ': ';
        echo OptionnalBooleanUtils::toYesNoUndefined(
            $configuration->hasToSaveEtag()
        );
        echo '</li>'. PHP_EOL;
        echo $linePrefix ."\t". '<li>';
        echo 'Save <a href="'. self::DNT_LINK .'">';
        echo '<abbr title="Do Not Track">DNT</abbr>';
        echo '</a>: ';
        echo OptionnalBooleanUtils::toYesNoUndefined(
            $configuration->hasToSaveDoNotTrack()
        );
        echo '</li>'. PHP_EOL;
    }
}
