<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @SuppressWarnings(NumberOfChildren)
 */
class DomDocumentProxifierAttribute
    extends DomDocumentProxifierDecorator
{
    private $xpathElementQueries;
    private $attribute;
    
    
    public function
        __construct(string $attribute,
                    array $xpathElementQueries = null,
                    DomDocumentProxifierAbstract $previousProxy = null)
    {
        parent::__construct($previousProxy);
        $this->attribute           = $attribute;
        $this->xpathElementQueries = $xpathElementQueries;
    }
    
    
    private function
        getXPathQueryWithAttribute($query) : string
    {
        if($query == null)
        {
            return '//[string(@'. $this->attribute .')]';
        }
        return $query .'[string(@'. $this->attribute .')]';
    }
    
    private function
        getXPathQueryOfIndexWithAttribute(int $index) : string
    {
        $query = $this->xpathElementQueries[$index];
        return $this->getXPathQueryWithAttribute($query);
    }
    
    public function
        getXPathQuery() : string
    {
        $nbQueries = count($this->xpathElementQueries);
        if($nbQueries == 0)
        {
            return '';
        }
        
        $resultQuery = $this->getXPathQueryOfIndexWithAttribute(0);
        for($index=1; $index < $nbQueries; ++$index)
        {
            $query = $this->getXPathQueryOfIndexWithAttribute($index);
            $resultQuery .= ' | '. $query;
        }
        return $resultQuery;
    }
    
    public function
        getElements(DOMDocument $document) : DOMNodeList
    {
        $pageXPath = new DOMXPath($document);
        $queryString = $this->getXPathQuery();
        return $pageXPath->query($queryString);
    }
    
    public function
        proxifyElement(DOMElement& $element) : bool
    {
        if($element->hasAttribute($this->attribute))
        {
            $attributeValue = $element->getAttribute($this->attribute);
            if(!empty(trim($attributeValue)))
            {
                $attributeValue = $this->proxifyUrl($attributeValue);
                $element->setAttribute($this->attribute, $attributeValue);
                return true;
            }
        }
        return false;
    }
    
    public function
        proxifyNodeList(DOMNodeList& $elements) : int
    {
        $nbModified = 0;
        foreach($elements as $element)
        {
            if($this->proxifyElement($element))
            {
                ++$nbModified;
            }
        }
        return $nbModified;
    }
    
    public function
        proxifyDocument(DOMDocument $document) : int
    {
        $nbModified = parent::proxifyDocument($document);
        $elements = $this->getElements($document);
        $nbModified += $this->proxifyNodeList($elements);
        return $nbModified;
    }
}
