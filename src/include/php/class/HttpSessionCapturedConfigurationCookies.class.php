<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class HttpSessionCapturedConfigurationCookies
    extends HttpSessionCapturedConfigurationAbstract
{
    public function
        hasToSaveCookies() : OptionnalBoolean
    {
        return SessionCapturedConfigurationCookiesUtils::hasToSaveCookies();
    }
    
    public function
        hasToSaveUserAgent() : OptionnalBoolean
    {
        return SessionCapturedConfigurationCookiesUtils::hasToSaveUserAgent();
    }
    
    public function
        hasToSaveReferer() : OptionnalBoolean
    {
        return SessionCapturedConfigurationCookiesUtils::hasToSaveReferer();
    }
    
    public function
        hasToSaveLanguage() : OptionnalBoolean
    {
        return SessionCapturedConfigurationCookiesUtils::hasToSaveLanguage();
    }
    
    public function
        hasToSaveEncoding() : OptionnalBoolean
    {
        return SessionCapturedConfigurationCookiesUtils::hasToSaveEncoding();
    }
    
    public function
        hasToSaveEtag() : OptionnalBoolean
    {
        return SessionCapturedConfigurationCookiesUtils::hasToSaveEtag();
    }
    
    public function
        hasToSaveDoNotTrack() : OptionnalBoolean
    {
        return SessionCapturedConfigurationCookiesUtils::hasToSaveDoNotTrack();
    }
}
