<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @SuppressWarnings(TooManyPublicMethods)
 */
class HttpSessionCapturedConfigurationOrderedList
    extends HttpSessionCapturedConfigurationAbstract
    implements Countable
{
    private $configurations;
    
    
    public function
        __construct(array $configurations)
    {
        $this->configurations = array();
        $this->addMany($configurations);
    }
    
    
    public function
        clear()
    {
        $this->configurations = array();
    }
    
    public function
        addOne(HttpSessionCapturedConfigurationAbstract $configuration) : bool
    {
        if(in_array($configuration, $this->configurations))
        {
            return false;
        }
        $this->configurations[] = $configuration;
        return true;
    }
    
    public function
        addMany(array $configurations) : int
    {
        $nbAdded = 0;
        foreach($configurations as $config)
        {
            if($config instanceof HttpSessionCapturedConfigurationAbstract)
            {
                if($this->addOne($config))
                {
                    ++$nbAdded;
                }
            }
        }
        return $nbAdded;
    }
    
    public function
        count() : int
    {
        return count($this->configurations);
    }
    
    
    public function
        hasToSaveCookies() : OptionnalBoolean
    {
        foreach($this->configurations as $configuration)
        {
            $value = $configuration->hasToSaveCookies();
            if($value->isDefined())
            {
                return $value;
            }
        }
        return OptionnalBooleanUtils::createUndefined();
    }
    
    public function
        hasToSaveUserAgent() : OptionnalBoolean
    {
        foreach($this->configurations as $configuration)
        {
            $value = $configuration->hasToSaveUserAgent();
            if($value->isDefined())
            {
                return $value;
            }
        }
        return OptionnalBooleanUtils::createUndefined();
    }
    
    public function
        hasToSaveReferer() : OptionnalBoolean
    {
        foreach($this->configurations as $configuration)
        {
            $value = $configuration->hasToSaveReferer();
            if($value->isDefined())
            {
                return $value;
            }
        }
        return OptionnalBooleanUtils::createUndefined();
    }
    
    public function
        hasToSaveLanguage() : OptionnalBoolean
    {
        foreach($this->configurations as $configuration)
        {
            $value = $configuration->hasToSaveLanguage();
            if($value->isDefined())
            {
                return $value;
            }
        }
        return OptionnalBooleanUtils::createUndefined();
    }
    
    public function
        hasToSaveEncoding() : OptionnalBoolean
    {
        foreach($this->configurations as $configuration)
        {
            $value = $configuration->hasToSaveEncoding();
            if($value->isDefined())
            {
                return $value;
            }
        }
        return OptionnalBooleanUtils::createUndefined();
    }
    
    public function
        hasToSaveEtag() : OptionnalBoolean
    {
        foreach($this->configurations as $configuration)
        {
            $value = $configuration->hasToSaveEtag();
            if($value->isDefined())
            {
                return $value;
            }
        }
        return OptionnalBooleanUtils::createUndefined();
    }
    
    public function
        hasToSaveDoNotTrack() : OptionnalBoolean
    {
        foreach($this->configurations as $configuration)
        {
            $value = $configuration->hasToSaveDoNotTrack();
            if($value->isDefined())
            {
                return $value;
            }
        }
        return OptionnalBooleanUtils::createUndefined();
    }
}
