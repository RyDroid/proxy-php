<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


define(
    'CONFIGURATION_DEFAULT_FILE_PATH',
    ABSOLUTE_PATH . 'configuration/configuration.ini'
);


final class ConfigurationDefaultFile
{
    private static $conf = null;
    
    
    public static function
        loadFromPathForce() : bool
    {
        if(!file_exists(CONFIGURATION_DEFAULT_FILE_PATH))
        {
            return false;
        }
        self::$conf = ConfigurationUtils::createFromFilePath(
            CONFIGURATION_DEFAULT_FILE_PATH
        );
        return !self::$conf->isNull();
    }
    
    public static function
        loadFromPath() : bool
    {
        if(self::$conf == null)
        {
            return self::loadFromPathForce();
        }
        return true;
    }
    
    public static function
        get() : ConfigurationAbstract
    {
        self::loadFromPath();
        return self::$conf;
    }
    
    
    public static function
        getProxy() : ProxyConfigurationAbstract
    {
        if(self::loadFromPath())
        {
            return self::$conf->getProxy();
        }
        return ProxyConfigurationUtils::createNull();
    }
    
    public static function
        getSessionCaptured() : SessionCapturedConfigurationAbstract
    {
        if(self::loadFromPath())
        {
            return self::$conf->getSessionCaptured();
        }
        return new SessionCapturedConfigurationNull();
    }
    
    public static function
        getLogs() : LogsConfigurationAbstract
    {
        if(self::loadFromPath())
        {
            return self::$conf->getLogs();
        }
        return new LogsConfigurationDefaultValues();
    }
}
