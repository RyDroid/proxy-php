<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


abstract class UserInterfaceConfigurationAbstract
{
    public abstract function
        showDebug() : OptionnalBoolean;
    
    public abstract function
        showProxyGetParametersConfiguration() : OptionnalBoolean;
    
    public abstract function
        showProxyCookiesConfiguration() : OptionnalBoolean;
    
    public abstract function
        useClientScripts() : OptionnalBoolean;
    
    public function
        isNull() : bool
    {
        return (
            $this->showDebug()->isUndefined() &&
            $this->showProxyGetParametersConfiguration()->isUndefined() &&
            $this->showProxyCookiesConfiguration()->isUndefined() &&
            $this->useClientScripts()->isUndefined()
        );
    }
}
