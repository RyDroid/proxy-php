<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class HtmlDocumentUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        getCharset(DOMDocument $document) : string
    {
        $xpath = new DOMXPath($document);
        
        $query = '/html/head/meta[string(@charset)]';
        $elements = $xpath->query($query);
        foreach($elements as $element)
        {
            $attributeValue = $element->getAttribute('charset');
            $attributeValue = trim($attributeValue);
            if($attributeValue != '')
            {
                return $attributeValue;
            }
        }
        
        $query = (
               '/html/head/meta'.
               '['. XPathUtils::lowerCase('string(@http-equiv)')
               .'=\'content-type\' and string(@content)]'
        );
        $elements = $xpath->query($query);
        foreach($elements as $element)
        {
            $attributeValue = $element->getAttribute('content');
            if($attributeValue != '')
            {
                $charsetPosition = strripos($attributeValue, 'charset=');
                if($charsetPosition != FALSE)
                {
                    $charsetValue = substr(
                        $attributeValue,
                        $charsetPosition + strlen('charset=')
                    );
                    $charsetValue = trim($charsetValue);
                    if($charsetValue != '')
                    {
                        return $charsetValue;
                    }
                }
            }
        }

        return '';
    }
    
    public static function
        hasCharset(DOMDocument $document) : bool
    {
        $charset = self::getCharset($document);
        $charset = trim($charset);
        return $charset != '';
    }
    
    
    public static function
        getElementByIdWithXPath(DOMXPath $xpath, string $identifier)
    {
        if(empty($identifier))
        {
            return null;
        }
        $expression = '//*[@id='. $identifier. ']';
        $nodes = $xpath->query($expression);
        if($nodes->length == 0)
        {
            return null;
        }
        return $nodes->item(0);
    }
    
    public static function
        getElementByIdThroughXPath(DOMDocument $document, string $identifier)
    {
        $xpath = new DOMXPath($document);
        return self::getElementByIdWithXPath($xpath, $identifier);
    }
    
    public static function
        hasElementWithIdentifier(DOMDocument $document, string $identifier)
        : bool
    {
        return $document->getElementById($identifier) != null;
    }
    
    public static function
        getFirstNotUsedIdentifier(DOMDocument $document, array $identifiers)
        : string
    {
        foreach($identifiers as $identifier)
        {
            if(!self::hasElementWithIdentifier($document, $identifier))
            {
                return $identifier;
            }
        }
        return '';
    }
}
