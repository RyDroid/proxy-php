<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class ProxifyResultWebUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        createFromUrl(string $url) : ProxifyResultWeb
    {
        $metadata = new ProxifyResultMetaData($url);
        $result = new ProxifyResultWeb($metadata, '');
        return $result;
    }
    
    public static function
        createFromContentRequestResponse(ContentRequestResponse $response,
                                         ProxifyResultMetaData  $metadata)
        : ProxifyResultWeb
    {
        $content = $response->getContent();
        $header  = $response->getHeader();
        if($response instanceof ContentRequestResponseWithErrors)
        {
            $errors = $response->getErrors();
            return new ProxifyResultWeb($metadata, $content, $header, $errors);
        }
        return new ProxifyResultWeb($metadata, $content, $header, array());
    }
}
