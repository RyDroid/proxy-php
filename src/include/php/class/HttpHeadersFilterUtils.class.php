<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class HttpHeadersFilterUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        removeCookie(HttpHeaders& $headers)
    {
        $headers->removeKey('Cookie');
        $headers->removeKey('Set-Cookie');
        # (Set-)Cookie2: introduced in RFC 2965 and deprecated in RFC 6265
        $headers->removeKey('Cookie');
        $headers->removeKey('Set-Cookie2');
    }
    
    public static function
        withHttpSessionCapturedConfiguration(
            HttpHeaders& $headers,
            HttpSessionCapturedConfigurationAbstract $configuration
        )
    {
        if($configuration->hasToSaveCookies()->isFalse())
        {
            self::removeCookie($headers);
        }
        if($configuration->hasToSaveUserAgent()->isFalse())
        {
            $headers->removeKey('User-Agent');
        }
        if($configuration->hasToSaveReferer()->isFalse())
        {
            $headers->removeKey('Referer');
        }
        if($configuration->hasToSaveEncoding()->isFalse())
        {
            $headers->removeKey('Accept-Encoding');
        }
        if($configuration->hasToSaveLanguage()->isFalse())
        {
            $headers->removeKey('Accept-Language');
        }
        if($configuration->hasToSaveEtag()->isFalse())
        {
            $headers->removeKey('ETag');
        }
        if($configuration->hasToSaveDoNotTrack()->isFalse())
        {
            $headers->removeKey('DNT');
        }
    }
}
