<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class ProxyConfigurationUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        createNull()
        : ProxyConfigurationAbstract
    {
        return new ProxyConfiguration('', -1);
    }
    
    public static function
        getUrlFromMapWithoutSection(array $anArray) : string
    {
        foreach(ProxyConfigurationConst::KEYS_FOR_URL as $key)
        {
            if(isset($anArray[$key]) &&
               !empty(trim($anArray[$key])))
            {
                return $anArray[$key];
            }
        }
        if(isset($anArray['url']) && trim($anArray['url']) != '')
        {
            return $anArray['url'];
        }
        if(isset($anArray['URL']) && trim($anArray['URL']) != '')
        {
            return $anArray['URL'];
        }
        return '';
    }
    
    public static function
        getPortFromMapWithoutSection(array $anArray) : int
    {
        foreach(ProxyConfigurationConst::KEYS_FOR_PORT as $key)
        {
            if(isset($anArray[$key]) &&
               !empty(trim($anArray[$key])) &&
               is_numeric($anArray[$key]))
            {
                return intval($anArray[$key]);
            }
        }
        if(isset($anArray['port']) && is_numeric($anArray['port']))
        {
            return intval($anArray['port']);
        }
        return -1;
    }
    
    public static function
        createFromMapWithoutSection(array $anArray)
        : ProxyConfigurationAbstract
    {
        $url = self::getUrlFromMapWithoutSection($anArray);
        if($url == '')
        {
            return self::createNull();
        }
        
        $port = self::getPortFromMapWithoutSection($anArray);
        return new ProxyConfiguration($url, $port);
    }
    
    public static function
        createFromMap(array $anArray) : ProxyConfigurationAbstract
    {
        if(ArrayUtils::keyExistsWithNotEmptyValue($anArray, 'proxy'))
        {
            return self::createFromMapWithoutSection($anArray['proxy']);
        }
        return self::createFromMapWithoutSection($anArray);
    }
    
    public static function
        createFromFilePath(string $filePath) : ProxyConfigurationAbstract
    {
        $anArray = IniFileUtils::parseFromFilePathWithSections($filePath);
        $configuration = self::createFromMap($anArray);
        return $configuration;
    }
    
    
    public static function
        toDomElement(
            ProxyConfigurationAbstract $configuration,
            DOMDocument $document
        )
        : DOMElement
    {
        $element = $document->createElement('proxy');
        if($configuration->hasUrl())
        {
            $element->setAttribute('url', $configuration->getUrl());
        }
        if($configuration->hasPort())
        {
            $element->setAttribute('port', $configuration->getPort());
        }
        return $element;
    }
}
