<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class UrlStringParseUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        toMap(string $url) : array
    {
        return parse_url($url);
    }
    
    public static function
        getComponent(string $url, int $component) : string
    {
        if($component == -1)
        {
            return '';
        }
        return parse_url($url, $component);
    }
    
    
    public static function
        getWithoutFile(string $url) : string
    {
        $positionLastSlash = strrpos($url, '/') + 1;
        if($positionLastSlash == FALSE)
        {
            return $url;
        }
        if($positionLastSlash < 9)
        {
            $positionProtocolEnd = strpos($url, '://');
            if($positionProtocolEnd != FALSE && $positionProtocolEnd < 6)
            {
                return $url;
            }
        }
        return substr($url, 0, $positionLastSlash);
    }
    
    public static function
        getDirectory(string $url) : string
    {
        $path = self::getComponent($url, PHP_URL_PATH);
        $path = self::getWithoutFile($path);
        if($url[0] == '/')
        {
            return rtrim($path, '/');
        }
        if(StringUtils::startsWith($path, './'))
        {
            $path = substr($path, 2, strlen($path));
            return rtrim($path, '/');
        }
        return trim($path, '/');
    }
    
    public static function
        getFileName(string $url) : string
    {
        $position = strrpos($url, '/') + 1;
        return substr($url, $position, strlen($url));
    }
    
    public static function
        getLoginWithComponents(array $components) : string
    {
        $login = '';
        if(ArrayUtils::keyExistsWithNotEmptyValue($components, 'user'))
        {
            $login .= $components['user'];
        }
        if(ArrayUtils::keyExistsWithNotEmptyValue($components, 'pass'))
        {
            $login .= ':' . $components['pass'];
        }
        if($login != '')
        {
            $login .= '@';
        }
        return $login;
    }
    
    public static function
        getWithoutDirectoryAndFileNameWithComponents(array $components)
        : string
    {
        $resultStart = null;
        
        if(ArrayUtils::keyExistsWithNotEmptyValue($components, 'scheme'))
        {
            $resultStart = $components['scheme'] . '://';
        }
        
        $login = self::getLoginWithComponents($components);
        
        $resultEnd = '';
        if(ArrayUtils::keyExistsWithNotEmptyValue($components, 'host'))
        {
            $resultEnd .= $components['host'];
        }
        if(ArrayUtils::keyExistsWithNotEmptyValue($components, 'port') &&
           is_numeric($components['port']))
        {
            $resultEnd .= ':'. $components['port'];
        }
        
        $result = $login . $resultEnd;
        if(!StringUtils::endsWith($result, '/'))
        {
            $result .= '/';
        }
        
        if($resultStart != null)
        {
            $result = $resultStart . $result;
        }
        
        return $result;
    }
    
    public static function
        getWithoutDirectoryAndFileName(string $url) : string
    {
        $components = self::toMap($url);
        return self::getWithoutDirectoryAndFileNameWithComponents($components);
    }
}
