<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class ProxyConfiguration
    extends ProxyConfigurationAbstract
{
    private $url;
    private $port;
    
    
    public function
        __construct(string $url, int $port = 1)
    {
        $this->url  = $url;
        $this->port = $port;
    }
    
    
    public function
        getUrl() : string
    {
        return $this->url;
    }
    
    public function
        getPort() : int
    {
        return $this->port;
    }
}
