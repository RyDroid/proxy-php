<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class CssStringProxifierImport
    extends CssStringProxifierDecorator
{
    private function
        hasToProxifyConsideringWhatIsBefore(string $line,
                                            int $positionStart,
                                            int $positionBefore)
        : bool
    {
        if($positionBefore < 0)
        {
            return false;
        }
        
        for($position = $positionStart;
            $position < $positionBefore;
            ++$position)
        {
            if(!ctype_space($line[$position]) &&
               !($position + 3 < $positionBefore &&
                 $line[$position]   == '/' && $line[$position+1] == '*' &&
                 $line[$position+2] == '*' && $line[$position+3] == '/'))
            {
                return false;
            }
        }
        return true;
    }
    
    private static function
        isPossibleSeparator(string $character) : bool
    {
        return $character == '\'' || $character == '"';
    }
    
    private static function
        isCharacterIncorrectForUrl(string $character) : bool
    {
        return (
            $character == ' ' ||
            $character == '	' ||
            $character == ';' ||
            $character == "\n" ||
            $character == "\r" ||
            $character == "\0"
        );
    }
    
    private function
        findPositionEndOfUrl(string $line, int $offset, string $separator)
        : int
    {
        $positionEnd = $offset;
        while(true)
        {
            ++$positionEnd;
            if(!isset($line[$positionEnd]))
            {
                return -1;
            }
            
            $currentCharacter = $line[$positionEnd];
            if(self::isCharacterIncorrectForUrl($currentCharacter))
            {
                return -1;
            }
            if($currentCharacter == $separator)
            {
                return $positionEnd;
            }
        }
    }
    
    public function
        proxifyLinePartly(string $lineOrigin, int $offset)
        : StringProxifiedPartly
    {
        $positionStart = strpos($lineOrigin, '@import', $offset);
        if($positionStart === FALSE)
        {
            return new StringProxifiedPartly($lineOrigin, -1);
        }
        
        $hasToProxify = $this->hasToProxifyConsideringWhatIsBefore(
            $lineOrigin, $offset, $positionStart
        );
        if(!$hasToProxify)
        {
            return new StringProxifiedPartly($lineOrigin, -1);
        }
        
        $positionStart += strlen('@import');
        $lineOriginLength = strlen($lineOrigin);
        $separator = '';
        while(true)
        {
            ++$positionStart;
            if($positionStart == $lineOriginLength)
            {
                return new StringProxifiedPartly($lineOrigin, -1);
            }
            
            $currentCharacter = $lineOrigin[$positionStart];
            if(self::isPossibleSeparator($currentCharacter))
            {
                $separator = $currentCharacter;
                ++$positionStart;
                break;
            }
            if($currentCharacter != ' ' &&
               $currentCharacter != '	')
            {
                return new StringProxifiedPartly($lineOrigin, -1);
            }
        }
        
        $positionEnd = $this->findPositionEndOfUrl(
            $lineOrigin, $positionStart + 1, $separator
        );
        if($positionEnd < 0)
        {
            return new StringProxifiedPartly($lineOrigin, -1);
        }
        
        $linePartlyProxified = $this->proxifyStringWithUrl(
            $lineOrigin, $positionStart, $positionEnd
        );
        $positionEnd = strpos(
            $linePartlyProxified, ';', $positionEnd
        );
        return new StringProxifiedPartly(
            $linePartlyProxified, $positionEnd+1, 1
        );
    }
    
    public function
        proxifyLine(string $lineOrigin) : string
    {
        $line = parent::proxifyLine($lineOrigin);
        return CssLineProxifierUtils::proxifyLine($this, $line);
    }
    
    public function
        proxifyString(string $cssOrigin) : string
    {
        $css = parent::proxifyString($cssOrigin);
        return CssLineProxifierUtils::proxifyString($this, $css);
    }
}
