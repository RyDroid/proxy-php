<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class UserInterfaceConfiguration
    extends UserInterfaceConfigurationMutableAbstract
{
    private $debug;
    private $proxyGet;
    private $proxyCookies;
    private $clientScripts;
    
    
    public function
        __construct(OptionnalBoolean $debug,
                    OptionnalBoolean $proxyGet,
                    OptionnalBoolean $proxyCookies,
                    OptionnalBoolean $clientScripts)
    {
        $this->debug         = $debug;
        $this->proxyGet      = $proxyGet;
        $this->proxyCookies  = $proxyCookies;
        $this->clientScripts = $clientScripts;
    }
    
    
    public function
        showDebug() : OptionnalBoolean
    {
        return $this->debug;
    }
    
    public function
        setShowDebug(OptionnalBoolean $value)
    {
        $this->debug = $value;
    }
    
    
    public function
        showProxyGetParametersConfiguration(): OptionnalBoolean
    {
        return $this->proxyGet;
    }
    
    public function
        setShowProxyGetParametersConfiguration(OptionnalBoolean $value)
    {
        $this->proxyGet = $value;
    }
    
    
    public function
        showProxyCookiesConfiguration() : OptionnalBoolean
    {
        return $this->proxyCookies;
    }
    
    public function
        setShowProxyCookiesConfiguration(OptionnalBoolean $value)
    {
        $this->proxyCookies = $value;
    }
    
    
    public function
        useClientScripts() : OptionnalBoolean
    {
        return $this->clientScripts;
    }
    
    public function
        setUseClientScripts(OptionnalBoolean $value)
    {
        $this->clientScripts = $value;
    }
}
