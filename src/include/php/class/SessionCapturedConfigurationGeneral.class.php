<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @SuppressWarnings(TooManyPublicMethods)
 */
final class SessionCapturedConfigurationGeneral
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    private static $conf = null;
    
    
    public static function
        loadForce()
    {
        self::$conf = new SessionCapturedConfigurationOrderedList(
            array(
                new SessionCapturedConfigurationCookies(),
                SessionCapturedConfigurationDefaultFile::get(),
                ConfigurationDefaultFile::getSessionCaptured(),
                ConfigurationDefaultSystemFile::get()->getSessionCaptured(),
                new SessionCapturedConfigurationDefaultValues()
            )
        );
    }
    
    public static function
        load() : bool
    {
        if(self::$conf == null)
        {
            self::loadForce();
        }
        return self::$conf != null && count(self::$conf) > 0;
    }
    
    public static function
        get() : SessionCapturedConfigurationAbstract
    {
        self::load();
        return self::$conf;
    }
    
    
    public static function
        getDirectory() : string
    {
        return self::get()->getDirectory();
    }
    
    public static function
        hasDirectory() : bool
    {
        return self::get()->hasDirectory();
    }
    
    public static function
        getFileName() : string
    {
        return self::get()->getFileName();
    }
    
    public static function
        hasFileName() : bool
    {
        return self::get()->hasFileName();
    }
    
    public static function
        getFilePath() : string
    {
        return self::get()->getFilePath();
    }
    
    public static function
        hasFilePath() : string
    {
        return self::get()->hasFilePath();
    }
    
    
    public static function
        hasToSave() : bool
    {
        return self::get()->hasToSave()->isTrue();
    }
    
    public static function
        hasToSaveProxy() : bool
    {
        return self::get()->hasToSaveProxy()->isTrue();
    }
    
    public static function
        hasToSaveCookies() : bool
    {
        return self::get()->getHttp()->hasToSaveCookies()->isTrue();
    }
    
    public static function
        hasToSaveUserAgent() : bool
    {
        return self::get()->getHttp()->hasToSaveUserAgent()->isTrue();
    }
    
    public static function
        hasToSaveReferer() : bool
    {
        return self::get()->getHttp()->hasToSaveReferer()->isTrue();
    }
    
    public static function
        hasToSaveLanguage() : bool
    {
        return self::get()->getHttp()->hasToSaveLanguage()->isTrue();
    }
    
    public static function
        hasToSaveEncoding() : bool
    {
        return self::get()->getHttp()->hasToSaveEncoding()->isTrue();
    }
    
    public static function
        hasToSaveEtag() : bool
    {
        return self::get()->getHttp()->hasToSaveEtag()->isTrue();
    }
    
    public static function
        hasToSaveDoNotTrack() : bool
    {
        return self::get()->getHttp()->hasToSaveDoNotTrack()->isTrue();
    }
}
