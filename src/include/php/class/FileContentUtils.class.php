<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class FileContentUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        seemToBeHtml(string $aString) : bool
    {
        $aStringTrimed = ltrim($aString);
        return (
            StringUtils::startsWithInsensitive($aStringTrimed, '<html') ||
            StringUtils::startsWithInsensitive($aStringTrimed, '<!DOCTYPE html')
        );
    }
    
    public static function
        seemToBeHtmlPart(string $aString) : bool
    {
        $aStringTrimed = ltrim($aString);
        if(isset($aStringTrimed[0]) && $aStringTrimed[0] == '<')
        {
            $aStringTrimed = rtrim($aString);
            if(substr($aStringTrimed, -1) == '>')
            {
                $document = new DOMDocument();
                try
                {
                    return $document->loadHTML($aStringTrimed);
                }
                catch(Exception $exception)
                {
                    return false;
                }
            }
        }
        return false;
    }
    
    public static function
        seemToBeHtmlOrPart(string $aString) : bool
    {
        return (
            self::seemToBeHtml    ($aString) ||
            self::seemToBeHtmlPart($aString)
        );
    }
}
