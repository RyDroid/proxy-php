<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class LogsConfiguration
    extends LogsConfigurationMutableAbstract
{
    private $save;
    private $filePath;
    private $show;
    
    
    public function
        __construct(string $filePath,
                    OptionnalBoolean $save,
                    OptionnalBoolean $show)
    {
        $this->save     = $save;
        $this->filePath = $filePath;
        $this->show     = $show;
    }
    
    
    public function
        hasToSave() : OptionnalBoolean
    {
        return $this->save;
    }
    
    public function
        setSave(OptionnalBoolean $value)
    {
        $this->save = $value;
    }
    
    
    public function
        getFilePath() : string
    {
        return $this->filePath;
    }
    
    public function
        setFilePath(string $value) : bool
    {
        if(empty($value) || StringUtils::endsWith($value, '/'))
        {
            return false;
        }
        $this->filePath = $value;
        return true;
    }
    
    
    public function
        canBeShown() : OptionnalBoolean
    {
        return $this->show;
    }
    
    public function
        setShow(OptionnalBoolean $value)
    {
        $this->show = $value;
    }
}
