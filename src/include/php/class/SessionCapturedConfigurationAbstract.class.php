<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


abstract class SessionCapturedConfigurationAbstract
{
    public abstract function
        getDirectory() : string;
    
    public function
        hasDirectory() : bool
    {
        return !empty($this->getDirectory());
    }
    
    public abstract function
        getFileName() : string;
    
    public function
        hasFileName() : bool
    {
        return !empty($this->getFileName());
    }
    
    public function
        getFilePath() : string
    {
        return UrlStringUtils::concatenate(
            $this->getDirectory(), $this->getFileName()
        );
    }
    
    public function
        hasFilePath() : bool
    {
        return !empty($this->getFilePath());
    }
    
    
    public abstract function
        hasToSave() : OptionnalBoolean;
    
    public abstract function
        hasToCreateAutomatically() : OptionnalBoolean;
    
    public abstract function
        hasToSaveProxy() : OptionnalBoolean;
    
    
    public abstract function
        getHttp() : HttpSessionCapturedConfigurationAbstract;
}
