<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class GenericConfigurationUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        isOkAccordingToString(string $value) : bool
    {
        $valuePrepared = strtolower(trim($value));
        return (
            BooleanStringUtils::isTrueExtended($valuePrepared) ||
            $valuePrepared == 'ok' ||
            $valuePrepared == 'of-course' ||
            $valuePrepared == 'of course'
        );
    }
    
    public static function
        isBooleanAccordingToKeyOfMap(array $aMap, string $key) : bool
    {
        return (
            ArrayUtils::keyExistsWithNotEmptyValue($aMap, $key) &&
            BooleanStringUtils::isBoolean($aMap[$key])
        );
    }
    
    public static function
        isBooleanExtendedAccordingToKeyOfMap(array $aMap, string $key) : bool
    {
        if(!ArrayUtils::keyExistsWithNotEmptyValue($aMap, $key))
        {
            return false;
        }
        $value = $aMap[$key];
        if(!is_string($value))
        {
            return false;
        }
        return BooleanStringUtils::isBooleanExtended($value);
    }
    
    public static function
        isOkAccordingToKeyOfMap(array $aMap, string $key) : bool
    {
        return (
            ArrayUtils::keyExistsWithNotEmptyValue($aMap, $key) &&
            self::isOkAccordingToString($aMap[$key])
        );
    }
    
    public static function
        isOkAccordingToKeysOfMap(array $aMap, array $keys) : bool
    {
        foreach($keys as $key)
        {
            if(self::isOkAccordingToKeyOfMap($aMap, $key))
            {
                return true;
            }
        }
        return false;
    }
    
    
    public static function
        getPotentielValueFromKeysOfMap(array $aMap, array $keys)
        : string
    {
        foreach($keys as $key)
        {
            if(ArrayUtils::keyExistsWithNotEmptyValue($aMap, $key))
            {
                return $aMap[$key];
            }
        }
        return '';
    }
    
    public static function
        getPotentielBooleanExtendedValueFromKeysOfMap(array $aMap, array $keys)
        : string
    {
        foreach($keys as $key)
        {
            if(GenericConfigurationUtils::
               isBooleanExtendedAccordingToKeyOfMap($aMap, $key))
            {
                return $aMap[$key];
            }
        }
        return '';
    }
}
