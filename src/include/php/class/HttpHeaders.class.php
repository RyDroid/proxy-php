<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


class HttpHeaders
{
    private $headers;
    
    
    private function
        __construct(array $headers)
    {
        ArrayStringUtils::removeEmptyAfterTrim($headers);
        $this->headers = $headers;
    }
    
    public static function
        createEmpty() : HttpHeaders
    {
        return new HttpHeaders(array());
    }
    
    public static function
        createFromArrayOfLines(array $headers) : HttpHeaders
    {
        return new HttpHeaders($headers);
    }
    
    public static function
        createFromMap(array $headers) : HttpHeaders
    {
        $table = array();
        foreach($headers as $key => $value)
        {
            $pair = Pair::create($key, $value);
            $table[] = (string) HttpHeaderLine::createFromPair($pair);
        }
        return self::createFromArrayOfLines($table);
    }
    
    public static function
        createFromString(string $headersString) : HttpHeaders
    {
        $headersArray = array();
        foreach(preg_split("/((\r?\n)|(\r\n?))/", $headersString) as $line)
        {
            if(trim($line) != '')
            {
                $headersArray[] = $line;
            }
        }
        return new HttpHeaders($headersArray);
    }
    
    public static function
        get() : HttpHeaders
    {
        return HttpHeaders::createFromMap(ServerUtils::getHeaders());
    }
    
    
    public function
        getArrayOfHttpHeaderLine() : array
    {
        $result = array();
        foreach($this->headers as $header)
        {
            $line = HttpHeaderLine::createFromString($header);
            $result[] = $line;
        }
        return $result;
    }
    
    public function
        getArrayOfLines() : array
    {
        return $this->headers;
    }
    
    public function
        getString() : string
    {
        return implode(PHP_EOL, $this->headers);
    }
    
    
    public function
        isEmpty() : bool
    {
        return empty($this->headers);
    }
    
    public function
        getNumberOfLines() : int
    {
        return count($this->headers);
    }
    
    
    public function
        getLineNumberOfKey(string $key) : int
    {
        $key = trim($key);
        if($key == '')
        {
            return -1;
        }
        
        $nbLines = count($this->headers);
        for($index = 0; $index < $nbLines; ++$index)
        {
            $line = $this->headers[$index];
            if(StringUtils::startsWithInsensitive(ltrim($line), $key))
            {
                return $index;
            }
        }
        return -1;
    }
    
    public function
        hasKey(string $key) : bool
    {
        return $this->getLineNumberOfKey($key) >= 0;
    }
    
    public function
        getLineOfKey(string $key) : string
    {
        $index = $this->getLineNumberOfKey($key);
        if($index < 0)
        {
            return '';
        }
        return $this->headers[$index];
    }
    
    public function
        getPairOfKey(string $key) : Pair
    {
        $key = trim($key);
        if($key == '')
        {
            return new Pair();
        }
        $line = $this->getLineOfKey($key);
        return HttpHeaderLineStringUtils::getPair($line);
    }
    
    public function
        getHttpHeaderLineOfKey(string $key) : HttpHeaderLine
    {
        $pair = $this->getPairOfKey($key);
        return HttpHeaderLine::createFromPair($pair);
    }
    
    public function
        getValueOfKey(string $key) : string
    {
        $pair = $this->getPairOfKey($key);
        if(is_string($pair->second))
        {
            return $pair->second;
        }
        return '';
    }
    
    public function
        setValueOfKey(string $key, string $value) : bool
    {
        $value = trim($value);
        if($value == '')
        {
            return false;
        }
        
        $lineNumber = $this->getLineNumberOfKey($key);
        if($lineNumber < 0)
        {
            return false;
        }
        
        $lineString = $this->headers[$lineNumber];
        $lineObject = HttpHeaderLine::createFromString($lineString);
        if($lineObject->isNull())
        {
            return false;
        }
        
        if(!$lineObject->setValue($value))
        {
            return false;
        }
        
        $this->headers[$lineNumber] = (string) $lineObject;
        return true;
    }
    
    public function
        removeKey(string $key) : bool
    {
        $index = $this->getLineNumberOfKey($key);
        if($index < 0)
        {
            return false;
        }
        array_splice($this->headers, $index, 1);
        return true;
    }
    
    public function
        addHttpHeaderLine(HttpHeaderLine $line) : bool
    {
        if($line->isNull())
        {
            return false;
        }
        $this->headers[] = (string) $line;
        return true;
    }
    
    public function
        addStringLine(string $lineString) : bool
    {
        $line = HttpHeaderLine::createFromString($lineString);
        return $this->addHttpHeaderLine($line);
    }
    
    
    public function
        __toString() : string
    {
        return implode(PHP_EOL, $this->getArrayOfLines());
    }
}
