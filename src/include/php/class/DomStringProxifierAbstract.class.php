<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


abstract class DomStringProxifierAbstract
    extends DomDocumentProxifierAbstract
{
    public static function
        createPermissiveDomDocument() : DOMDocument
    {
        $document = new DOMDocument();
        #$document->recover = true;
        #$document->strictErrorChecking = false;
        return $document;
    }
    
    
    private $preparator;
    
    
    public function
        __construct()
    {
        parent::__construct();
        $this->preparator = new DomStringPreparatorEntity();
    }
    
    
    public function
        prepareString(string $domString) : string
    {
        return $this->preparator->prepare($domString);
    }
    
    public function
        unprepareString(string $domString) : string
    {
        return $this->preparator->unprepare($domString);
    }
    
    
    public function
        proxifyString(string $domString)
        : string
    {
        $document = self::createPermissiveDomDocument();
        $domStringModified = $this->prepareString($domString);
        if(@$document->loadXML($domStringModified))
        {
            $result = $this->proxifyDocumentToString($document);
            $result = $this->unprepareString($result);
            return $result;
        }
        return $domString;
    }
    
    public function
        proxifyStringWithoutDeclaration(string $domString)
        : string
    {
        $document = self::createPermissiveDomDocument();
        $domStringModified = $this->prepareString($domString);
        if(@$document->loadXML($domStringModified))
        {
            $res = $this->proxifyDocumentToStringWithoutDeclaration($document);
            $res = $this->unprepareString($res);
            return $res;
        }
        return $domString;
    }
    
    public function
        proxifyHtmlString(string $html)
        : string
    {
        if(trim($html) == '')
        {
            return $html;
        }
        $document = self::createPermissiveDomDocument();
        $htmlModified = $this->prepareString($html);
        if(@$document->loadHTML($htmlModified))
        {
            $result = $this->proxifyDocumentToHtmlString($document);
            $result = $this->unprepareString($result);
            return $result;
        }
        return $html;
    }
    
    public function
        proxifyHtmlStringWithoutDeclaration(string $html)
        : string
    {
        $document = self::createPermissiveDomDocument();
        $htmlModified = $this->prepareString($html);
        if(@$document->loadHTML($htmlModified))
        {
            $result = $this->proxifyDocumentToHtmlStringWithoutDeclaration(
                $document
            );
            $result = $this->unprepareString($result);
            return $result;
        }
        return $html;
    }
}
