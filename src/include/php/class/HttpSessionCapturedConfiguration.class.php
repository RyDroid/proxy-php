<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class HttpSessionCapturedConfiguration
    extends HttpSessionCapturedConfigurationMutableAbstract
{
    private $saveCookies;
    private $saveUserAgent;
    private $saveReferer;
    private $saveLanguage;
    private $saveEncoding;
    private $saveEtag;
    private $saveDoNotTrack;
    
    
    public function
        __construct()
    {
        $this->saveCookies    = OptionnalBooleanUtils::createUndefined();
        $this->saveUserAgent  = OptionnalBooleanUtils::createUndefined();
        $this->saveReferer    = OptionnalBooleanUtils::createUndefined();
        $this->saveLanguage   = OptionnalBooleanUtils::createUndefined();
        $this->saveEncoding   = OptionnalBooleanUtils::createUndefined();
        $this->saveEtag       = OptionnalBooleanUtils::createUndefined();
        $this->saveDoNotTrack = OptionnalBooleanUtils::createUndefined();
    }
    
    
    public function
        hasToSaveCookies() : OptionnalBoolean
    {
        return $this->saveCookies;
    }
    
    public function
        setSaveCookies(OptionnalBoolean $value)
    {
        $this->saveCookies = $value;
    }
    
    
    public function
        hasToSaveUserAgent() : OptionnalBoolean
    {
        return $this->saveUserAgent;
    }
    
    public function
        setSaveUserAgent(OptionnalBoolean $value)
    {
        $this->saveUserAgent = $value;
    }
    
    
    public function
        hasToSaveReferer() : OptionnalBoolean
    {
        return $this->saveReferer;
    }
    
    public function
        setSaveReferer(OptionnalBoolean $value)
    {
        $this->saveReferer = $value;
    }
    
    
    public function
        hasToSaveLanguage() : OptionnalBoolean
    {
        return $this->saveLanguage;
    }
    
    public function
        setSaveLanguage(OptionnalBoolean $value)
    {
        $this->saveLanguage = $value;
    }
    
    
    public function
        hasToSaveEncoding() : OptionnalBoolean
    {
        return $this->saveEncoding;
    }
    
    public function
        setSaveEncoding(OptionnalBoolean $value)
    {
        $this->saveEncoding = $value;
    }
    
    
    public function
        hasToSaveEtag() : OptionnalBoolean
    {
        return $this->saveEtag;
    }
    
    public function
        setSaveEtag(OptionnalBoolean $value)
    {
        $this->saveEtag = $value;
    }
    
    
    public function
        hasToSaveDoNotTrack() : OptionnalBoolean
    {
        return $this->saveDoNotTrack;
    }
    
    public function
        setSaveDoNotTrack(OptionnalBoolean $value)
    {
        $this->saveDoNotTrack = $value;
    }
}
