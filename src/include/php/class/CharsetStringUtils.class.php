<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class CharsetStringUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        simplify(string $charset) : string
    {
        return strtolower(str_replace('-', '', trim($charset)));
    }
    
    
    public static function
        isUtf8(string $charset) : bool
    {
        return self::simplify($charset) == 'utf8';
    }
    
    public static function
        isUtf16(string $charset) : bool
    {
        return self::simplify($charset) == 'utf16';
    }
    
    public static function
        isUtf32(string $charset) : bool
    {
        return self::simplify($charset) == 'utf32';
    }
    
    public static function
        isUnicode(string $charset) : bool
    {
        return (
            self::isUtf8($charset)  ||
            self::isUtf16($charset) ||
            self::isUtf32($charset)
        );
    }
}
