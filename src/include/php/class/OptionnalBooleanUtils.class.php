<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class OptionnalBooleanUtils
{
    public static function
        isValidInteger(int $value) : bool
    {
        return (
            $value == OptionnalBooleanConst::UNDEFINED ||
            $value == OptionnalBooleanConst::FALSE  ||
            $value == OptionnalBooleanConst::TRUE
        );
    }
    
    public static function
        isValidString(string $value) : bool
    {
        $newValue = strtolower(trim($value));
        return (
            BooleanStringUtils::isBooleanExtended($newValue) ||
            $newValue == 'undefined' ||
            $newValue == 'unknow'
        );
    }
    
    
    public static function
        createUndefined() : OptionnalBoolean
    {
        return new OptionnalBoolean(OptionnalBooleanConst::UNDEFINED);
    }
    
    public static function
        createFalse() : OptionnalBoolean
    {
        return new OptionnalBoolean(OptionnalBooleanConst::FALSE);
    }
    
    public static function
        createTrue() : OptionnalBoolean
    {
        return new OptionnalBoolean(OptionnalBooleanConst::TRUE);
    }
    
    public static function
        createFromString(string $value) : OptionnalBoolean
    {
        if(BooleanStringUtils::isFalseExtended($value))
        {
            return self::createFalse();
        }
        if(BooleanStringUtils::isTrueExtended($value))
        {
            return self::createTrue();
        }
        return self::createUndefined();
    }
    
    
    public static function
        getFirstValueDefinedFromArray(array $values) : OptionnalBoolean
    {
        foreach($values as $value)
        {
            if($value instanceof OptionnalBoolean &&
               $value->isDefined())
            {
                return $value;
            }
        }
        return self::createUndefined();
    }
    
    
    public static function
        toYesNoUndefined(OptionnalBoolean $value) : string
    {
        if($value->isFalse())
        {
            return 'no';
        }
        if($value->isTrue())
        {
            return 'yes';
        }
        return 'undefined';
    }
}
