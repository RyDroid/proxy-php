<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class IniFileUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    const DEFAULT_SCANNER_MODE = INI_SCANNER_RAW;
    
    
    public static function
        parseStringWithSections(string $iniContent,
                                int $scannerMode = self::DEFAULT_SCANNER_MODE)
        : array
    {
        return parse_ini_string($iniContent, true, $scannerMode);
    }
    
    public static function
        parseStringWithoutSections(
            string $iniContent,
            int $scannerMode = self::DEFAULT_SCANNER_MODE
        ) : array
    {
        return parse_ini_string($iniContent, false, $scannerMode);
    }
    
    public static function
        parseFromFilePathWithSections(
            string $filePath,
            int $scannerMode = self::DEFAULT_SCANNER_MODE
        ) : array
    {
        if(!file_exists($filePath))
        {
            return array();
        }
        return parse_ini_file($filePath, true, $scannerMode);
    }
    
    public static function
        parseFromFilePathWithoutSections(string $filePath,
                                         int $scannerMode = INI_SCANNER_NORMAL)
        : array
    {
        if(!file_exists($filePath))
        {
            return array();
        }
        return parse_ini_file($filePath, false, $scannerMode);
    }
    
    
    public static function
        simplePairToString(string $key, string $value) : string
    {
        return $key .'='. $value;
    }
    
    public static function
        pairToString(string $key, $value, string $section = '')
        : string
    {
        if(is_array($value))
        {
            if($section != '')
            {
                $section .= '.';
            }
            $section .= $key;
            return
                '[' . $section . ']'. PHP_EOL .
                self::arrayPartToString($value, $section);
        }
        return self::simplePairToString($key, $value);
    }
    
    public static function
        arrayPartToString(array $anArray, string $section) : string
    {
        $out = '';
        foreach($anArray as $key => $value)
        {
            $out .= self::pairToString($key, $value, $section) . PHP_EOL;
        }
        return $out;
    }
    
    public static function
        arrayToString(array $anArray) : string
    {
        return self::arrayPartToString($anArray, '');
    }
}
