<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


abstract class LogsConfigurationAbstract
{
    public abstract function
        hasToSave() : OptionnalBoolean;
    
    public abstract function
        getFilePath() : string;
    
    public function
        hasFilePath() : string
    {
        return !empty($this->getFilePath());
    }
    
    public function
        getDirectoryName() : string
    {
        return pathinfo($this->getFilePath(), PATHINFO_DIRNAME);
    }
    
    public function
        getFileName() : string
    {
        return pathinfo($this->getFilePath(), PATHINFO_FILENAME);
    }
    
    public function
        getBaseName() : string
    {
        return pathinfo($this->getFilePath(), PATHINFO_BASENAME);
    }
    
    public function
        getFormatAsString() : string
    {
        return pathinfo($this->getFilePath(), PATHINFO_EXTENSION);
    }
    
    public function
        hasFormat() : bool
    {
        $format = trim($this->getFormatAsString());
        return !empty($format) && $format != 'undefined';
    }
    
    public abstract function
        canBeShown() : OptionnalBoolean;
}
