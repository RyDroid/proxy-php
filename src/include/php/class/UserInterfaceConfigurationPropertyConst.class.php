<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class UserInterfaceConfigurationPropertyConst
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    const SHOW_DEBUG_WITHOUT_SECTION_KEYS = [
        'show-debug', 'debug-show',
        'show-dbg',   'gdb-run'
    ];
    const SHOW_PROXY_GET_PARAMETERS_CONFIGURATION_WITHOUT_SECTION_KEYS = [
        'show-proxy-get-parameters-configuration',
        'display-proxy-get-parameters-configuration',
        'show-proxy-get-params-configuration',
        'display-proxy-get-params-configuration',
        'show-proxy-get-parameters',    'show-proxy-get-params',
        'display-proxy-get-parameters', 'display-proxy-get-params'
    ];
    const SHOW_PROXY_COOKIES_CONFIGURATION_WITHOUT_SECTION_KEYS = [
        'show-proxy-cookies-configuration',
        'display-proxy-cookies-configuration',
        'show-proxy-cookies-parameters',
        'display-proxy-cookies-parameters',
        'show-proxy-cookies-params',
        'display-proxy-cookies-params'
    ];
    const USE_CLIENT_SCRIPTS_WITHOUT_SECTION_KEYS = [
        'client-scripts', 'use-client-scripts', 'enable-client-scripts',
        'javascript',     'use-javascript',     'enable-javascript'
    ];
}
