<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class TimeXmlUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        floatToString(float $value) : string
    {
        if(!function_exists('sprintf'))
        {
            return number_format($value, 5, '.', '');
        }
        return sprintf('%.5F' /* no scientific notation */, $value);
    }
    
    public static function
        toDomElement(
            DOMDocument $document,
            float  $value,
            string $name = '',
            string $unit = '',
            string $type = ''
        )
        : DOMElement
    {
        if(empty($name))
        {
            $name = 'time';
        }
        $element = $document->createElement($name);
        $valueString = self::floatToString($value);
        $element->setAttribute('value', $valueString);
        if(!empty(trim($unit)))
        {
            $element->setAttribute('unit', $unit);
        }
        if(!empty(trim($type)))
        {
            $element->setAttribute('type', $type);
        }
        return $element;
    }
}
