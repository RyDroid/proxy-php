<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class WebSessionCapturedXml
    extends WebSessionCapturedFileLockedAbstract
{
    public function
        __construct(string $filePath)
    {
        parent::__construct($filePath);
    }
    
    
    protected function
        saveFileWithoutLock($file) : array
    {
        if(!($file instanceof DOMDocument))
        {
            return array(
                get_class() .'->:'. __METHOD__ .': '.
                '$file is not an instance of DOMDocument'
            );
        }
        
        try
        {
            if($file->save($this->getFilePath()))
            {
                return array();
            }
            return array(
                get_class() .'->:'. __METHOD__ .': '.
                '$file was not saved'
            );
        }
        catch(Exception $exception)
        {
            return array((string) $exception);
        }
    }
    
    protected function
        loadFileWithoutUnlock($file) : array
    {
        if(!($file instanceof DOMDocument))
        {
            return array(
                get_class() .'->'. __METHOD__ .': '.
                '$file is not an instance of DOMDocument'
            );
        }
        
        $filePath = $this->getFilePath();
        if(filesize($filePath) == 0 || $file->load($filePath))
        {
            return array();
        }
        return array(
            get_class() .'->:'. __METHOD__ .': '.
            '$file was not loaded'
        );
    }
    
    
    private function
        addMetaDataToLoadedDocument(ProxifyResultMetaData $metadata,
                                    DOMDocument $document) : array
    {
        if(!$document->hasChildNodes())
        {
            $element = $document->createElement('session');
            $document->appendChild($element);
        }
        
        $sessionElements = $document->getElementsByTagName('session');
        $sessionElement = $sessionElements->item(0);
        $resourceElement = (
            ProxifyResultMetaDataConvertUtils::toDomElementDefault(
                $metadata, $document
            )
        );
        $sessionElement->appendChild($resourceElement);
        
        return $this->saveFileAndUnlock($document);
    }
    
    public function
        addMetaData(ProxifyResultMetaData $metadata) : array
    {
        if(!$this->createDirectoriesForFileIfNeeded())
        {
            return array(
                get_class(). ': fail to create directory/directories'
            );
        }
        $document = new DOMDocument('1.0', mb_internal_encoding());
        if(!$this->loadFileIfExist($document))
        {
            return array(get_class(). ': fail to load file');
        }
        return $this->addMetaDataToLoadedDocument($metadata, $document);
    }
}
