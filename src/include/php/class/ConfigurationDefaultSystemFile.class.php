<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


define(
    'CONFIGURATION_DEFAULT_SYSTEM_FILE_PATH',
    '/etc/rydroid-web-proxy/configuration.ini'
);


final class ConfigurationDefaultSystemFile
{
    private static $conf = null;
    
    
    public static function
        loadFromPathForce() : bool
    {
        if(!file_exists(CONFIGURATION_DEFAULT_SYSTEM_FILE_PATH))
        {
            return false;
        }
        self::$conf = ConfigurationUtils::createFromFilePath(
            CONFIGURATION_DEFAULT_SYSTEM_FILE_PATH
        );
        return !self::$conf->isNull();
    }
    
    public static function
        loadFromPath() : bool
    {
        if(self::$conf == null)
        {
            return self::loadFromPathForce();
        }
        return true;
    }
    
    public static function
        get() : ConfigurationAbstract
    {
        if(self::loadFromPath())
        {
            return self::$conf;
        }
        return new ConfigurationDefaultValues();
    }
}
