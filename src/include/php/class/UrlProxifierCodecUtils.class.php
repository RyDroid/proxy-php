<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class UrlProxifierCodecUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        encodeUrl(string& $anUrl)
    {
        $anUrl = urlencode($anUrl);
    }
    
    public static function
        getEncodedUrl(string $anUrl) : string
    {
        $urlResult = $anUrl;
        self::encodeUrl($urlResult);
        return $urlResult;
    }
    
    public static function
        encodeUrls(array& $urls)
    {
        foreach($urls as &$url)
        {
            self::encodeUrl($url);
        }
    }
    
    public static function
        createEncodedUrls(array $urls) : array
    {
        $urlsResult = $urls;
        self::encodeUrls($urlsResult);
        return $urlsResult;
    }
    
    
    public static function
        decodeUrl(string $anUrl) : string
    {
        return urldecode($anUrl);
    }
    
    public static function
        decodeUrls(array& $urls)
    {
        foreach($urls as &$url)
        {
            $url = self::decodeUrl($url);
        }
    }
    
    public static function
        createDecodedUrls(array $urls) : array
    {
        $urlsResult = $urls;
        self::decodeUrls($urlsResult);
        return $urlsResult;
    }
    
    
    public static function
        encodeParameter(string $parameter) : string
    {
        return rawurlencode($parameter);
    }
    
    public static function
        encodeParameters(array& $parameters)
    {
        foreach($parameters as &$parameter)
        {
            $parameter = self::encodeParameter($parameter);
        }
    }
    
    public static function
        createEncodedParameters(array $parameters) : array
    {
        $parametersResult = $parameters;
        self::encodeParameters($parametersResult);
        return $parametersResult;
    }
    
    public static function
        encodeParametersOfUrl(string $anUrl) : string
    {
        $components = UrlStringParseUtils::toMap($anUrl);
        if(ArrayUtils::keyExistsWithNotEmptyValue($components, 'query'))
        {
            $queryParts = GetParametersUtils::stringToMap($components['query']);
            self::encodeParameters($queryParts);
            $components['query'] = GetParametersUtils::mapToString($queryParts);
            $result = UrlStringUtils::componentsToString($components);
            return $result;
        }
        return $anUrl;
    }
}
