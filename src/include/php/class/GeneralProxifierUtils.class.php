<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class GeneralProxifierUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    /**
     * @return Number of modified elements, or -1 if a problem occured
     */
    public static function
        proxifyHtmlDocument(DOMDocument $document) : int
    {
        return HtmlDocumentProxifierUtils::proxifyDocument($document);
    }
    
    public static function
        proxifyHtmlDocumentToString(DOMDocument $document) : string
    {
        return HtmlDocumentProxifierUtils::proxifyDocumentToString($document);
    }
    
    public static function
        proxifyHtmlStringToString(string $html) : string
    {
        return HtmlDocumentProxifierUtils::proxifyString($html);
    }
    
    public static function
        proxifyCssStringToString(string $css) : string
    {
        return CssStringProxifierUtils::proxifyString($css);
    }
    
    public static function
        proxifyContent(string $content) : string
    {
        if(FileContentUtils::seemToBeHtmlOrPart($content))
        {
            return self::proxifyHtmlStringToString($content);
        }
        return $content;
    }
    
    public static function
        proxifyRemoteContent(string $url, HttpHeaders $headers)
        : ProxifyResultWeb
    {
        $metadata = new ProxifyResultMetaData($url);
        
        $metadata->setTimeRequestToNow();
        $response = GeneralContentGetterUtils::get($url, $headers);
        $metadata->setTimeResponseToNow();
        $metadata->setHeadersClient($headers);
        
        $result  = ProxifyResultWebUtils::createFromContentRequestResponse(
            $response, $metadata
        );
        return $result;
    }
}
