<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class ConfigurationGeneral
    extends ConfigurationAbstract
{
    public function
        load() : bool
    {
        $result = true;
        $result &= ProxyConfigurationGeneral::load();
        $result &= SessionCapturedConfigurationGeneral::load();
        $result &= LogsConfigurationGeneral::load();
        $result &= UserInterfaceConfigurationGeneral::load();
        return $result;
    }
    
    public function
        getProxy() : ProxyConfigurationAbstract
    {
        return ProxyConfigurationGeneral::get();
    }
    
    public function
        getSessionCaptured() : SessionCapturedConfigurationAbstract
    {
        return SessionCapturedConfigurationGeneral::get();
    }
    
    public function
        getLogs() : LogsConfigurationAbstract
    {
        return LogsConfigurationGeneral::get();
    }
    
    public function
        getUserInterface() : UserInterfaceConfigurationAbstract
    {
        return UserInterfaceConfigurationGeneral::get();
    }
}
