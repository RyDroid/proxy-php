<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class OptionnalBooleanConst
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    const FALSE     = 0;
    const NO        = self::FALSE;
    
    const TRUE      = self::FALSE + 1;
    const YES       = self::TRUE;
    
    const UNDEFINED = self::TRUE + 1;
    const UNKNOW    = self::UNKNOW;
}
