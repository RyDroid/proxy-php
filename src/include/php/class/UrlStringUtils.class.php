<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class UrlStringUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        isLocal(string $url) : bool
    {
        return (
            StringUtils::startsWith($url, '/') ||
            StringUtils::startsWith($url, 'file://')
        );
    }
    
    
    public static function
        componentsToStringWithoutQueryAndFragment(array $components)
        : string
    {
        $result = (
            UrlStringParseUtils::
            getWithoutDirectoryAndFileNameWithComponents(
                $components
            )
        );
        if(ArrayUtils::keyExistsWithNotEmptyValue($components, 'path'))
        {
            $result = self::concatenate($result, $components['path']);
        }
        return $result;
    }
    
    public static function
        componentsToString(array $components)
        : string
    {
        $result = self::componentsToStringWithoutQueryAndFragment($components);
        if(ArrayUtils::keyExistsWithNotEmptyValue($components, 'query'))
        {
            $result .= '?'. $components['query'];
        }
        if(ArrayUtils::keyExistsWithNotEmptyValue($components, 'fragment'))
        {
            $result .= '#'. $components['fragment'];
        }
        return $result;
    }
    
    
    public static function
        concatenate(string $url1, string $url2) : string
    {
        if($url1 == '')
        {
            return $url2;
        }
        if($url2 == '')
        {
            return $url1;
        }
        if(StringUtils::endsWith($url1, '/') &&
           $url2[0] == '/')
        {
            return $url1 . substr($url2, 1);
        }
        if(!StringUtils::endsWith($url1, '/') &&
           $url2[0] != '/')
        {
            return $url1 .'/'. $url2;
        }
        if(StringUtils::endsWith($url1, '/') &&
           StringUtils::startsWith($url2, './'))
        {
            return $url1 . substr($url2, 2);
        }
        return $url1 . $url2;
    }
    
    public static function
        getAbsolute(string $currentUrl, string $notAbsoluteUrl) : string
    {
        if($notAbsoluteUrl[0] == '/' &&
           (!isset($notAbsoluteUrl[1]) || $notAbsoluteUrl[1] != '/'))
        {
            return self::concatenate(
                UrlStringParseUtils::getWithoutDirectoryAndFileName(
                    $currentUrl
                ),
                $notAbsoluteUrl
            );
        }
        if(!StringUtils::startsWith($notAbsoluteUrl, '//') &&
           !UrlProtocolStringUtils::isWeb($notAbsoluteUrl))
        {
            return self::concatenate(
                $currentUrl, $notAbsoluteUrl
            );
        }
        return $notAbsoluteUrl;
    }
    
    public static function
        addStringParameters(string $url, string $parameters) : string
    {
        if(empty(trim($parameters)))
        {
            return $url;
        }
        if(StringUtils::contains($url, '?'))
        {
            return $url .'&'. $parameters;
        }
        return $url .'?'. $parameters;
    }
    
    public static function
        removeStringParameters(string $url, string $parameters) : string
    {
        $markPosition = stripos($url, '?');
        if($markPosition === FALSE)
        {
            return $url;
        }
        
        $beforeMarkString = substr($url, 0, $markPosition);
        $afterMarkString  = substr($url, $markPosition + 1);
        $afterMarkString  = str_replace($parameters, '', $afterMarkString);
        
        if(empty($afterMarkString))
        {
            return $beforeMarkString;
        }
        return $beforeMarkString .'?'. trim($afterMarkString, '&');
    }
}
