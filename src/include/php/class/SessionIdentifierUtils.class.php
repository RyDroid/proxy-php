<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class SessionIdentifierUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        generate() : string
    {
        return hash('sha512', random_bytes(64));
    }
    
    /**
     * @SuppressWarnings(PHPMD.Superglobals)
     */
    public static function
        initialize()
    {
        $value = self::generate();
        setcookie('session-identifier', $value);
        session_start();
        $_SESSION['session-identifier'] = $value;
    }
    
    /**
     * @SuppressWarnings(PHPMD.Superglobals)
     */
    public static function
        get() : string
    {
        if(isset($_SESSION) &&
           isset($_SESSION['session-identifier']) &&
           !empty($_SESSION['session-identifier']))
        {
            return $_SESSION['session-identifier'];
        }
        if(isset($_COOKIE) &&
           isset($_COOKIE['session-identifier']) &&
           !empty($_COOKIE['session-identifier']))
        {
            return $_COOKIE['session-identifier'];
        }
        return '';
    }
    
    public static function
        isInitialized() : bool
    {
        return !empty(self::get());
    }
    
    public static function
        initializeIfNeeded()
    {
        if(!self::isInitialized())
        {
            self::initialize();
        }
    }
    
    public static function
        deletePotentialFile() :  bool
    {
        if(SessionCapturedConfigurationGeneral::hasFilePath())
        {
            $filePath = SessionCapturedConfigurationGeneral::getFilePath();
            if(file_exists($filePath))
            {
                return unlink($filePath);
            }
        }
        return false;
    }
    
    /**
     * @SuppressWarnings(PHPMD.Superglobals)
     */
    public static function
        destroyIfNeeded()
    {
        self::deletePotentialFile();
        
        if(isset($_SESSION) &&
           !empty($_SESSION) &&
           isset($_SESSION['session-identifier']))
        {
            unset($_SESSION['session-identifier']);
            if(count($_SESSION) == 1)
            {
                session_destroy();
            }
        }
        
        if(isset($_COOKIE) &&
           !empty($_COOKIE) &&
           isset($_COOKIE['session-identifier']))
        {
            unset($_COOKIE['session-identifier']);
            setcookie('session-identifier', '', time() - 3600);
        }
    }
}
