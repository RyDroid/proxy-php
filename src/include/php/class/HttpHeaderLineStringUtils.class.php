<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class HttpHeaderLineStringUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        getPair(string $line) : Pair
    {
        $line = trim($line);
        if($line == '')
        {
            return new Pair();
        }
        
        $position = strpos($line, ':');
        if($position === FALSE)
        {
            return new Pair();
        }
        
        $key = substr($line, 0, $position);
        $key = trim($key);
        if($key == '')
        {
            return new Pair();
        }
        
        $value = substr($line, $position + 1);
        $value = trim($value);
        if($value == '')
        {
            return new Pair();
        }

        return Pair::create($key, $value);
    }
    
    public static function
        getHttpHeaderLine(string $line) : HttpHeaderLine
    {
        $pair = self::getPair($line);
        return HttpHeaderLine::createFromPair($pair);
    }
}
