<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


class HttpHeaderLine
{
    private $pair;
    
    
    public function
        __construct()
    {
        $this->pair = Pair::create('', '');
    }
    
    public static function
        createNull() : HttpHeaderLine
    {
        return new HttpHeaderLine();
    }
    
    public static function
        createFromString(string $line) : HttpHeaderLine
    {
        $pair = HttpHeaderLineStringUtils::getPair($line);
        return HttpHeaderLine::createFromPair($pair);
    }
    
    public static function
        createFromPair(Pair $pair) : HttpHeaderLine
    {
        $line = new HttpHeaderLine();
        if(is_string($pair->first) && is_string($pair->second))
        {
            $line->pair = $pair;
            $line->pair->first  = trim($line->pair->first);
            $line->pair->second = trim($line->pair->second);
        }
        return $line;
    }
    
    public static function
        createFromArray(array $anArray) : HttpHeaderLine
    {
        $pair = Pair::createEmpty();
        if(isset($anArray[0]) && isset($anArray[1]))
        {
            $pair->first  = $anArray[0];
            $pair->second = $anArray[1];
        }
        else if(isset($anArray['first']) && isset($anArray['second']))
        {
            $pair->first  = $anArray['first'];
            $pair->second = $anArray['second'];
        }
        else if(isset($anArray['key']) && isset($anArray['value']))
        {
            $pair->first  = $anArray['key'];
            $pair->second = $anArray['value'];
        }
        return self::createFromPair($pair);
    }
    
    
    public function
        isNull() : bool
    {
        return $this->pair->first == '' || $this->pair->second == '';
    }
    
    public function
        getKey() : string
    {
        return $this->pair->first;
    }
    
    public function
        getValue() : string
    {
        return $this->pair->second;
    }
    
    public function
        setValue(string $newValue) : bool
    {
        $newValue = trim($newValue);
        if($newValue == '')
        {
            return false;
        }
        $this->pair->second = $newValue;
        return true;
    }
    
    
    public function
        __toString() : string
    {
        if($this->isNull())
        {
            return '';
        }
        return $this->pair->first .': '. $this->pair->second;
    }
}
