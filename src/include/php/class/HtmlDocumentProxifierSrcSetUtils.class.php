<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class HtmlDocumentProxifierSrcSetUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        proxifySrcSetStringComponent(UrlProxifier $proxifier, string $source)
        : string
    {
        if(empty(trim($source)))
        {
            return $source;
        }
        
        // Split by last space and trim
        $tmp = strrpos($source, ' ');
        if($tmp === FALSE)
        {
            $sourceProxified = $proxifier->proxifyUrl($source);
            return $sourceProxified;
        }
        $components = str_split($source, $tmp);
        foreach($components as $key => $value)
        {
            $newValue = trim($value);
            $components[$key] = $newValue;
        }
        
        // First component of the split components string should be an image URL
        // So it has to be proxified
        $components[0] = $proxifier->proxifyUrl($components[0]);
        
        // Recombine the components into a single source
        return implode($components, ' ');
    }
    
    public static function
        proxifySrcSetArrayReference(UrlProxifier $proxifier, array& $sources)
    {
        foreach($sources as $key => $value)
        {
            $valueProxified = self::proxifySrcSetStringComponent(
                $proxifier, $value
            );
            $sources[$key] = $valueProxified;
        }
    }
    
    public static function
        proxifySrcSetArray(UrlProxifier $proxifier, array $sources) : array
    {
        $proxifiedSources = $sources;
        self::proxifySrcSetArrayReference($proxifier, $proxifiedSources);
        return $proxifiedSources;
    }
    
    public static function
        proxifySrcSetString(UrlProxifier $proxifier, string $srcset) : string
    {
        // Split all contents by comma and trim each value
        $sources = array_map('trim', explode(',', $srcset));
        
        $proxifiedSources = self::proxifySrcSetArray($proxifier, $sources);
        
        // Recombine the sources into a single "srcset"
        $proxifiedSrcset = implode(', ', $proxifiedSources);
        return $proxifiedSrcset;
    }
    
    public static function
        proxifyElement(UrlProxifier $proxifier, DOMElement& $element) : bool
    {
        if(!$element->hasAttribute('srcset'))
        {
            return false;
        }
        
        $valueOrigin = $element->getAttribute('srcset');
        if(empty($valueOrigin))
        {
            return false;
        }
        
        $valueOriginTrimmed = trim($valueOrigin);
        if(empty($valueOriginTrimmed))
        {
            return false;
        }
        
        $newValue = self::proxifySrcSetString($proxifier, $valueOriginTrimmed);
        if(empty($newValue))
        {
            return false;
        }
        
        $element->setAttribute('srcset', $newValue);
        return true;
    }
    
    public static function
        proxifyNodeList(UrlProxifier $proxifier, DOMNodeList& $elements) : int
    {
        $nbModified = 0;
        foreach($elements as $element)
        {
            if(self::proxifyElement($proxifier, $element))
            {
                ++$nbModified;
            }
        }
        return $nbModified;
    }
}
