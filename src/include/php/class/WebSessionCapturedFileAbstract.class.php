<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


abstract class WebSessionCapturedFileAbstract
    extends WebSessionCapturedAbstract
{
    private $filePath;
    
    
    public function
        __construct(string $filePath)
    {
        $this->setFilePath($filePath);
    }
    
    public function
        __destruct()
    {
        $this->filePath = null;
    }
    
    
    public function
        getDirectory() : string
    {
        return UrlStringParseUtils::getWithoutFile($this->filePath);
    }
    
    public function
        getFilePath() : string
    {
        return $this->filePath;
    }
    
    public function
        setFilePath(string $filePath)
    {
        if($this->filePath != $filePath)
        {
            $this->unlockFile();
            $this->filePath = $filePath;
        }
    }
    
    
    public function
        createDirectoriesForFile() : bool
    {
        umask(0);
        $directoryName = $this->getDirectory();
        if(!mkdir($directoryName, 0744))
        {
            echo 'ERROR: Impossible to create directory '. $directoryName;
            return false;
        }
        return true;
    }
    
    public function
        createDirectoriesForFileIfNeeded() : bool
    {
        $directoryName = $this->getDirectory();
        if(!file_exists($directoryName))
        {
            return $this->createDirectoriesForFile();
        }
        return true;
    }
}
