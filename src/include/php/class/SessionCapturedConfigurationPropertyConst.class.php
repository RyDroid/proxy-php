<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class SessionCapturedConfigurationPropertyConst
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    const SAVE_WITHOUT_SECTION_KEYS = [
        'save-session',                 'session-save',
        'has-to-save-session',          'must-save-session',
        'save-session-captured',        'session-captured-save',
        'has-to-save-session-captured', 'must-save-session-captured',
    ];
    
    const CREATE_AUTO_WITHOUT_SECTION_KEYS = [
        'session-create-auto', 'session-create-automatically',
        'session-autocreate',  'session-auto-create'
    ];
    
    const DIRECTORY_WITHOUT_SECTION_KEYS = [
        'session-captured-directory', 'session-captured-dir',
        'session-captured-folder'
    ];
    
    const SAVE_COOKIES_KEYS = [
        'save-cookies',      'session-save-cookies',
        'save-http-cookies', 'save-HTTP-cookies',
        'http-save-cookies', 'HTTP-save-cookies'
    ];
    
    const SAVE_USER_AGENT_KEYS = [
        'save-user-agent',      'save-User-Agent',
        'save-http-user-agent', 'save-HTTP-user-agent',
        'http-save-user-agent', 'HTTP-save-user-agent',
        'session-save-user-agent'
    ];
    
    const SAVE_REFERER_KEYS = [
        'save-referer',      'save-Referer',
        'save-http-referer', 'save-http-Referer',
        'save-HTTP-referer', 'save-HTTP-Referer',
        'session-save-referer'
    ];
    
    const SAVE_LANGUAGE_KEYS = [
        'save-language',      'session-save-language',
        'save-http-language', 'save-HTTP-language',
        'http-save-language', 'HTTP-save-language'
    ];
    
    const SAVE_ENCODING_KEYS = [
        'save-encoding',      'session-save-encoding',
        'save-http-encoding', 'save-HTTP-encoding',
        'http-save-encoding', 'HTTP-save-encoding'
    ];
    
    const SAVE_ETAG_KEYS = [
        'save-etag', 'save-ETag', 'session-save-etag', 'session-save-ETag',
        'save-http-etag', 'save-http-ETag', 'save-HTTP-etag', 'save-HTTP-ETag',
        'http-save-etag', 'http-save-ETag', 'HTTP-save-etag', 'HTTP-save-ETag'
    ];
    
    const SAVE_DO_NOT_TRACK_KEYS = [
        'session-save-http-dnt',
        'session-save-http-DNT',     'session-save-HTTP-DNT',
        'session-save-do-not-track', 'session-save-Do-Not-Track',
        'save-do-not-track',         'save-Do-Not-Track',
        'save-http-do-not-track',    'save-http-Do-Not-Track',
        'save-HTTP-do-not-track',    'save-HTTP-Do-Not-Track',
        'save-donottrack',           'save-DoNotTrack',
        'save-http-dnt', 'save-http-DNT', 'save-HTTP-dnt', 'save-HTTP-DNT',
        'http-dnt-save', 'http-DNT-save', 'HTTP-dnt-save', 'HTTP-DNT-save'
    ];
}
