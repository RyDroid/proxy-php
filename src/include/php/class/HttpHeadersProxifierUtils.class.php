<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class HttpHeadersProxifierUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        proxifyHttpHeaders(HttpHeaders& $headers) : int
    {
        $proxifier = new HttpHeadersProxifier();
        return $proxifier->proxifyHttpHeaders($headers);
    }
    
    public static function
        prepareToSend(HttpHeaders& $headers)
    {
        $headers->removeKey('Transfer-Encoding');
        $headers->removeKey('Content-Encoding');
        if(DebugUtils::isOn())
        {
            $headers->addStringLine(
                'URL-to-proxify: '.
                ProxifierGetParametersDefaultUtils::
                getUrlToProxifyWithEncodedParameters()
            );
        }
    }
    
    public static function
        sendPreparedProxifiedHttpHeaders(HttpHeaders $headers) : int
    {
        $nbModified = HttpHeadersProxifierUtils::proxifyHttpHeaders($headers);
        HttpHeadersUtils::send($headers);
        return $nbModified;
    }
    
    public static function
        sendProxifiedHttpHeaders(HttpHeaders $headers) : int
    {
        HttpHeadersProxifierUtils::prepareToSend($headers);
        return self::sendPreparedProxifiedHttpHeaders($headers);
    }
}
