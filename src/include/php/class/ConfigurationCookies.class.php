<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class ConfigurationCookies
    extends ConfigurationAbstract
{
    public function
        getProxy() : ProxyConfigurationAbstract
    {
        return new ProxyConfigurationCookies();
    }
    
    public function
        getSessionCaptured() : SessionCapturedConfigurationAbstract
    {
        return new SessionCapturedConfigurationCookies();
    }
    
    public function
        getLogs() : LogsConfigurationAbstract
    {
        return new LogsConfigurationCookies();
    }
    
    public function
        getUserInterface() : UserInterfaceConfigurationAbstract
    {
        return new UserInterfaceConfigurationNull();
    }
}
