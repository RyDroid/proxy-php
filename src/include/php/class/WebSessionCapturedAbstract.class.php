<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


abstract class WebSessionCapturedAbstract
{
    public function
        addUrl(string $url) : bool
    {
        $metadata = new ProxifyResultMetaData($url);
        return $this->addMetaData($metadata);
    }
    
    public abstract function
        addMetaData(ProxifyResultMetaData $metadata) : array;
    
    public function
        addProxifyResult(ProxifyResultAbstract $result) : array
    {
        return $this->addMetaData($result->getMetaData());
    }
    
    
    /**
     * @SuppressWarnings(PHPMD.ExitExpression)
     */
    public function
        addUrlOrExit(string $url, int $errorCode = 1)
    {
        if(!$this->addUrl($url))
        {
            exit($errorCode);
        }
    }
    
    /**
     * @SuppressWarnings(PHPMD.ExitExpression)
     */
    public function
        addMetaDataOrExit(ProxifyResultMetaData $metadata,
                          int $errorCode = 1)
    {
        if(!empty($this->addMetaData($metadata)))
        {
            exit($errorCode);
        }
    }
    
    /**
     * @SuppressWarnings(PHPMD.ExitExpression)
     */
    public function
        addProxifyResultOrExit(ProxifyResultAbstract $result,
                               int $errorCode = 1)
    {
        if(!empty($this->addProxifyResult($result)))
        {
            exit($errorCode);
        }
    }
}
