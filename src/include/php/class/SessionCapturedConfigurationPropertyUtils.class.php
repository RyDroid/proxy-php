<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class SessionCapturedConfigurationPropertyUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        getSaveFromMapWithoutSection(array $aMap) : string
    {
        return (
            GenericConfigurationUtils::
            getPotentielBooleanExtendedValueFromKeysOfMap(
                $aMap,
                SessionCapturedConfigurationPropertyConst::
                SAVE_WITHOUT_SECTION_KEYS
            )
        );
    }
    
    public static function
        getSaveFromMapInSection(array $aMap) : string
    {
        $potentialResult = (
            GenericConfigurationUtils::
            getPotentielBooleanExtendedValueFromKeysOfMap(
                $aMap,
                GenericConfigurationPropertyConst::SAVE_KEYS
            )
        );
        if(!empty($potentialResult))
        {
            return $potentialResult;
        }
        return self::getSaveFromMapWithoutSection($aMap);
    }
    
    public static function
        getCreateAutomaticallyFromMapWithoutSection(array $aMap) : string
    {
        return (
            GenericConfigurationUtils::
            getPotentielBooleanExtendedValueFromKeysOfMap(
                $aMap,
                SessionCapturedConfigurationPropertyConst::
                CREATE_AUTO_WITHOUT_SECTION_KEYS
            )
        );
    }
    
    public static function
        getCreateAutomaticallyFromMapInSection(array $aMap) : string
    {
        $potentialResult = (
            GenericConfigurationUtils::
            getPotentielBooleanExtendedValueFromKeysOfMap(
                $aMap,
                GenericConfigurationPropertyConst::CREATE_AUTO_KEYS
            )
        );
        if(!empty($potentialResult))
        {
            return $potentialResult;
        }
        return self::getSaveFromMapWithoutSection($aMap);
    }
    
    public static function
        getDirectoryFromMapWithoutSection(array $aMap) : string
    {
        if(GenericConfigurationUtils::
           getPotentielBooleanExtendedValueFromKeysOfMap(
               $aMap,
               array('session-captured-use-tmp',
                     'session-captured-use-tmp-dir',
                     'session-captured-use-temp-dir',
                     'session-captured-use-tmp-directory',
                     'session-captured-use-temp-directory',
                     'session-captured-use-sys-tmp',
                     'session-captured-use-sys-tmp-dir'
               )
           )
        )
        {
            return FileSystemUtils::getSystemTemporaryDirectory();
        }
        return (
            GenericConfigurationUtils::
            getPotentielValueFromKeysOfMap(
                $aMap,
                SessionCapturedConfigurationPropertyConst::
                DIRECTORY_WITHOUT_SECTION_KEYS
            )
        );
    }
    
    public static function
        getDirectoryFromMapInSection(array $aMap) : string
    {
        if(ArrayUtils::keyExistsWithNotEmptyValue($aMap, 'directory'))
        {
            return $aMap['directory'];
        }
        if(ArrayUtils::keyExistsWithNotEmptyValue($aMap, 'dir'))
        {
            return $aMap['dir'];
        }
        if(ArrayUtils::keyExistsWithNotEmptyValue($aMap, 'folder'))
        {
            return $aMap['folder'];
        }
        if(GenericConfigurationUtils::isOkAccordingToKeyOfMap($aMap, 'use-tmp'))
        {
            return FileSystemUtils::getSystemTemporaryDirectory();
        }
        return self::getDirectoryFromMapWithoutSection($aMap);
    }
    
    public static function
        getSaveProxyFromMapWithoutSection(array $aMap) : string
    {
        if(GenericConfigurationUtils::
           isBooleanExtendedAccordingToKeyOfMap($aMap, 'save-proxy'))
        {
            return $aMap['save-proxy'];
        }
        if(GenericConfigurationUtils::
           isBooleanExtendedAccordingToKeyOfMap($aMap, 'save-proxy-conf'))
        {
            return $aMap['save-proxy-conf'];
        }
        return '';
    }
    
    public static function
        getSaveCookiesFromMapWithoutSection(array $aMap) : string
    {
        return (
            GenericConfigurationUtils::
            getPotentielBooleanExtendedValueFromKeysOfMap(
                $aMap,
                SessionCapturedConfigurationPropertyConst::SAVE_COOKIES_KEYS
            )
        );
    }
    
    public static function
        getSaveUserAgentFromMapWithoutSection(array $aMap) : string
    {
        return (
            GenericConfigurationUtils::
            getPotentielBooleanExtendedValueFromKeysOfMap(
                $aMap,
                SessionCapturedConfigurationPropertyConst::SAVE_USER_AGENT_KEYS
            )
        );
    }
    
    public static function
        getSaveRefererFromMapWithoutSection(array $aMap) : string
    {
        return (
            GenericConfigurationUtils::
            getPotentielBooleanExtendedValueFromKeysOfMap(
                $aMap,
                SessionCapturedConfigurationPropertyConst::SAVE_REFERER_KEYS
            )
        );
    }
    
    public static function
        getSaveLanguageFromMapWithoutSection(array $aMap) : string
    {
        return (
            GenericConfigurationUtils::
            getPotentielBooleanExtendedValueFromKeysOfMap(
                $aMap,
                SessionCapturedConfigurationPropertyConst::SAVE_LANGUAGE_KEYS
            )
        );
    }
    
    public static function
        getSaveEncodingFromMapWithoutSection(array $aMap) : string
    {
        return (
            GenericConfigurationUtils::
            getPotentielBooleanExtendedValueFromKeysOfMap(
                $aMap,
                SessionCapturedConfigurationPropertyConst::SAVE_ENCODING_KEYS
            )
        );
    }
    
    public static function
        getSaveEtagFromMapWithoutSection(array $aMap) : string
    {
        return (
            GenericConfigurationUtils::
            getPotentielBooleanExtendedValueFromKeysOfMap(
                $aMap,
                SessionCapturedConfigurationPropertyConst::SAVE_ETAG_KEYS
            )
        );
    }
    
    public static function
        getSaveDoNotTrackFromMapWithoutSection(array $aMap) : string
    {
        return (
            GenericConfigurationUtils::
            getPotentielBooleanExtendedValueFromKeysOfMap(
                $aMap,
                SessionCapturedConfigurationPropertyConst::
                SAVE_DO_NOT_TRACK_KEYS
            )
        );
    }
}
