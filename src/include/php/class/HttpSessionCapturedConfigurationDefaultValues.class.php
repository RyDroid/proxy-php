<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class HttpSessionCapturedConfigurationDefaultValues
    extends HttpSessionCapturedConfigurationAbstract
{
    const SAVE_COOKIES      = OptionnalBooleanConst::FALSE;
    const SAVE_USER_AGENT   = OptionnalBooleanConst::TRUE;
    const SAVE_REFERER      = OptionnalBooleanConst::TRUE;
    const SAVE_LANGUAGE     = OptionnalBooleanConst::TRUE;
    const SAVE_ENCODING     = OptionnalBooleanConst::TRUE;
    const SAVE_ETAG         = OptionnalBooleanConst::TRUE;
    const SAVE_DO_NOT_TRACK = OptionnalBooleanConst::TRUE;
    
    
    public function
        hasToSaveCookies() : OptionnalBoolean
    {
        return new OptionnalBoolean(self::SAVE_COOKIES);
    }
    
    public function
        hasToSaveUserAgent() : OptionnalBoolean
    {
        return new OptionnalBoolean(self::SAVE_USER_AGENT);
    }
    
    public function
        hasToSaveReferer() : OptionnalBoolean
    {
        return new OptionnalBoolean(self::SAVE_REFERER);
    }
    
    public function
        hasToSaveLanguage() : OptionnalBoolean
    {
        return new OptionnalBoolean(self::SAVE_LANGUAGE);
    }
    
    public function
        hasToSaveEncoding() : OptionnalBoolean
    {
        return new OptionnalBoolean(self::SAVE_ENCODING);
    }
    
    public function
        hasToSaveEtag() : OptionnalBoolean
    {
        return new OptionnalBoolean(self::SAVE_ETAG);
    }
    
    public function
        hasToSaveDoNotTrack() : OptionnalBoolean
    {
        return new OptionnalBoolean(self::SAVE_DO_NOT_TRACK);
    }
}
