<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


abstract class ProxyConfigurationAbstract
{
    public function
        isNull() : bool
    {
        return !$this->hasUrl();
    }
    
    public function
        hasUrl() : bool
    {
        return trim($this->getUrl()) != '';
    }
    
    public abstract function
        getUrl() : string;
    
    public abstract function
        getPort() : int;
    
    public function
        hasPort() : bool
    {
        return $this->getPort() >= 0;
    }
    
    public function
        getFullUrl() : string
    {
        if($this->hasPort())
        {
            return $this->getUrl() .':'. $this->getPort();
        }
        return $this->getUrl();
    }
    
    public function
        __toString() : string
    {
        return $this->getFullUrl();
    }
}
