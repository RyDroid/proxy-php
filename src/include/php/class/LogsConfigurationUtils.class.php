<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class LogsConfigurationUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        createNull()
        : LogsConfigurationMutableAbstract
    {
        return new LogsConfiguration(
            '',
            OptionnalBooleanUtils::createUndefined(),
            OptionnalBooleanUtils::createUndefined()
        );
    }
    
    public static function
        createDefault()
        : LogsConfigurationAbstract
    {
        return new LogsConfiguration(
            LoggerUtils::getLogFilePath(),
            new OptionnalBoolean(LogsConfigurationDefaultValues::SAVE),
            new OptionnalBoolean(LogsConfigurationDefaultValues::SHOW)
        );
    }
    
    
    public static function
        createFromMapWithoutSection(array $aMap)
        : LogsConfigurationAbstract
    {
        $configuration = self::createNull();
        
        $save = LogsConfigurationPropertyUtils::getSaveFromMapWithoutSection(
            $aMap
        );
        if(!empty($save))
        {
            $configuration->setSaveWithString($save);
        }
        
        $filePath = (
            LogsConfigurationPropertyUtils::getFilePathFromMapWithoutSection(
                $aMap
            )
        );
        if(!empty($filePath))
        {
            $configuration->setFilePath($filePath);
        }
        
        $fileName = (
            LogsConfigurationPropertyUtils::getFileNameFromMapWithoutSection(
                $aMap
            )
        );
        if(!empty($fileName))
        {
            $configuration->setBaseName($fileName);
        }
        
        $directory = (
            LogsConfigurationPropertyUtils::getDirectoryFromMapWithoutSection(
                $aMap
            )
        );
        if(!empty($directory))
        {
            $configuration->setDirectoryName($directory);
        }
        
        $format = (
            LogsConfigurationPropertyUtils::
            getFormatAsStringFromMapWithoutSection(
                $aMap
            )
        );
        if(!empty($format))
        {
            $configuration->setFormatAsString($format);
        }
        
        $configuration->setShowWithString(
            LogsConfigurationPropertyUtils::
            getShowFromMapWithoutSection($aMap)
        );
        
        return $configuration;
    }
    
    public static function
        createFromMapInSection(array $aMap)
        : LogsConfigurationAbstract
    {
        $configuration = self::createNull();
        
        $save = LogsConfigurationPropertyUtils::getSaveFromMapInSection($aMap);
        if(!empty($save))
        {
            $configuration->setSaveWithString($save);
        }
        
        $filePath = (
            LogsConfigurationPropertyUtils::getFilePathFromMapInSection($aMap)
        );
        if(!empty($filePath))
        {
            $configuration->setFilePath($filePath);
        }
        
        $fileName = (
            LogsConfigurationPropertyUtils::getFileNameFromMapInSection($aMap)
        );
        if(!empty($fileName))
        {
            $configuration->setBaseName($fileName);
        }
        
        $directory = (
            LogsConfigurationPropertyUtils::getDirectoryFromMapInSection($aMap)
        );
        if(!empty($directory))
        {
            $configuration->setDirectoryName($directory);
        }
        
        $format = (
            LogsConfigurationPropertyUtils::
            getFormatAsStringFromMapInSection($aMap)
        );
        if(!empty($format))
        {
            $configuration->setFormatAsString($format);
        }

        $configuration->setShowWithString(
            LogsConfigurationPropertyUtils::
            getShowFromMapInSection($aMap)
        );
        
        return $configuration;
    }
    
    public static function
        createFromMap(array $aMap) : LogsConfigurationAbstract
    {
        if(ArrayUtils::keyExistsWithNotEmptyValue($aMap, 'logs'))
        {
            return self::createFromMapInSection($aMap['logs']);
        }
        if(ArrayUtils::keyExistsWithNotEmptyValue($aMap, 'log'))
        {
            return self::createFromMapInSection($aMap['log']);
        }
        return self::createFromMapWithoutSection($aMap);
    }
    
    public static function
        createFromFilePath(string $filePath) : LogsConfigurationAbstract
    {
        $anArray = IniFileUtils::parseFromFilePathWithSections($filePath);
        $configuration = self::createFromMap($anArray);
        return $configuration;
    }
}
