<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class ProxyConfigurationGeneral
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    private static $conf = null;
    
    
    public static function
        loadForce()
    {
        self::$conf = new ProxyConfigurationOrderedList(
            array(
                new ProxyConfigurationGetParameters(),
                new ProxyConfigurationCookies(),
                new ProxyConfigurationSession(),
                ProxyConfigurationDefaultFile::get(),
                ConfigurationDefaultFile::getProxy(),
                ConfigurationDefaultSystemFile::get()->getProxy()
            )
        );
    }
    
    public static function
        load() : bool
    {
        if(self::$conf == null)
        {
            self::loadForce();
        }
        return self::$conf != null && count(self::$conf) > 0;
    }
    
    
    public static function
        get() : ProxyConfigurationAbstract
    {
        self::load();
        return self::$conf;
    }
    
    public static function
        isNull() : bool
    {
        return self::get() == null || self::get()->isNull();
    }
    
    public static function
        getUrl() : string
    {
        return self::get()->getUrl();
    }
    
    public static function
        hasUrl() : bool
    {
        return self::get()->hasUrl();
    }
    
    public static function
        getPort() : int
    {
        return self::get()->getPort();
    }
    
    public static function
        hasPort() : bool
    {
        return self::get()->hasPort();
    }
    
    public static function
        getFullUrl() : string
    {
        return self::get()->getFullUrl();
    }
}
