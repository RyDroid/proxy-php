<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class CssLineProxifierUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        proxifyLine($proxifier, string $lineOrigin) : string
    {
        $result = new StringProxifiedPartly($lineOrigin, 0, 0);
        do
        {
            $result = $proxifier->proxifyLinePartly(
                $result->string, $result->offset
            );
        }
        while($result->offset > 0);
        return $result->string;
    }
    
    public static function
        proxifyString($proxifier, string $cssOrigin) : string
    {
        $cssOriginTrimmed = trim($cssOrigin);
        if($cssOriginTrimmed == '' || $cssOriginTrimmed == '/**/')
        {
            return $cssOrigin;
        }
        $cssOriginTrimmed = null;
        
        $cssProxified = '';
        # TODO catch and reput the same newline character
        foreach(preg_split("/((\r?\n)|(\r\n?))/", $cssOrigin) as $lineOrigin)
        {
            $lineProxified = self::proxifyLine($proxifier, $lineOrigin);
            $cssProxified .= $lineProxified . PHP_EOL;
        }
        return $cssProxified;
    }
}
