<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class UrlProxifier
{
    private $url;
    private $baseProxifiedUrl;
    
    
    public function
        __construct()
    {
        $this->url = UrlProxifierDefaultUtils::getUrl();
        $this->baseProxifiedUrl = (
            UrlProxifierDefaultUtils::getBaseProxifiedUrl()
        );
    }
    
    
    public function
        hasUrl() : bool
    {
        return trim($this->getUrl()) != '';
    }
    
    public function
        getUrl() : string
    {
        return $this->url;
    }
    
    public function
        setUrl(string $newUrl) : bool
    {
        $newUrl = trim($newUrl);
        if($newUrl == '')
        {
            return false;
        }
        $this->url = $newUrl;
        return true;
    }
    
    public function
        getBaseProxifiedUrl() : string
    {
        return $this->baseProxifiedUrl;
    }
    
    public function
        setBaseProxifiedUrl(string $newUrl) : bool
    {
        $newUrl = trim($newUrl);
        if($newUrl == '')
        {
            return false;
        }
        $this->baseProxifiedUrl = $newUrl;
        return true;
    }
    
    public function
        getAbsoluteUrl(string $url) : string
    {
        return UrlStringUtils::getAbsolute(
            $this->baseProxifiedUrl, $url
        );
    }
    
    public function
        getAbsoluteUrlEncoded(string $url) : string
    {
        $absoluteUrl = $this->getAbsoluteUrl($url);
        return UrlProxifierCodecUtils::getEncodedUrl($absoluteUrl);
    }
    
    
    public function
        proxifyAbsoluteUrlNoCheck(string $urlToProxify) : string
    {
        $urlProxified = (
            $this->getUrl() . '?'.
            ProxifierGetParametersUtils::DEFAULT_KEY_TO_PROXIFY
            .'='. $urlToProxify
        );
        $urlProxified = UrlProxifierProxyUtils::addParametersToUrl(
            $urlProxified
        );
        return $urlProxified;
    }
    
    public function
        proxifyUrlNoCheck(string $urlToProxify) : string
    {
        $urlAbsoluteEncoded = $this->getAbsoluteUrlEncoded($urlToProxify);
        $urlProxified = $this->proxifyAbsoluteUrlNoCheck($urlAbsoluteEncoded);
        return $urlProxified;
    }
    
    public function
        proxifyUrl(string $urlToProxify) : string
    {
        if(trim($urlToProxify) == '' ||
           StringUtils::startsWith($urlToProxify, $this->getUrl()))
        {
            return $urlToProxify;
        }
        $urlProxified = $this->proxifyUrlNoCheck($urlToProxify);
        return $urlProxified;
    }
    
    public function
        unproxifyUrl(string $url) : string
    {
        if(!StringUtils::startsWith($url, $this->getUrl()))
        {
            return $url;
        }
        $urlQueryString = parse_url($url, PHP_URL_QUERY);
        $url = ProxifierGetParametersUtils::getUrlToProxifyWithParamsFromArray(
            $urlQueryString
        );
        return $url;
    }
}
