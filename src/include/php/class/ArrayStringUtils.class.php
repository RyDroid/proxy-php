<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class ArrayStringUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        removeEmpty(array& $anArray)
    {
        $size = count($anArray);
        for($index = 0; $index < $size; ++$index)
        {
            $item = $anArray[$index];
            if($item == '')
            {
                array_splice($anArray, $index, 1);
            }
        }
    }
    
    public static function
        removeEmptyAfterTrim(array& $anArray)
    {
        $size = count($anArray);
        for($index = 0; $index < $size; ++$index)
        {
            $item = $anArray[$index];
            $item = trim($item);
            if($item == '')
            {
                array_splice($anArray, $index, 1);
            }
        }
    }
}
