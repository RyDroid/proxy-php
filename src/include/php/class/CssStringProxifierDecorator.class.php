<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


abstract class CssStringProxifierDecorator
    extends CssStringProxifierAbstract
{
    private $previousProxy = null;
    
    public function
        __construct(CssStringProxifierAbstract $previousProxy = null)
    {
        parent::__construct();
        $this->previousProxy = $previousProxy;
    }
    
    public function
        setUrl(string $url) : bool
    {
        if($this->previousProxy == null)
        {
            return parent::setUrl($url);
        }
        return parent::setUrl($url) && $this->previousProxy->setUrl($url);
    }
    
    public function
        proxifyLine(string $lineOrigin) : string
    {
        if($this->previousProxy == null)
        {
            return $lineOrigin;
        }
        $line = $this->previousProxy->proxifyLine($lineOrigin);
        return $line;
    }
    
    public function
        proxifyString(string $cssOrigin) : string
    {
        if($this->previousProxy == null)
        {
            return $cssOrigin;
        }
        $css = $this->previousProxy->proxifyString($cssOrigin);
        return $css;
    }
}
