<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class WebSessionCapturedXmlUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        addUrl(string $filePath, string $url, int $errorCode = 1) : bool
    {
        $session = new WebSessionCapturedXml($filePath);
        return $session->addUrl($url, $errorCode);
    }
    
    public static function
        addUrlOrExit(string $filePath, string $url, int $errorCode = 1)
    {
        $session = new WebSessionCapturedXml($filePath);
        $session->addUrlOrExit($url, $errorCode);
    }
    
    public static function
        addProxifyMetaDataOrExit(string $filePath,
                                 ProxifyResultMetaData $metadata,
                                 int $errorCode = 1)
    {
        $session = new WebSessionCapturedXml($filePath);
        $session->addProxifyMetaDataOrExit($metadata, $errorCode);
    }
    
    public static function
        addProxifyResult(string $filePath,
                         ProxifyResultAbstract $result)
        : array
    {
        $session = new WebSessionCapturedXml($filePath);
        return $session->addProxifyResult($result);
    }
    
    public static function
        addProxifyResultOrExit(string $filePath,
                               ProxifyResultAbstract $result,
                               int $errorCode = 1)
    {
        $session = new WebSessionCapturedXml($filePath);
        $session->addProxifyResultOrExit($result, $errorCode);
    }
}
