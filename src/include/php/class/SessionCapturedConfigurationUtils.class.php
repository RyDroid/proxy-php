<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class SessionCapturedConfigurationUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        setWithMapWithoutSection(
            SessionCapturedConfigurationMutableAbstract $configuration,
            array $aMap
        )
    {
        $configuration->setSaveFromString(
            SessionCapturedConfigurationPropertyUtils::
            getSaveFromMapWithoutSection($aMap)
        );
        
        $configuration->setCreateAutomaticallyFromString(
            SessionCapturedConfigurationPropertyUtils::
            getCreateAutomaticallyFromMapWithoutSection($aMap)
        );
        
        $directory = (
            SessionCapturedConfigurationPropertyUtils::
            getDirectoryFromMapWithoutSection($aMap)
        );
        if(!empty($directory))
        {
            $configuration->setDirectory($directory);
        }
        
        $configuration->setSaveProxyFromString(
            SessionCapturedConfigurationPropertyUtils::
            getSaveProxyFromMapWithoutSection($aMap)
        );
        $configuration->getMutableHttp()->setSaveCookiesFromString(
            SessionCapturedConfigurationPropertyUtils::
            getSaveCookiesFromMapWithoutSection($aMap)
        );
        $configuration->getMutableHttp()->setSaveUserAgentFromString(
            SessionCapturedConfigurationPropertyUtils::
            getSaveUserAgentFromMapWithoutSection($aMap)
        );
        $configuration->getMutableHttp()->setSaveRefererFromString(
            SessionCapturedConfigurationPropertyUtils::
            getSaveRefererFromMapWithoutSection($aMap)
        );
        $configuration->getMutableHttp()->setSaveLanguageFromString(
            SessionCapturedConfigurationPropertyUtils::
            getSaveLanguageFromMapWithoutSection($aMap)
        );
        $configuration->getMutableHttp()->setSaveEncodingFromString(
            SessionCapturedConfigurationPropertyUtils::
            getSaveEncodingFromMapWithoutSection($aMap)
        );
        $configuration->getMutableHttp()->setSaveEtagFromString(
            SessionCapturedConfigurationPropertyUtils::
            getSaveEtagFromMapWithoutSection($aMap)
        );
        $configuration->getMutableHttp()->setSaveDoNotTrackFromString(
            SessionCapturedConfigurationPropertyUtils::
            getSaveDoNotTrackFromMapWithoutSection($aMap)
        );
    }
    
    public static function
        createFromMapWithoutSection(array $aMap)
        : SessionCapturedConfigurationAbstract
    {
        $configuration = new SessionCapturedConfiguration();
        self::setWithMapWithoutSection($configuration, $aMap);
        return $configuration;
    }
    
    public static function
        createFromMap(array $aMap) : SessionCapturedConfigurationAbstract
    {
        if(isset($aMap['session-captured']) &&
           !empty($aMap['session-captured']))
        {
            $configuration = self::createFromMapWithoutSection(
                $aMap['session-captured']
            );
            $configuration->setSaveFromString(
                SessionCapturedConfigurationPropertyUtils::
                getSaveFromMapInSection($aMap['session-captured'])
            );
            $configuration->setCreateAutomaticallyFromString(
                SessionCapturedConfigurationPropertyUtils::
                getCreateAutomaticallyFromMapInSection(
                    $aMap['session-captured']
                )
            );
            $directory = (
                SessionCapturedConfigurationPropertyUtils::
                getDirectoryFromMapInSection(
                    $aMap['session-captured']
                )
            );
            if(!empty($directory))
            {
                $configuration->setDirectory($directory);
            }
            return $configuration;
        }
        return self::createFromMapWithoutSection($aMap);
    }
    
    public static function
        createFromFilePath(string $filePath)
        : SessionCapturedConfigurationAbstract
    {
        $anArray = IniFileUtils::parseFromFilePathWithSections($filePath);
        $configuration = self::createFromMap($anArray);
        return $configuration;
    }
}
