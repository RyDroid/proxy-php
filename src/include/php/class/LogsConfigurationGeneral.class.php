<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class LogsConfigurationGeneral
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    private static $conf = null;
    
    
    public static function
        loadForce()
    {
        self::$conf = new LogsConfigurationOrderedList(
            array(
                LogsConfigurationDefaultFile::get(),
                ConfigurationDefaultFile::getLogs(),
                ConfigurationDefaultSystemFile::get()->getLogs(),
                new LogsConfigurationDefaultValues()
            )
        );
    }
    
    public static function
        load() : bool
    {
        if(self::$conf == null)
        {
            self::loadForce();
        }
        return self::$conf != null && count(self::$conf) > 0;
    }
    
    public static function
        get() : LogsConfigurationAbstract
    {
        if(self::load())
        {
            return self::$conf;
        }
        return LogsConfigurationUtils::createNull();
    }
    
    
    public static function
        hasToSave() : bool
    {
        return self::get()->hasToSave()->isTrue();
    }
    
    public static function
        getFilePath() : string
    {
        return self::get()->getFilePath();
    }
    
    public static function
        canBeShown() : bool
    {
        return self::get()->canBeShown()->isTrue();
    }
}
