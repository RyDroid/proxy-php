<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


define('LOG_DEFAULT_DIRECTORY',      ABSOLUTE_PATH);
define('LOG_DEFAULT_FILE_NAME',      'logs');
define('LOG_DEFAULT_FILE_EXTENSION', 'txt');
define(
    'LOG_DEFAULT_BASE_NAME',
    LOG_DEFAULT_FILE_NAME .'.'. LOG_DEFAULT_FILE_EXTENSION
);
define(
    'LOG_DEFAULT_FILE_PATH',
    LOG_DEFAULT_DIRECTORY . LOG_DEFAULT_BASE_NAME
    
);


final class LoggerUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    private static $logFilePath = LOG_DEFAULT_FILE_PATH;
    
    
    public static function
        setDefaultLogFilePath()
    {
        self::$logFilePath = LOG_DEFAULT_FILE_PATH;
    }
    
    public static function
        setLogFilePath($path)
    {
        self::$logFilePath = $path;
    }
    
    public static function
        getLogFilePath()
    {
        return self::$logFilePath;
    }
    
    public static function
        createLogFileIfNeeded() : bool
    {
        if(file_exists(self::$logFilePath))
        {
            return true;
        }
        
        $fileHandler = NULL;
        try
        {
            $fileHandler = fopen(self::$logFilePath, 'a');
        }
        catch(Exception $exception)
        {
            return false;
        }
        if($fileHandler == FALSE || $fileHandler == NULL)
        {
            return false;
        }
        fclose($fileHandler);
        return true;
    }
    
    
    public static function
        logString(string $aString) : bool
    {
        if(empty(trim($aString)))
        {
            return false;
        }
        if(empty(self::$logFilePath))
        {
            self::setDefaultLogFilePath();
        }
        if(!self::createLogFileIfNeeded())
        {
            return false;
        }
        return error_log($aString, 3, self::$logFilePath);
    }
    
    public static function
        logStrings(array $strings) : int
    {
        $nbSuccess = 0;
        foreach($strings as $aString)
        {
            if(self::logString($aString))
            {
                ++$nbSuccess;
            }
        }
        return $nbSuccess;
    }
}
