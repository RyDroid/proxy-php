<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class WebSessionCapturedFileLockedDefault
{
    const FORCE_LOCK                = false;
    const LOCK_FILE_NAME_SUFFIX     = '.lock';
    const LOCK_FILE_NAME_SUFFIX_MIN =  2;
    
    
    private static $forceLock          = self::FORCE_LOCK;
    private static $lockFileNameSuffix = self::LOCK_FILE_NAME_SUFFIX;
    
    
    public static function
        isForcingLock()
    {
        return self::$forceLock;
    }
    
    public static function
        setForceLock(bool $value)
    {
        self::$forceLock = $value;
    }
    
    public static function
        getLockFileNameSuffix() : string
    {
        return self::$lockFileNameSuffix;
    }
    
    public static function
        setLockFileNameSuffix(string $value) : bool
    {
        if($value == null ||
           strlen($value) < self::LOCK_FILE_NAME_SUFFIX_MIN_LENGTH)
        {
            return false;
        }
        self::$lockFileNameSuffix = $value;
        return true;
    }
}
