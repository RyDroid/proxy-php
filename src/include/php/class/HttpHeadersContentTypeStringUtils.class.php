<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class HttpHeadersContentTypeStringUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        isCss(string $headers) : bool
    {
        foreach(preg_split("/((\r?\n)|(\r\n?))/", $headers) as $line)
        {
            $line = trim($line);
            if(StringUtils::startsWithInsensitive($line, 'content-type:'))
            {
                $line = trim(substr($line, strlen('content-type:')));
                return StringUtils::startsWithInsensitive($line, 'text/css');
            }
        }
        return false;
    }
}
