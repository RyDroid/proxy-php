<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class HtmlDocumentProxifierFormGet
    extends DomDocumentProxifierDecorator
{
    public function
        __construct(DomDocumentProxifierAbstract $previousProxy = null)
    {
        parent::__construct($previousProxy);
    }
    
    
    public function
        getXPathQuery() : string
    {
        return (
            '/html/body//form['.
            'not(@action) or'.
            '(string(@action) and ('.
            XPathUtils::lowerCase('@method').'=\'get\')'.
            ' or not(@method))]'
        );
    }
    
    public function
        getElements(DOMDocument $document) : DOMNodeList
    {
        $docXPath = new DOMXPath($document);
        $queryString = $this->getXPathQuery();
        $elements = $docXPath->query($queryString);
        return $elements;
    }
    
    private function
        getUrlToProxifyFromFormElement(DOMElement $element) : string
    {
        if(!$element->hasAttribute('action'))
        {
            return '';
        }
        
        $url = $element->getAttribute('action');
        if(empty($url))
        {
            return '';
        }
        
        $urlUnproxified = $this->unproxifyUrl($url);
        return $urlUnproxified;
    }
    
    private function
        proxifyElement(DOMElement& $element) : bool
    {
        $url = $this->getUrlToProxifyFromFormElement($element);
        if(empty($url))
        {
            return false;
        }
        $url = UrlProxifierDefaultUtils::getAbsoluteUrl($url);
        
        if(empty(ProxifierGetParametersUtils::KEYS_TO_PROXIFY))
        {
            return false;
        }
        $identifier = HtmlDocumentUtils::getFirstNotUsedIdentifier(
            $element->ownerDocument,
            ProxifierGetParametersUtils::KEYS_TO_PROXIFY
        );
        if(empty(trim($identifier)))
        {
            return false;
        }
        
        // TODO check that there is no input with the same name or id
        $inputElement = $element->ownerDocument->createElement('input');
        $inputElement->setAttribute('name', $identifier);
        $inputElement->setAttribute('id',   $identifier);
        $inputElement->setAttribute('type', 'hidden');
        $inputElement->setAttribute('value', $url);
        $element->appendChild($inputElement);
        
        $proxyUrl = ProxyConfigurationGetParametersUtils::getUrl();
        if(!empty(trim($proxyUrl)))
        {
            // TODO check that there is no input with the same name or id
            $inputElement = $element->ownerDocument->createElement('input');
            $inputElement->setAttribute(
                'name', ProxyConfigurationConst::DEFAULT_KEY_FOR_URL
            );
            $inputElement->setAttribute(
                'id',   ProxyConfigurationConst::DEFAULT_KEY_FOR_URL
            );
            $inputElement->setAttribute('type', 'hidden');
            $inputElement->setAttribute('value', $proxyUrl);
            $element->appendChild($inputElement);
            
            $proxyPort = ProxyConfigurationGetParametersUtils::getPort();
            if($proxyPort >= 0)
            {
                // TODO check that there is no input with the same name or id
                $inputElement = $element->ownerDocument->createElement('input');
                $inputElement->setAttribute(
                    'name', ProxyConfigurationConst::DEFAULT_KEY_FOR_PORT
                );
                $inputElement->setAttribute(
                    'id',   ProxyConfigurationConst::DEFAULT_KEY_FOR_PORT
                );
                $inputElement->setAttribute('type', 'hidden');
                $inputElement->setAttribute('value', $proxyPort);
                $element->appendChild($inputElement);
            }
        }
        
        return true;
    }
    
    public function
        proxifyNodeList(DOMNodeList& $elements) : int
    {
        $nbModified = 0;
        foreach($elements as $element)
        {
            if($this->proxifyElement($element))
            {
                ++$nbModified;
            }
        }
        return $nbModified;
    }
    
    /**
     * @return Number of modified elements, or -1 if a problem occured
     */
    public function
        proxifyDocument(DOMDocument $document) : int
    {
        $nbModified = parent::proxifyDocument($document);
        $elements = $this->getElements($document);
        $nbModified += $this->proxifyNodeList($elements);
        return $nbModified;
    }
}
