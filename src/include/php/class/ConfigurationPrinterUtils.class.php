<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class ConfigurationPrinterUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        printAsHtmlList(ConfigurationAbstract $configuration)
    {
        echo '<ul>'. PHP_EOL;
        
        echo "\t". '<li>';
        echo 'Proxy:';
        ProxyConfigurationPrinterUtils::printAsHtmlList(
            $configuration->getProxy(), "\t\t"
        );
        echo "\t". '</li>'. PHP_EOL;
        
        echo "\t". '<li>';
        echo 'Session captured:'. PHP_EOL;
        SessionCapturedConfigurationPrinterUtils::printAsHtmlList(
            $configuration->getSessionCaptured(), "\t\t"
        );
        echo "\t". '</li>'. PHP_EOL;
        
        echo "\t". '<li>';
        echo 'Logs: '. PHP_EOL;
        LogsConfigurationPrinterUtils::printAsHtmlList(
            $configuration->getLogs(), "\t\t"
        );
        echo "\t". '</li>'. PHP_EOL;
        
        echo "\t". '<li>';
        echo 'User Interface: '. PHP_EOL;
        UserInterfaceConfigurationPrinterUtils::printAsHtmlList(
            $configuration->getUserInterface(), "\t\t"
        );
        echo "\t". '</li>'. PHP_EOL;
        
        echo '</ul>'. PHP_EOL;
    }
}
