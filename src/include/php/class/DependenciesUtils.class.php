<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class DependenciesUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        messagesArrayToHtmlListItems(array $messages) : string
    {
        $html = '';
        foreach($messages as $message)
        {
            $item = '<li>'. $message .'</li>';
            $html .= $item;
        }
        return $html;
    }
    
    public static function
        messagesArrayToHtmlUnorderedList(array $messages) : string
    {
        $html = self::messagesArrayToHtmlListItems($messages);
        if(empty(trim($html)))
        {
            return $html;
        }
        $html = '<ul>'. $html .'</ul>';
        return $html;
    }
    
    public static function
        messagesArrayToHtmlOrderedList(array $messages) : string
    {
        $html = self::messagesArrayToHtmlListItems($messages);
        if(empty(trim($html)))
        {
            return $html;
        }
        $html = '<ol>'. $html .'</ol>';
        return $html;
    }
    
    
    public static function
        getErrorsAsArrayMessages() : array
    {
        $errors = array();
        if(!function_exists('mb_strtolower'))
        {
            $errors[] = 'No module for Multibyte String';
        }
        if(!class_exists('DOMDocument'))
        {
            $errors[] = 'No module to manipulate DOM';
        }
        return $errors;
    }
    
    public static function
        getErrorsAsHtmlUnorderedList() : string
    {
        $errors = self::getErrorsAsArrayMessages();
        $html = self::messagesArrayToHtmlUnorderedList($errors);
        return $html;
    }
    
    public static function
        getErrorsAsHtmlOrderedList() : string
    {
        $errors = self::getErrorsAsArrayMessages();
        $html = self::messagesArrayToHtmlOrderedList($errors);
        return $html;
    }
    
    public static function
        printErrorsAsHtmlUnorderedList()
    {
        echo self::getErrorsAsHtmlUnorderedList();
    }
    
    
    public static function
        getWarningsAsArrayMessages() : array
    {
        $warnings = array();
        if(!function_exists('curl_init'))
        {
            $warnings[] = 'No module for curl';
        }
        return $warnings;
    }
    
    public static function
        getWarningsAsHtmlUnorderedList() : string
    {
        $warnings = self::getWarningsAsArrayMessages();
        $html = self::messagesArrayToHtmlUnorderedList($warnings);
        return $html;
    }
    
    public static function
        getWarningsAsHtmlOrderedList() : string
    {
        $warnings = self::getWarningsAsArrayMessages();
        $html = self::messagesArrayToHtmlOrderedList($warnings);
        return $html;
    }
    
    public static function
        printWarningsAsHtmlUnorderedList()
    {
        echo self::getWarningsAsHtmlUnorderedList();
    }
}
