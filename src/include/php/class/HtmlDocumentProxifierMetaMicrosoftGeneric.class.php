<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class HtmlDocumentProxifierMetaMicrosoftGeneric
    extends DomDocumentProxifierDecorator
{
    private $name;
    
    
    public function
        __construct(string $name,
                    DomDocumentProxifierAbstract $previousProxy = null)
    {
        parent::__construct($previousProxy);
        $this->name = strtolower($name);
    }
    
    
    public function
        getXPathQuery() : string
    {
        return (
            '/html/head/meta['.
            XPathUtils::lowerCase('@name').
            '=\'msapplication-'. $this->name .'\''.
            ' and string(@content)]'
        );
    }
    
    public function
        getElements(DOMDocument $document) : DOMNodeList
    {
        $docXPath = new DOMXPath($document);
        $queryString = $this->getXPathQuery();
        $elements = $docXPath->query($queryString);
        return $elements;
    }
    
    public function
        isElementWithNeededAttributes(DOMElement $element) : bool
    {
        return (
            $element->hasAttribute('name') &&
            $element->hasAttribute('content')
        );
    }
    
    public function
        isElementWithNeededNotEmptyAttributes(DOMElement $element) : bool
    {
        if(!$this->isElementWithNeededAttributes($element))
        {
            return false;
        }
        return (
            !empty(trim($element->getAttribute('name'))) &&
            !empty(trim($element->getAttribute('content')))
        );
    }
    
    private function
        proxifyElement(DOMElement& $element) : bool
    {
        if(!$this->isElementWithNeededNotEmptyAttributes($element))
        {
            return false;
        }
        
        if(strtolower($element->getAttribute('name'))
           != 'msapplication-'. $this->name)
        {
            return false;
        }
        $attributeValue = $element->getAttribute('content');
        if(empty(trim($attributeValue)))
        {
            return false;
        }
        
        $newAttributeValue = $this->proxifyUrl($attributeValue);
        if(empty(trim($newAttributeValue)))
        {
            return false;
        }
        
        $element->setAttribute('content', $newAttributeValue);
        return true;
    }
    
    public function
        proxifyDocument(DOMDocument $document) : int
    {
        $nbModified = parent::proxifyDocument($document);
        
        $elements = $this->getElements($document);
        foreach($elements as $element)
        {
            if($this->proxifyElement($element))
            {
                ++$nbModified;
            }
        }
        
        return $nbModified;
    }
}
