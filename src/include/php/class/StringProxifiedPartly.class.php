<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class StringProxifiedPartly
{
    public $string;
    public $offset;
    public $nbModifiedElements;
    
    
    /**
     * @SuppressWarnings(ElseExpression)
     */
    public function
        __construct(string $string, int $offset = 0,
                    int $nbModifiedElements = 0)
    {
        $this->string = $string;
        $this->offset = $offset;
        if($nbModifiedElements < 0)
        {
            $this->nbModifiedElements = 0;
        }
        else
        {
            $this->nbModifiedElements = $nbModifiedElements;
        }
    }
}
