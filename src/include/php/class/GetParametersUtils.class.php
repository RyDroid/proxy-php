<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class GetParametersUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        mapToString(array $aMap) : string
    {
        $result = '';
        foreach($aMap as $key => $value)
        {
            if(empty($key))
            {
                continue;
            }
            if(!empty($result))
            {
                $result .= '&';
            }
            $result .= $key;
            if(!empty($value))
            {
                $result .= '='. $value;
            }
        }
        return $result;
    }
    
    public static function
        mapNotEncodedToString(array $aMap) : string
    {
        $mapEncoded = UrlProxifierCodecUtils::createEncodedParameters($aMap);
        $stringResult = self::mapToString($mapEncoded);
        return $stringResult;
    }
    
    
    public static function
        stringToMap(string $query) : array
    {
        $query = html_entity_decode($query);
        $queryParts = explode('&', $query);
        $result = array();
        foreach($queryParts as $part)
        {
            if(!empty($part))
            {
                $tmp = explode('=', $part);
                $tmpSize = sizeof($tmp);
                if($tmpSize == 1)
                {
                    $result[$tmp[0]] = '';
                }
                else if($tmpSize > 1)
                {
                    $result[$tmp[0]] = $tmp[1];
                }
            }
        }
        return $result;
    }
}
