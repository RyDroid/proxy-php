<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class UserInterfaceConfigurationUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}


    public static function
        createNullMutable()
        : UserInterfaceConfigurationMutableAbstract
    {
        return new UserInterfaceConfiguration(
            OptionnalBooleanUtils::createUndefined(),
            OptionnalBooleanUtils::createUndefined(),
            OptionnalBooleanUtils::createUndefined(),
            OptionnalBooleanUtils::createUndefined()
        );
    }
    
    
    public static function
        createFromMapWithoutSection(array $aMap)
        : UserInterfaceConfigurationAbstract
    {
        $configuration = self::createNullMutable();
        $configuration->setShowDebugWithString(
            UserInterfaceConfigurationPropertyUtils::
            getShowDebugFromMapWithoutSection($aMap)
        );
        $configuration->setShowProxyGetParametersConfigurationWithString(
            UserInterfaceConfigurationPropertyUtils::
            getShowProxyGetParametersConfigurationFromMapWithoutSection($aMap)
        );
        $configuration->setShowProxyCookiesConfigurationWithString(
            UserInterfaceConfigurationPropertyUtils::
            getShowProxyCookiesConfigurationFromMapWithoutSection($aMap)
        );
        $configuration->setUseClientScriptsWithString(
            UserInterfaceConfigurationPropertyUtils::
            getUseClientScriptsFromMapWithoutSection($aMap)
        );
        return $configuration;
    }
    
    public static function
        createFromMapInSection(array $aMap)
        : UserInterfaceConfigurationAbstract
    {
        $configuration = self::createNullMutable();
        $configuration->setShowDebugWithString(
            UserInterfaceConfigurationPropertyUtils::
            getShowDebugFromMapInSection($aMap)
        );
        $configuration->setShowProxyGetParametersConfigurationWithString(
            UserInterfaceConfigurationPropertyUtils::
            getShowProxyGetParametersConfigurationFromMapInSection($aMap)
        );
        $configuration->setShowProxyCookiesConfigurationWithString(
            UserInterfaceConfigurationPropertyUtils::
            getShowProxyCookiesConfigurationFromMapInSection($aMap)
        );
        $configuration->setUseClientScriptsWithString(
            UserInterfaceConfigurationPropertyUtils::
            getUseClientScriptsFromMapInSection($aMap)
        );
        return $configuration;
    }
    
    public static function
        createFromMap(array $aMap)
        : UserInterfaceConfigurationAbstract
    {
        if(ArrayUtils::keyExistsWithNotEmptyValue($aMap, 'user-interface'))
        {
            return self::createFromMapInSection($aMap['user-interface']);
        }
        if(ArrayUtils::keyExistsWithNotEmptyValue($aMap, 'ui'))
        {
            return self::createFromMapInSection($aMap['ui']);
        }
        return self::createFromMapWithoutSection($aMap);
    }
    
    public static function
        createFromFilePath(string $filePath)
        : UserInterfaceConfigurationAbstract
    {
        $anArray = IniFileUtils::parseFromFilePathWithSections($filePath);
        $configuration = self::createFromMap($anArray);
        return $configuration;
    }
}
