<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


define('DEFAULT_PROXIFIER_URL', URL . 'proxy.php');


final class UrlProxifierDefaultUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    private static $defaultUrl = DEFAULT_PROXIFIER_URL;
    private static $baseProxifiedUrl;
    
    
    public static function
        setUrl(string $url) : bool
    {
        $url = trim($url);
        if($url == '')
        {
            return false;
        }
        self::$defaultUrl = $url;
        return true;
    }
    
    public static function
        getUrl() : string
    {
        return self::$defaultUrl;
    }
    
    
    public static function
        setBaseProxifiedUrl(string $url) : bool
    {
        $url = trim($url);
        if($url == '' || $url == 'http://' || $url == 'https://')
        {
            return false;
        }
        self::$baseProxifiedUrl = $url;
        return true;
    }
    
    public static function
        setBaseProxifiedUrlWithoutFile(string $url) : bool
    {
        $urlWithoutFile = UrlStringParseUtils::getWithoutFile($url);
        return self::setBaseProxifiedUrl($urlWithoutFile);
    }
    
    /**
     * @SuppressWarnings(PHPMD.ExitExpression)
     */
    public static function
        setBaseProxifiedUrlWithoutFileOrExit(string $url, int $errorCode = 1)
    {
        if(!self::setBaseProxifiedUrlWithoutFile($url))
        {
            echo 'ERROR: Defining base URL without file failed';
            exit($errorCode);
        }
    }
    
    public static function
        getBaseProxifiedUrl() : string
    {
        if(self::$baseProxifiedUrl == null)
        {
            return '';
        }
        return self::$baseProxifiedUrl;
    }
    
    public static function
        getBaseProxifiedUrlWithoutDirectory() : string
    {
        $url = self::getBaseProxifiedUrl();
        $url = UrlStringParseUtils::getWithoutDirectoryAndFileName($url);
        return $url;
    }
    
    
    public static function
        getAbsoluteUrl(string $url) : string
    {
        $absoluteUrl = UrlStringUtils::getAbsolute(
            self::getBaseProxifiedUrl(), $url
        );
        return $absoluteUrl;
    }
}
