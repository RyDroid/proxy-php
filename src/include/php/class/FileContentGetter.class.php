<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class FileContentGetter
    extends AbstractContentGetter
{
    public static function
        getHttpContextOptionsArray(HttpHeaders $headers) : array
    {
        $httpContextOptions = array();
        
        if(!ProxyConfigurationGeneral::isNull())
        {
            $httpContextOptions['proxy'] = (
                ProxyConfigurationGeneral::getFullUrl()
            );
            $httpContextOptions['request_fulluri'] = true;
        }
        
        if(!$headers->isEmpty())
        {
            $httpContextOptions['header'] = (
                $headers->getString()
            );
        }
        
        return $httpContextOptions;
    }
    
    public static function
        getContextOptionsArray(HttpHeaders $headers) : array
    {
        $httpContextOptions = self::getHttpContextOptionsArray($headers);
        if(empty($httpContextOptions))
        {
            return array();
        }
        return array('http' => $httpContextOptions);
    }
    
    public static function
        getContext(HttpHeaders $headers)
    {
        $streamContextOptions = self::getContextOptionsArray($headers);
        $streamContext = stream_context_create($streamContextOptions);
        return $streamContext;
    }
    
    public function
        getRaw(string $url, HttpHeaders $headers) : string
    {
        $streamContext = self::getContext($headers);
        if(StringUtils::startsWith($url, '//'))
        {
            $result = file_get_contents('https:'. $url, false, $streamContext);
            if(empty($result))
            {
                $result = file_get_contents(
                    'http:'. $url, false, $streamContext
                );
            }
            return $result;
        }
        return file_get_contents($url, false, $streamContext);
    }
    
    public function
        get(string $url, HttpHeaders $headers) : ContentRequestResponse
    {
        $errors = [];
        
        $content = '';
        try
        {
            $this->getRaw($url, $headers);
        }
        catch(Exception $exception)
        {
            $errors[] = (string) $exception;
        }
        
        if(FileContentUtils::seemToBeHtml($content) &&
           !HtmlStringUtils::hasCharset($content))
        {
            $content = utf8_decode($content);
        }
        
        if(UrlTextStringUtils::isCss($url))
        {
            $response = new FileRequestResponse($content, 'text/css');
            $response->addErrors($errors);
            return $response;
        }
        $response = new FileRequestResponse($content);
        $response->addErrors($errors);
        return $response;
    }
}
