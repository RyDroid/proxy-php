<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @SuppressWarnings(PHPMD.Superglobals)
 */
final class SessionCapturedConfigurationCookiesUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        getDirectory() : string
    {
        return '';
    }
    
    public static function
        getFileName() : string
    {
        return '';
    }
    
    
    public static function
        hasToSave() : OptionnalBoolean
    {
        if(!isset($_COOKIE) || empty($_COOKIE))
        {
            return OptionnalBooleanUtils::createUndefined();
        }
        return OptionnalBooleanUtils::createFromString(
            SessionCapturedConfigurationPropertyUtils::
            getSaveFromMapWithoutSection($_COOKIE)
        );
    }
    
    public static function
        hasToCreateAutomatically() : OptionnalBoolean
    {
        if(!isset($_COOKIE) || empty($_COOKIE))
        {
            return OptionnalBooleanUtils::createUndefined();
        }
        return OptionnalBooleanUtils::createFromString(
            SessionCapturedConfigurationPropertyUtils::
            getCreateAutomaticallyFromMapWithoutSection($_COOKIE)
        );
    }
    
    public static function
        hasToSaveProxy() : OptionnalBoolean
    {
        if(!isset($_COOKIE) || empty($_COOKIE))
        {
            return OptionnalBooleanUtils::createUndefined();
        }
        return OptionnalBooleanUtils::createFromString(
            SessionCapturedConfigurationPropertyUtils::
            getSaveProxyFromMapWithoutSection($_COOKIE)
        );
    }
    
    public static function
        hasToSaveCookies() : OptionnalBoolean
    {
        if(!isset($_COOKIE) || empty($_COOKIE))
        {
            return OptionnalBooleanUtils::createUndefined();
        }
        return OptionnalBooleanUtils::createFromString(
            SessionCapturedConfigurationPropertyUtils::
            getSaveCookiesFromMapWithoutSection($_COOKIE)
        );
    }
    
    public static function
        hasToSaveUserAgent() : OptionnalBoolean
    {
        if(!isset($_COOKIE) || empty($_COOKIE))
        {
            return OptionnalBooleanUtils::createUndefined();
        }
        return OptionnalBooleanUtils::createFromString(
            SessionCapturedConfigurationPropertyUtils::
            getSaveUserAgentFromMapWithoutSection($_COOKIE)
        );
    }
    
    public static function
        hasToSaveReferer() : OptionnalBoolean
    {
        if(!isset($_COOKIE) || empty($_COOKIE))
        {
            return OptionnalBooleanUtils::createUndefined();
        }
        return OptionnalBooleanUtils::createFromString(
            SessionCapturedConfigurationPropertyUtils::
            getSaveRefererFromMapWithoutSection($_COOKIE)
        );
    }
    
    public static function
        hasToSaveLanguage() : OptionnalBoolean
    {
        if(!isset($_COOKIE) || empty($_COOKIE))
        {
            return OptionnalBooleanUtils::createUndefined();
        }
        return OptionnalBooleanUtils::createFromString(
            SessionCapturedConfigurationPropertyUtils::
            getSaveLanguageFromMapWithoutSection($_COOKIE)
        );
    }
    
    public static function
        hasToSaveEncoding() : OptionnalBoolean
    {
        if(!isset($_COOKIE) || empty($_COOKIE))
        {
            return OptionnalBooleanUtils::createUndefined();
        }
        return OptionnalBooleanUtils::createFromString(
            SessionCapturedConfigurationPropertyUtils::
            getSaveEncodingFromMapWithoutSection($_COOKIE)
        );
    }
    
    public static function
        hasToSaveEtag() : OptionnalBoolean
    {
        if(!isset($_COOKIE) || empty($_COOKIE))
        {
            return OptionnalBooleanUtils::createUndefined();
        }
        return OptionnalBooleanUtils::createFromString(
            SessionCapturedConfigurationPropertyUtils::
            getSaveEtagFromMapWithoutSection($_COOKIE)
        );
    }
    
    public static function
        hasToSaveDoNotTrack() : OptionnalBoolean
    {
        if(!isset($_COOKIE) || empty($_COOKIE))
        {
            return OptionnalBooleanUtils::createUndefined();
        }
        return OptionnalBooleanUtils::createFromString(
            SessionCapturedConfigurationPropertyUtils::
            getSaveDoNotTrackFromMapWithoutSection($_COOKIE)
        );
    }
}
