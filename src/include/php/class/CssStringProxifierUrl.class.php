<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class CssStringProxifierUrl
    extends CssStringProxifierDecorator
{
    public function
        proxifyStringWithUrlArgument(string $lineOrigin,
                                     int $positionStart = 0,
                                     int $positionEnd   = 0)
        : string
    {
        $delimiter = '';
        $delimiterStart = $positionStart;
        while(true)
        {
            $currentCharacter = $lineOrigin[$delimiterStart];
            if($currentCharacter == ' ' || $currentCharacter == '	')
            {
                ++$delimiterStart;
                continue;
            }
            if($currentCharacter == '\'' || $currentCharacter == '"')
            {
                ++$delimiterStart;
                $delimiter = $currentCharacter;
                break;
            }
            break;
        }
        
        if($positionEnd <= 0)
        {
            $positionEnd += strlen($lineOrigin);
        }
        $delimiterEnd = (
            CssStringProxifierUrlUtils::findDelimiterEndPositionOfUrlArgument(
                $lineOrigin, $positionEnd, $delimiter
            )
        );
        
        if($delimiter == '' || $delimiterStart >= $delimiterEnd)
        {
            return $this->proxifyStringWithUrl(
                $lineOrigin, $positionStart, $positionEnd
            );
        }
        return $this->proxifyStringWithUrl(
            $lineOrigin, $delimiterStart, $delimiterEnd
        );
    }
    
    private function
        proxifyLinePartlyYes(string $lineOrigin, int $positionStart)
        : StringProxifiedPartly
    {
        $positionStart += strlen(CssStringProxifierUrlUtils::FUNCTION_NAME) +1;
        $positionEnd = strpos($lineOrigin, ')', $positionStart);
        if($positionEnd !== FALSE)
        {
            $positionEnd2 = CssStringProxifierUrlUtils::findEndPosition(
                $lineOrigin, $positionEnd
            );
            if($positionEnd2 > $positionEnd)
            {
                $linePartlyProxified = $this->proxifyStringWithUrlArgument(
                    $lineOrigin, $positionStart, $positionEnd
                );
                $positionEnd2 = CssStringProxifierUrlUtils::findEndPosition(
                    $linePartlyProxified, $positionEnd2
                );
                return new StringProxifiedPartly(
                    $linePartlyProxified, $positionEnd2+1, 1
                );
            }
        }
        return new StringProxifiedPartly($lineOrigin, -1);
    }
    
    public function
        proxifyLinePartly(string $lineOrigin, int $offset)
        : StringProxifiedPartly
    {
        $positionStart = strpos(
            $lineOrigin, CssStringProxifierUrlUtils::FUNCTION_NAME .'(', $offset
        );
        if($positionStart === FALSE)
        {
            return new StringProxifiedPartly($lineOrigin, -1);
        }
        
        $hasToProxify = (
            CssStringProxifierUrlUtils::hasToProxifyConsideringWhatIsBefore(
                $lineOrigin, $offset, $positionStart
            )
        );
        if($hasToProxify)
        {
            return $this->proxifyLinePartlyYes($lineOrigin, $positionStart);
        }
        return new StringProxifiedPartly($lineOrigin, -1);
    }
    
    public function
        proxifyLine(string $lineOrigin) : string
    {
        $line = parent::proxifyLine($lineOrigin);
        return CssLineProxifierUtils::proxifyLine($this, $line);
    }
    
    public function
        proxifyString(string $cssOrigin) : string
    {
        $css = parent::proxifyString($cssOrigin);
        return CssLineProxifierUtils::proxifyString($this, $css);
    }
}
