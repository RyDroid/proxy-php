<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class OptionnalBoolean
{
    private $value;
    
    
    /**
     * @SuppressWarnings(ElseExpression)
     */
    public function
        __construct(int $value)
    {
        if(OptionnalBooleanUtils::isValidInteger($value))
        {
            $this->value = $value;
        }
        else
        {
            $this->value = OptionnalBooleanConst::UNDEFINED;
        }
    }
    
    
    public function
        isUndefined() : bool
    {
        return $this->value == OptionnalBooleanConst::UNDEFINED;
    }
    
    public function
        isDefined() : bool
    {
        return $this->value != OptionnalBooleanConst::UNDEFINED;
    }
    
    public function
        isFalse() : bool
    {
        return $this->value == OptionnalBooleanConst::FALSE;
    }
    
    public function
        isTrue() : bool
    {
        return $this->value == OptionnalBooleanConst::TRUE;
    }
}
