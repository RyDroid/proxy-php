/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


var WebSessionCapturedToClif = {};

/** @const */
WebSessionCapturedToClif.XSLT_FILE_PATH = 'include/xslt/clif.xslt';

/**
 * @return {boolean}
 */
WebSessionCapturedToClif.loadAndDo =
    function(functionToDoAfter)
{
    "use strict";
    return XsltProcessorCache.loadAndDo(
        WebSessionCapturedToClif.XSLT_FILE_PATH, functionToDoAfter
    );
};

/**
 * @param {Object} aDocument
 */
WebSessionCapturedToClif.prepareDomDocument =
    function(aDocument)
{
    "use strict";
    WebSessionCapturedPauseAdder.convertDomDocument(aDocument);
};

/**
 * @param {Object} docSource
 * @return {Object}
 */
WebSessionCapturedToClif.getPreparedDomDocument =
    function(docSource)
{
    "use strict";
    var docResult = domDocumentClone(docSource);
    WebSessionCapturedToClif.prepareDomDocument(docResult);
    return docResult;
};

/**
 * @param {Object} docSource
 * @return {Object}
 */
WebSessionCapturedToClif.convertDomDocumentToDomDocument =
    function(docSource)
{
    "use strict";
    var docPrepared = WebSessionCapturedToClif.getPreparedDomDocument(
        docSource
    );
    var docResult = WebSessionCapturedToXml.convertDomDocToDomDocWithXsltUrl(
        docPrepared, WebSessionCapturedToClif.XSLT_FILE_PATH
    );
    return docResult;
};

/**
 * @param {String|string} xmlStringSource
 */
WebSessionCapturedToClif.convertXmlStringToDomDocument =
    function(xmlStringSource)
{
    "use strict";
    var docResult = WebSessionCapturedToXml.convertXmlStringToDomDocument(
        xmlStringSource,
        WebSessionCapturedToClif.convertDomDocumentToDomDocument
    );
    return docResult;
};

/**
 * @param {String|string} xmlStringSource
 * @return {String|string}
 */
WebSessionCapturedToClif.convertXmlStringToXmlString =
    function(xmlStringSource)
{
    "use strict";
    var xmlStringResult = WebSessionCapturedToXml.convertXmlStringToXmlString(
        xmlStringSource,
        WebSessionCapturedToClif.convertDomDocumentToDomDocument
    );
    return xmlStringResult;
};

/**
 * @return {String|string}
 */
WebSessionCapturedToClif.getType =
    function()
{
    "use strict";
    return 'text/xml;charset=utf-8';
};

/**
 * @return {String|string}
 */
WebSessionCapturedToClif.getFileExtension =
    function()
{
    "use strict";
    return 'xis';
};
