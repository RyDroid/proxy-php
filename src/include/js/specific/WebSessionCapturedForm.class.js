/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


var WebSessionCapturedForm = {};

/** @const */
WebSessionCapturedForm.ID = 'convert-form';

WebSessionCapturedForm.downloadButtonListenerAdded = false;

WebSessionCapturedForm.getInputStringElement =
    function()
{
    "use strict";
    return document.getElementById('input-string');
};

WebSessionCapturedForm.getOutputStringElement =
    function()
{
    "use strict";
    return document.getElementById('output-string');
};

/**
 * @param {Object} event
 */
WebSessionCapturedForm.setInputStringWithServerDataStateChange =
    function(event)
{
    "use strict";
    var httpRequest = event.currentTarget;
    if(httpRequest.readyState == 4)
    {
	if(httpRequest.status === 200)
	{
	    var inputStringElement = (
		WebSessionCapturedForm.getInputStringElement()
	    );
	    if(inputStringElement != null &&
	       inputStringElement.value.trim().length == 0 &&
	       httpRequest.responseText != undefined &&
	       httpRequest.responseText != null &&
	       httpRequest.responseText.trim().length > 0)
	    {
		inputStringElement.value = httpRequest.responseText;
		WebSessionCapturedForm.update();
	    }
	}
    }
};

/**
 * @return {boolean}
 */
WebSessionCapturedForm.setInputStringWithServerData =
    function()
{
    "use strict";
    
    if(typeof(getXMLHttpRequest) != 'function')
    {
	return false;
    }
    var httpRequest = getXMLHttpRequest();
    if(httpRequest == null)
    {
	return false;
    }
    
    httpRequest.onreadystatechange = (
	WebSessionCapturedForm.setInputStringWithServerDataStateChange
    );
    httpRequest.open('GET', 'data-raw.php', true);
    return sendXMLHttpRequest(httpRequest);
};

/**
 * @param {String|string} aString
 * @return {Object}
 */
WebSessionCapturedForm.getConverterWithString =
    function(aString)
{
    "use strict";
    aString = aString.trim().toLowerCase();
    if(aString == 'clif')
    {
	return WebSessionCapturedToClif;
    }
    return null;
};

/**
 * @param {Object} element
 * @return {Object}
 */
WebSessionCapturedForm.getConverterWithElement =
    function(element)
{
    "use strict";
    if(element == null)
    {
	return null;
    }
    var value = element.value;
    return WebSessionCapturedForm.getConverterWithString(value);
};

/**
 * @return {Object}
 */
WebSessionCapturedForm.getConverter =
    function()
{
    "use strict";
    if(typeof(document.querySelector) == 'function')
    {
	var element = document.querySelector(
	    '#'+ WebSessionCapturedForm.ID +' input[name="convert-to"]:checked'
	);
	return WebSessionCapturedForm.getConverterWithElement(element);
    }
    if(typeof(document.getElementsByName) == 'function')
    {
	var radios = document.getElementsByName('convert-to');
	for(var index = 0; index < radios.length; ++index)
	{
	    var radio = radios[index];
	    if(typeof(radio.checked) == 'boolean' && radio.checked)
	    {
		return WebSessionCapturedForm.getConverterWithElement(radio);
	    }
	}
    }
    return null;
};

/**
 * @return {boolean}
 */
WebSessionCapturedForm.updateOutput =
    function()
{
    "use strict";
    
    var inputStringElement = WebSessionCapturedForm.getInputStringElement();
    if(inputStringElement == null)
    {
	return false;
    }
    
    var outputStringElement = WebSessionCapturedForm.getOutputStringElement();
    if(outputStringElement == null)
    {
	return false;
    }
    
    var inputString  = inputStringElement.value;
    if(inputString.trim().length == 0)
    {
	return false;
    }
    
    var converter = WebSessionCapturedForm.getConverter();
    
    var outputString = converter.convertXmlStringToXmlString(
	inputString
    );
    if(outputString == null)
    {
	return false;
    }
    
    if(typeof(outputStringElement.innerText) != 'undefined')
    {
	outputStringElement.innerText = outputString;
    }
    else
    {
	outputStringElement.value = outputString;
    }
    return true;
};

/**
 * @return {boolean}
 */
WebSessionCapturedForm.updateOutputAsynchronous =
    function(functionToDoAfter)
{
    "use strict";
    
    var inputStringElement = WebSessionCapturedForm.getInputStringElement();
    if(inputStringElement == null)
    {
	return false;
    }
    
    var outputStringElement = WebSessionCapturedForm.getOutputStringElement();
    if(outputStringElement == null)
    {
	return false;
    }
    
    var inputString  = inputStringElement.value;
    if(inputString.trim().length == 0)
    {
	return false;
    }
    
    var converter = WebSessionCapturedForm.getConverter();
    
    return converter.loadAndDo(
	function()
	{
	    "use strict";
	    
	    var outputString = converter.convertXmlStringToXmlString(
		inputString
	    );
	    if(outputString == null)
	    {
		return;
	    }
	    
	    if(typeof(outputStringElement.innerText) != 'undefined')
	    {
		outputStringElement.innerText = outputString;
	    }
	    else
	    {
		outputStringElement.value = outputString;
	    }
	    
	    if(typeof(functionToDoAfter) == 'function')
	    {
		functionToDoAfter();
	    }
	}
    );
};

/**
 * @return {Object}
 * @suppress {undefinedVars}
 */
WebSessionCapturedForm.getOutputAsBlob =
    function()
{
    "use strict";
    
    var outputStringElement = document.getElementById('output-string');
    if(outputStringElement == null)
    {
	return null;
    }
    
    var outputString;
    if(typeof(outputStringElement.innerText) != 'undefined')
    {
	outputString = outputStringElement.innerText;
    }
    else
    {
	outputString = outputStringElement.value;
    }
    if(outputString == null || outputString.trim().length == 0)
    {
	return null;
    }

    if(typeof(Blob) != 'function' || typeof(saveAs) != 'function')
    {
	return null;
    }
    
    var converter = WebSessionCapturedForm.getConverter();
    if(typeof(converter) != 'object')
    {
	return null;
    }
    
    var type;
    if(typeof(converter.getType) == 'function')
    {
	type = converter.getType();
    }
    else
    {
	type = 'xml';
    }
    
    return new Blob([outputString], {'type': type});
};

/**
 * @return {String|string}
 */
WebSessionCapturedForm.getOutputFileName =
    function()
{
    "use strict";
    var fileName = 'data.';
    var converter = WebSessionCapturedForm.getConverter();
    if(typeof(converter) == 'object' &&
       typeof(converter.getFileExtension) == 'function' &&
       converter.getFileExtension().length > 0)
    {
	fileName += converter.getFileExtension();
    }
    else
    {
	fileName += 'xml';
    }
    return fileName;
};

/**
 * @return {boolean}
 */
WebSessionCapturedForm.saveOutputAs =
    function()
{
    "use strict";
    
    var blob = WebSessionCapturedForm.getOutputAsBlob();
    if(blob == null)
    {
	return false;
    }
    
    var fileName = WebSessionCapturedForm.getOutputFileName();
    if(fileName == null || fileName.length == 0)
    {
	return false;
    }
    
    saveAs(blob, fileName);
    return true;
};

/**
 * @return {boolean}
 */
WebSessionCapturedForm.update =
    function()
{
    "use strict";
    return WebSessionCapturedForm.updateOutputAsynchronous(
	function()
	{
	    "use strict";
	    var downloadElement = document.getElementById('download');
	    if(downloadElement != null)
	    {
		downloadElement.disabled = false;
		if(!WebSessionCapturedForm.downloadButtonListenerAdded)
		{
		    downloadElement.addEventListener(
			'click', WebSessionCapturedForm.saveOutputAs, false
		    );
		    WebSessionCapturedForm.downloadButtonListenerAdded = true;
		}
	    }
	    return true;
	}
    );
};

WebSessionCapturedForm.onInput =
    function()
{
    "use strict";
    WebSessionCapturedForm.update();
};

WebSessionCapturedForm.onWindowLoaded =
    function()
{
    "use strict";
    
    var formElement = document.getElementById(WebSessionCapturedForm.ID);
    if(formElement)
    {
	if(typeof(formElement.addEventListener) == 'function')
	{
	    formElement.addEventListener(
		'input', WebSessionCapturedForm.onInput, false
	    );
	}
	else
	{
	    formElement.oninput = WebSessionCapturedForm.onInput;
	}
    }
    else
    {
	if(typeof(window.console)       == 'object' &&
	   typeof(window.console.error) == 'function')
	{
	    window.console.error('The form element was not found!');
	}
    }
    
    // If there was already something in the form
    WebSessionCapturedForm.update();
    
    var inputStringElement = WebSessionCapturedForm.getInputStringElement();
    if(inputStringElement != null &&
       inputStringElement.value.trim().length == 0)
    {
	WebSessionCapturedForm.setInputStringWithServerData();
    }
};

WebSessionCapturedForm.main =
    function()
{
    "use strict";
    if(typeof(window.addEventListener) == 'function')
    {
	window.addEventListener(
	    'load', WebSessionCapturedForm.onWindowLoaded, false
	);
    }
    else
    {
	window.onload = WebSessionCapturedForm.onWindowLoaded;
    }
};
