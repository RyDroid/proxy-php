/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


var WebSessionCapturedPauseAdder = {};

/**
 * @return {boolean}
 */
WebSessionCapturedPauseAdder.manageConsecutiveNodes =
    function(currentElement, nextElement)
{
    "use strict";
    
    if(currentElement == null || nextElement == null)
    {
        return false;
    }
    
    if(typeof(currentElement.getElementsByTagName) != 'function')
    {
        return false;
    }
    var currentTimeRequestElements = currentElement.getElementsByTagName(
        'timeRequest'
    );
    if(currentTimeRequestElements.length == 0)
    {
        return false;
    }
    var currentTimeRequestElement = currentTimeRequestElements[0];
    
    if(typeof(nextElement.getElementsByTagName) != 'function')
    {
        return false;
    }
    var nextTimeRequestElements = nextElement.getElementsByTagName(
        'timeRequest'
    );
    if(nextTimeRequestElements.length == 0)
    {
        return false;
    }
    var nextTimeRequestElement = nextTimeRequestElements[0];
    
    if(!currentTimeRequestElement.hasAttribute('value') ||
       !nextTimeRequestElement.hasAttribute('value'))
    {
        return false;
    }
    var currentTimeRequestValue = currentTimeRequestElement.getAttribute(
        'value'
    );
    var nextTimeRequestValue    = nextTimeRequestElement.getAttribute(
        'value'
    );
    
    var currentTimeRequestFloat = parseFloat(currentTimeRequestValue);
    if(isNaN(currentTimeRequestFloat))
    {
        return false;
    }
    var nextTimeRequestFloat    = parseFloat(nextTimeRequestValue);
    if(isNaN(nextTimeRequestFloat))
    {
        return false;
    }
    
    var diffTimeRequestFloat = (
        nextTimeRequestFloat - currentTimeRequestFloat
    );
    if(diffTimeRequestFloat > 0)
    {
        var pauseElement = currentElement.ownerDocument.createElement('pause');
        if(pauseElement == null)
        {
            return false;
        }
        if(typeof(pauseElement.setAttribute) != 'function')
        {
            return false;
        }
        pauseElement.setAttribute('value', diffTimeRequestFloat);
        
        if(currentTimeRequestElement.hasAttribute('unit') &&
           nextTimeRequestElement.hasAttribute('unit') &&
           currentTimeRequestElement.getAttribute('unit')
           == nextTimeRequestElement.getAttribute('unit'))
        {
            pauseElement.setAttribute(
                'unit', currentTimeRequestElement.getAttribute('unit')
            );
        }
        
        if(currentTimeRequestElement.hasAttribute('type') &&
           nextTimeRequestElement.hasAttribute('type') &&
           currentTimeRequestElement.getAttribute('type')
           == nextTimeRequestElement.getAttribute('type'))
        {
            pauseElement.setAttribute(
                'type', currentTimeRequestElement.getAttribute('type')
            );
        }
        
        if(typeof(currentElement.parentNode.insertAfter) == 'function')
        {
            currentElement.parentNode.insertAfter(pauseElement, currentElement);
            return true;
        }
        if(typeof(currentElement.parentNode.insertBefore) == 'function')
        {
            currentElement.parentNode.insertBefore(pauseElement, nextElement);
            return true;
        }
    }
    
    return false;
};

/**
 * @param {Object} document
 * @return {boolean}
 */
WebSessionCapturedPauseAdder.convertDomDocument =
    function(document)
{
    "use strict";
    
    if(typeof(document) != 'object' ||
       isNaN(document.childNodes.length) ||
       document.childNodes.length == 0)
    {
        return false;
    }
    var sessionElement = document.childNodes[0];
    if(sessionElement.localName != 'session')
    {
        return false;
    }
    
    if(sessionElement.childNodes.length < 2)
    {
        return false;
    }
    var currentElement = sessionElement.childNodes[0];
    
    do
    {
        var nextElement = currentElement.nextSibling;
        WebSessionCapturedPauseAdder.manageConsecutiveNodes(
            currentElement, nextElement
        );
        currentElement = nextElement;
    }
    while(currentElement != null && typeof(currentElement) == 'object');
    return true;
};

/**
 * @param {Object} docSource
 * @return {Object}
 */
WebSessionCapturedPauseAdder.convertDomDocToDomDocument =
    function(docSource)
{
    "use strict";
    var docResult = domDocumentClone(docSource);
    WebSessionCapturedPauseAdder.convertDomDocument(docResult);
    return docResult;
};
