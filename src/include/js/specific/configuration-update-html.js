/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @param  {String|string} from
 * @param  {String|string} elementId
 * @return {boolean}
 */
function updateOneHtmlConfiguration(from, elementId)
{
    'use strict';
    
    if(typeof(getXMLHttpRequest) != 'function')
    {
	return false;
    }
    var httpRequest = getXMLHttpRequest();
    if(httpRequest == null)
    {
	return false;
    }
    
    if(typeof(elementId) == 'undefined')
    {
	return false;
    }
    elementId = elementId.toString();
    if(elementId.trim().length == 0)
    {
	return false;
    }
    
    if(typeof(document.getElementById) != 'function')
    {
	return false;
    }
    var element = document.getElementById(elementId);
    if(element == null || typeof(element) != 'object')
    {
	return false;
    }
    
    httpRequest.onreadystatechange = function()
    {
	'use strict';
	if(!isNaN(httpRequest.readyState) && httpRequest.readyState == 4)
	{
	    if(!isNaN(httpRequest.status) && httpRequest.status === 200)
	    {
		var htmlString = httpRequest.responseText;
		if(htmlString.trim().length > 0)
		{
		    element.innerHTML = htmlString;
		}
	    }
	}
    };
    var url = 'configuration-get.php?from='+ from +'&format=html';
    httpRequest.open('GET', url, true);
    return sendXMLHttpRequest(httpRequest);
}

/**
 * @return {boolean}
 */
function updateHtmlCurrentGeneralConfiguration()
{
    'use strict';
    return updateOneHtmlConfiguration(
        'general', 'current-general-configuration-content'
    );
}

/**
 * @return {boolean}
 */
function updateHtmlCurrentCookiesConfiguration()
{
    'use strict';
    return updateOneHtmlConfiguration(
        'cookies', 'current-cookies-configuration-content'
    );
}

/**
 * @return {boolean}
 */
function updateHtmlConfiguration()
{
    'use strict';
    /** @type {boolean} */
    var result = true;
    result &= updateHtmlCurrentGeneralConfiguration();
    result &= updateHtmlCurrentCookiesConfiguration();
    return result;
}
