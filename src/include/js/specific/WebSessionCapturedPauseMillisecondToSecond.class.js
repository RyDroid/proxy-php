/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


var WebSessionCapturedPauseMillisecondToSecond = {};

/** @const */
WebSessionCapturedPauseMillisecondToSecond.XPATH_EXPRESSION_ROOT = (
    "//pause[string(number(@value)) != 'NaN' and "+
    "(string(@unit) = 'ms' or string(@unit) = 'millisecond')]"
);
/** @const */
WebSessionCapturedPauseMillisecondToSecond.XPATH_EXPRESSION_NODE = (
    "."+ WebSessionCapturedPauseMillisecondToSecond.XPATH_EXPRESSION_ROOT
);

/**
 * @param {Object} node
 * @return {boolean}
 */
WebSessionCapturedPauseMillisecondToSecond.manageNodeBasic =
    function(node)
{
    "use strict";
    
    if(!WebSessionCapturedPauseTransformGeneric.manageNodeBasic(node) ||
       !node.hasAttribute('unit'))
    {
	return false;
    }
    var unit = node.getAttribute('value').trim().toLowerCase();
    if(unit != 'ms' && unit != 'millisecond' && unit != 'milliseconds')
    {
	return false;
    }
    
    var valueString = node.getAttribute('value');
    var valueInteger = parseInt(valueString, 10);
    if(isNaN(valueInteger) || valueInteger < 0)
    {
	return false;
    }
    valueInteger = valueInteger / 1000;
    node.setAttribute('value', valueInteger);
    node.setAttribute('unit', 'second');
    return true;
};

/**
 * @param {Object} node
 * @return {boolean}
 */
WebSessionCapturedPauseMillisecondToSecond.manageNodeWithXPath =
    function(node)
{
    "use strict";
    return WebSessionCapturedPauseTransformGeneric.manageNodeWithXPath(
        node,
        WebSessionCapturedPauseMillisecondToSecond.XPATH_EXPRESSION_NODE,
        WebSessionCapturedPauseMillisecondToSecond.manageNodeBasic
    );
};

/**
 * @param {Object} node
 * @return {boolean}
 */
WebSessionCapturedPauseMillisecondToSecond.manageNode = (
    WebSessionCapturedPauseMillisecondToSecond.manageNodeWithXPath
);

/**
 * @param {Object} document
 * @return {boolean}
 */
WebSessionCapturedPauseMillisecondToSecond.convertDomDocument =
    function(document)
{
    "use strict";
    return WebSessionCapturedPauseTransformGeneric.convertDomDocument(
        document, WebSessionCapturedPauseMillisecondToSecond.manageNode
    );
};

/**
 * @param {Object} docSource
 * @return {Object}
 */
WebSessionCapturedPauseMillisecondToSecond.convertDomDocToDomDocument =
    function(docSource)
{
    "use strict";
    var docResult = domDocumentClone(docSource);
    WebSessionCapturedPauseMillisecondToSecond.convertDomDocument(docResult);
    return docResult;
};
