/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


var WebSessionCapturedPauseTransformGeneric = {};

/**
 * @param {Object} node
 * @return {boolean}
 */
WebSessionCapturedPauseTransformGeneric.manageNodeBasic =
    function(node)
{
    "use strict";
    return (
	node.localName == 'pause' &&
	typeof(node.hasAttribute) == 'function' &&
	typeof(node.getAttribute) == 'function' &&
	typeof(node.setAttribute) == 'function' &&
	node.hasAttribute('value') &&
	!isNaN(node.getAttribute('value'))
    );
};

/**
 * @param {Object} node
 * @return {boolean}
 */
WebSessionCapturedPauseTransformGeneric.manageNodeWithXPath =
    function(node, xpathExpression, manageNodeBasicFunction)
{
    "use strict";
    
    var document = (
	typeof(node.ownerDocument.evaluate) == 'function'
	    ? node.ownerDocument
	    : (typeof(node.ownerDocument.documentElement.evaluate) == 'function'
	       ? node.ownerDocument.documentElement
	       : (typeof(node.documentElement.evaluate) == 'function'
		  ? node.documentElement
		  : null))
    );
    if(document == null)
    {
	return false;
    }
    
    var iterator = document.evaluate(
        xpathExpression, node,
        null, XPathResult.UNORDERED_NODE_ITERATOR_TYPE, null
    );
    if(iterator == null ||
       typeof(iterator.iterateNext) != 'function')
    {
	return false;
    }
    
    do
    {
	try
	{
	    var currentNode = iterator.iterateNext();
	}
	catch(exception)
	{
	    return false;
	}
	if(currentNode)
	{
	    manageNodeBasicFunction(currentNode);
	}
	else
	{
	    return true;
	}
    }
    while(true);
};

/**
 * @param {Object} document
 * @return {boolean}
 */
WebSessionCapturedPauseTransformGeneric.convertDomDocument =
    function(document, manageNodeFunction)
{
    "use strict";
    
    if(typeof(document) != 'object' ||
       isNaN(document.childNodes.length) ||
       document.childNodes.length == 0)
    {
	return false;
    }
    
    var sessionElement = document.childNodes[0];
    if(sessionElement.localName != 'session')
    {
	return false;
    }
    
    if(sessionElement.childNodes.length < 1)
    {
	return false;
    }
    var currentElement = sessionElement.childNodes[0];
    
    while(currentElement != null && typeof(currentElement) == 'object')
    {
	manageNodeFunction(currentElement);
	currentElement = currentElement.nextSibling;
    }
    return true;
};
