/*
 * Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


/**
 * @return {Array}
 */
function getCookiesAsStrings()
{
    "use strict";
    return document.cookie.split(';');
}

/**
 * @return {Array}
 */
function getNamesOfCookies()
{
    "use strict";
    var cookies = getCookiesAsStrings();
    var names = [];
    for(var index = 0; index < cookies.length; ++index)
    {
	var cookie = cookies[index]
	var name = cookie.split('=')[0];
	names.push(name);
    }
    return names;
}

/**
 * @param {String|string}    name
 * @param {String|string}    value
 * @param {number=}          expireTimestampMs
 * @param {(String|string)=} path
 * @param {(String|string)=} domain
 */
function setCookie(name, value, expireTimestampMs, path, domain)
{
    "use strict";
    
    if(typeof(expireTimestampMs) == 'undefined')
    {
	expireTimestampMs = 0;
    }
    
    var aDate = new Date(expireTimestampMs);
    var expireString = "expires="+ aDate.toUTCString();
    var stringToSet = name +"="+ value +";"+ expireTimestampMs;
    if(typeof(path) != 'undefined')
    {
	stringToSet += ";path="+ path;
    }
    if(typeof(domain) != 'undefined')
    {
	stringToSet += ";domain="+ domain;
    }
    document.cookie = stringToSet;
}

/**
 * @param  {String|string} name
 * @return {String|string}
 */
function getCookie(name)
{
    "use strict";
    var cookieStart = name + '=';
    var cookies = getCookiesAsStrings();
    for(var index = 0; index < cookies.length; ++index)
    {
        var cookie = cookies[index];
        while(cookie.charAt(0) == ' ')
	{
            cookie = cookie.substring(1);
        }
        if(cookie.indexOf(cookieStart) == 0)
	{
            return cookie.substring(cookieStart.length, cookie.length);
        }
    }
    return "";
}

/**
 * @param  {String|string} name
 * @return {boolean}
 */
function hasCookie(name)
{
    "use strict";
    return getCookie(name).length > 0;
}

/**
 * @param  {String|string}    name
 * @param  {(String|string)=} path
 * @param  {(String|string)=} domain
 * @return {boolean}
 */
function removeCookie(name, path, domain)
{
    "use strict";
    
    if(!hasCookie(name))
    {
	return false;
    }
    
    var stringToRemove = name + '=';
    if(typeof(path) != 'undefined')
    {
	stringToRemove += ";path="+ path;
    }
    if(typeof(domain) != 'undefined')
    {
	stringToRemove += ";domain="+ domain;
    }
    stringToRemove += ";expires=Thu, 01 Jan 1970 00:00:01 GMT";
    document.cookie = stringToRemove;
    return true;
}
