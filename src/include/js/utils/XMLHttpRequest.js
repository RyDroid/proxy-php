/*
 * Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


/**
 * @return {Object}
 */
function getXMLHttpRequest()
{
    'use strict';
    
    if(typeof(XMLHttpRequest) == 'function')
    {
	return new XMLHttpRequest();
    }
    if(typeof(window.ActiveXObject) == 'function')
    {
	// Old versions of Internet Explorer...
	try
	{
	    return new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
	    return new ActiveXObject("Microsoft.XMLHTTP");
	}
    }
    return null;
}

/**
 * @return {boolean}
 */
function isXMLHttpRequest(value)
{
    'use strict';
    if(typeof(value) == 'undefined' ||
       value == null)
    {
	return false;
    }
    if(typeof(XMLHttpRequest) == 'function' &&
       value instanceof XMLHttpRequest)
    {
	return true;
    }
    return false;
}

/**
 * @return {Object}
 */
function createCORSRequest(method, url, async)
{
    if(typeof(async) == 'undefined')
    {
	async = true;
    }
    
    var xhr = getXMLHttpRequest();
    if('withCredentials' in xhr)
    {
	// Check if the XMLHttpRequest object has a "withCredentials" property.
	// "withCredentials" only exists on XMLHTTPRequest2 objects.
	xhr.open(method, url, async);
    }
    else if(typeof(XDomainRequest) != 'undefined')
    {
	// Otherwise, check if XDomainRequest.
	// XDomainRequest only exists in IE, and is IE's way of making CORS requests.
	xhr = new XDomainRequest();
	xhr.open(method, url);
    }
    else
    {
	// Otherwise, CORS is not supported by the browser.
	xhr = null;
    }
    return xhr;
}

/**
 * @param {Object} request
 * @param {*=} data
 * @return {boolean}
 */
function sendXMLHttpRequest(request, data)
{
    'use strict';
    
    if(!isXMLHttpRequest(request))
    {
	return false;
    }
    if(typeof(data) == 'undefined')
    {
	data = null;
    }
    
    try
    {
	if(data == null)
	{
	    request.send();
	}
	else
	{
	    request.send(data);
	}
	return true;
    }
    catch(exception)
    {
	if(typeof(window.console)       == 'object' &&
	   typeof(window.console.error) == 'function')
	{
	    if(exception == undefined ||
	       exception == null ||
	       (exception.name.length == 0 &&
		exception.message.length == 0))
	    {
		window.console.error('Error with a XMLHttpRequest');
	    }
	    else
	    {
		window.console.error(exception.name +": "+ exception.message);
	    }
	}
	return false;
    }
}

function getXmlResponseOfXMLHttpRequest(request)
{
    'use strict';
    
    if(request.responseXML)
    {
	return request.responseXML;
    }
    
    var parser = new DOMParser();
    if(request.responseText)
    {
	return parser.parseFromString(request.responseText, "text/xml");
    }
    if(request.response)
    {
	return parser.parseFromString(request.response, "text/xml");
    }
    return null;
}
