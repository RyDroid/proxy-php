/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


var XsltProcessorCache = { 'elements' : {} };

/**
 * @param {String|string} url
 * @param {Node} xsltDocument
 * @return {boolean}
 */
XsltProcessorCache.loadWithDocument = function(url, xsltDocument)
{
    'use strict';
    
    if(typeof(getXsltProcessor) != 'function')
    {
        return false;
    }
    var xsltProcessor = getXsltProcessor();
    if(typeof(xsltProcessor) != 'object' ||
       typeof(xsltProcessor.importStylesheet) != 'function')
    {
        return false;
    }
    
    xsltProcessor.importStylesheet(xsltDocument);
    XsltProcessorCache['elements'][url] = xsltProcessor;
    return true;
};

/**
 * @param {String|string} url
 * @param {String|string} xsltString
 * @return {boolean}
 */
XsltProcessorCache.loadWithString = function(url, xsltString)
{
    'use strict';
    return XsltProcessorCache.loadWithDocument(
        url,
        (new DOMParser()).parseFromString(xsltString.toString(), 'text/xml')
    );
};

/**
 * @param {String|string} url
 * @param {boolean=} async
 * @return {boolean}
 */
XsltProcessorCache.loadWithHttpRequest = function(url, async)
{
    'use strict';
    
    if(typeof(getXMLHttpRequest) != 'function')
    {
        return false;
    }
    var httpRequest = getXMLHttpRequest();
    if(httpRequest == null)
    {
        return false;
    }
    
    if(typeof(async) != 'boolean')
    {
        async = false;
    }
    
    httpRequest.open('GET', url, async);
    sendXMLHttpRequest(httpRequest);
    var xsltString = getXmlResponseOfXMLHttpRequest(httpRequest);
    if(xsltString == null)
    {
        return false;
    }
    return XsltProcessorCache.loadWithString(url, xsltString);
};

XsltProcessorCache.hasUrl = function(url)
{
    'use strict';
    return url in XsltProcessorCache['elements'];
};

/**
 * @param {String|string} url
 * @return {Object}
 */
XsltProcessorCache.getUnsafe = function(url)
{
    'use strict';
    return XsltProcessorCache['elements'][url];
};

/**
 * @param {String|string} url
 * @param {boolean=} async
 * @return {boolean}
 */
XsltProcessorCache.loadAndDo = function(url, functionToCallWhenGot, async)
{
    'use strict';
    
    if(XsltProcessorCache.hasUrl(url))
    {
        functionToCallWhenGot();
        return true;
    }
    
    if(typeof(getXMLHttpRequest) != 'function')
    {
        return false;
    }
    var httpRequest = getXMLHttpRequest();
    if(httpRequest == null)
    {
        return false;
    }
    
    if(typeof(async) != 'boolean')
    {
        async = true;
    }

    httpRequest.onreadystatechange = function()
    {
        'use strict';
        if(!isNaN(httpRequest.readyState) && httpRequest.readyState == 4)
        {
            if(!isNaN(httpRequest.status) && httpRequest.status === 200)
            {
                var xmlString = httpRequest.responseText;
                if(XsltProcessorCache.loadWithString(url, xmlString))
                {
                    functionToCallWhenGot();
                }
            }
        }
    };
    httpRequest.open('GET', url, async);
    return sendXMLHttpRequest(httpRequest);
};

/**
 * @param {String|string} url
 * @return {Object}
 */
XsltProcessorCache.get = function(url)
{
    'use strict';
    if(url.length == 0)
    {
        return null;
    }
    if(XsltProcessorCache.hasUrl(url))
    {
        return XsltProcessorCache.getUnsafe(url);
    }
    if(XsltProcessorCache.loadWithHttpRequest(url))
    {
        return XsltProcessorCache.getUnsafe(url);
    }
    return null;
};

XsltProcessorCache.clean = function()
{
    'use strict';
    XsltProcessorCache['elements'] = {};
};
