<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!--
      Copying and distribution of this file, with or without modification,
      are permitted in any medium without royalty provided this notice is
      preserved.  This file is offered as-is, without any warranty.
      Names of contributors must not be used to endorse or promote products
      derived from this file without specific prior written permission.
  -->
  
  <xsl:output method="xml" omit-xml-declaration="no" indent="yes" />
  
  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
