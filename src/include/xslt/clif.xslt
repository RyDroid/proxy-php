<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!--
      Copyright (C) 2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
      
      This program is free software: you can redistribute it and/or modify
      it under the terms of the GNU Lesser General Public License as published
      by the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.
      
      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
      GNU Lesser General Public License for more details.
      
      You should have received a copy of the GNU Lesser General Public License
      along with this program. If not, see <https://www.gnu.org/licenses/>.
  -->
  
  
  <xsl:output encoding             = "utf-8"
              indent               = "yes"
              method               = "xml"
              omit-xml-declaration = "no"
              version              = "1.0" />
  
  
  <xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'" />
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
  
  
  <xsl:template match="session">
    <scenario>
      <behaviors>
	<plugins>
	  <use id="replayHttp" name="HttpInjector"></use>
	  <use id="replayTimer" name="ConstantTimer"></use>
	</plugins>
	
	<behavior id="session">
	  <xsl:apply-templates />
	</behavior>
      </behaviors>
      
      <loadprofile>
	<group behavior="session" forceStop="false">
	  <ramp style="line">
	    <points>
	      <point x="0" y="1"></point>
	      <point x="1" y="1"></point>
	    </points>
	  </ramp>
	</group>
      </loadprofile>
    </scenario>
  </xsl:template>
  
  <xsl:template match="ressource">
    <!-- In case the tag name was in french instead of english -->
    <xsl:call-template name="resource" />
  </xsl:template>
  
  <xsl:template match="resource" name="resource">
    <sample>
      <xsl:attribute name="use">replayHttp</xsl:attribute>
      <xsl:choose>
	<xsl:when test="method[string(@value)]">
	  <xsl:attribute name="name">
	    <!-- CLIF needs lower-case for HTTP method -->
	    <xsl:value-of select="translate(method/@value, $uppercase, $lowercase)" />
	  </xsl:attribute>
	</xsl:when>
      </xsl:choose>
      
      <params>
	<xsl:apply-templates />
      </params>
    </sample>
  </xsl:template>
  
  <xsl:template match="url">
    <xsl:element name="param">
      <xsl:attribute name="name">uri</xsl:attribute>
      <xsl:attribute name="value">
	<xsl:choose>
	  <xsl:when test="string(@value)">
	    <xsl:value-of select="@value" />
	  </xsl:when>
	  <xsl:otherwise>
            <xsl:value-of select="." />
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:attribute>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="postFields">
    <xsl:element name="param">
      <xsl:attribute name="name">bodyparameters</xsl:attribute>
      <xsl:attribute name="value">
	<xsl:apply-templates select="field" />
	<xsl:apply-templates select="postField" />
      </xsl:attribute>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="postField">
    <xsl:call-template name="field" />
  </xsl:template>
  
  <xsl:template match="field" name="field">
    <xsl:if test="string(./key)">
      <xsl:value-of select="./key" />
      <xsl:text>=</xsl:text>
      <xsl:call-template name="header-escape-semicolon">
	<xsl:with-param name="replace" select="./value" />
      </xsl:call-template>
      <xsl:text>;</xsl:text>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="headers">
    <xsl:element name="param">
      <xsl:attribute name="name">headers</xsl:attribute>
      <xsl:attribute name="value">
	<!-- HTTP headers are just one big string parameter. :( -->
	<xsl:apply-templates select="header" />
	<!-- The select is useful,
	     without it xsltproc add nasty whitespaces. -->
      </xsl:attribute>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="header">
    <!-- A header is like a row of a table with 2 columns. -->
    <!-- Pipe ("|") is used as a delimiter of a cell. -->
    <!-- Semicolon (";") is used as a delimiter of a row. -->
    <xsl:text>header=</xsl:text>
    <xsl:value-of select="@key" />
    <xsl:text>|value=</xsl:text>
    <xsl:call-template name="header-escape-semicolon">
      <xsl:with-param name="replace" select="@value" />
    </xsl:call-template>
    <xsl:text>|;</xsl:text>
  </xsl:template>
  
  <xsl:template name="header-escape-all">
    <xsl:param name="replace" />
    <xsl:call-template name="header-escape-semicolon">
      <xsl:with-param name="replace" select="$replace" />
    </xsl:call-template>
    <xsl:call-template name="header-escape-pipe">
      <xsl:with-param name="replace" select="$replace" />
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template name="header-escape-semicolon">
    <xsl:param name="replace" />
    <xsl:call-template name="header-escape-one-character">
      <xsl:with-param name="string" select="$replace" />
      <xsl:with-param name="character" select="';'" />
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template name="header-escape-pipe">
    <xsl:param name="replace" />
    <xsl:call-template name="header-escape-one-character">
      <xsl:with-param name="string" select="$replace" />
      <xsl:with-param name="character" select="'|'" />
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template name="header-escape-one-character">
    <!-- Thanks to
	 https://stackoverflow.com/questions/16239431/
	 escaping-value-of-xml-attribute-with-xsltproc#answer-16241733 -->
    <xsl:param name="string" />
    <xsl:param name="character" />
    <xsl:choose>
      <xsl:when test="contains($string, $character)">
        <xsl:value-of select="substring-before($string, $character)" />
        <xsl:text>\</xsl:text>
        <xsl:value-of select="$character" />
        <xsl:call-template name="header-escape-one-character">
          <xsl:with-param name="string"
                          select="substring-after($string, $character)" />
          <xsl:with-param name="character" select="$character" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$string" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="proxy">
    <xsl:choose>
      <xsl:when test="string(@url)">
	<xsl:call-template name="proxy-url">
	  <xsl:with-param name="url" select="@url" />
	</xsl:call-template>
      </xsl:when>
      <xsl:when test="string(@host)">
	<xsl:call-template name="proxy-url">
	  <xsl:with-param name="url" select="@host" />
	</xsl:call-template>
      </xsl:when>
    </xsl:choose>
    <xsl:choose>
      <xsl:when test="string(@port)">
	<xsl:call-template name="proxy-port">
	  <xsl:with-param name="port" select="@port" />
	</xsl:call-template>
      </xsl:when>
      <xsl:when test="string(@portnumber)">
	<xsl:call-template name="proxy-port">
	  <xsl:with-param name="port" select="@portnumber" />
	</xsl:call-template>
      </xsl:when>
      <xsl:when test="string(@port-number)">
	<xsl:call-template name="proxy-port">
	  <xsl:with-param name="port" select="@port-number" />
	</xsl:call-template>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template name="proxy-url">
    <xsl:param name="url" />
    <xsl:if test="string(normalize-space($url))">
      <xsl:element name="param">
	<xsl:attribute name="name">proxyhost</xsl:attribute>
	<xsl:attribute name="value">
	  <xsl:value-of select="$url" />
	</xsl:attribute>
      </xsl:element>
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="proxy-port">
    <xsl:param name="port" />
    <xsl:if test="string(number($port)) != 'NaN'">
      <xsl:element name="param">
	<xsl:attribute name="name">proxyport</xsl:attribute>
	<xsl:attribute name="value">
	  <xsl:value-of select="$port" />
	</xsl:attribute>
      </xsl:element>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="pause[string(number(@value)) != 'NaN']">
    <timer use="replayTimer" name="sleep">
      <params>
	<xsl:element name="param">
	  <xsl:attribute name="name">duration_arg</xsl:attribute>
	  <xsl:attribute name="value">
	    <xsl:variable name="value">
	      <xsl:choose>
		<xsl:when test="string(@unit) = 'ns' or
				string(@unit) = 'nanosecond' or
				string(@unit) = 'nanoseconds'">
		  <xsl:value-of select="@value div 1000000" />
		</xsl:when>
		<xsl:when test="string(@unit) = 'us' or
				string(@unit) = 'microsecond' or
				string(@unit) = 'microseconds'">
		  <xsl:value-of select="@value div 1000" />
		</xsl:when>
		<xsl:when test="string(@unit) = 'ms' or
				string(@unit) = 'millisecond' or
				string(@unit) = 'milliseconds'">
		  <xsl:value-of select="@value" />
		</xsl:when>
		<xsl:when test="string(@unit) = 's' or
				string(@unit) = 'second' or
				string(@unit) = 'seconds'">
		  <xsl:value-of select="@value * 1000" />
		</xsl:when>
		<xsl:otherwise>
		  <xsl:value-of select="@value" />
		</xsl:otherwise>
	      </xsl:choose>
	    </xsl:variable>
	    <!-- CLIF does not accept float for that, integer needed -->
	    <xsl:value-of select="round($value)" />
	  </xsl:attribute>
	</xsl:element>
      </params>
    </timer>
  </xsl:template>
</xsl:stylesheet>
