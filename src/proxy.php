<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


require_once('kernel.inc.php');


$url = (
    ProxifierGetParametersDefaultUtils::
    getUrlToProxifyWithEncodedParameters()
);
if($url == '')
{
    echo 'ERROR: Undefined URL!';
    exit(1);
}

UrlProxifierDefaultUtils::setBaseProxifiedUrlWithoutFileOrExit($url);
$headersClient = HttpHeaders::get();
HttpHeadersUtils::removeServerSpecificKeys($headersClient);
if(DebugUtils::isOn())
{
    $headersClient->addStringLine('URL-to-proxify: '. $url);
}
$result =
    GeneralProxifierCatcherUtils::proxifyRemoteContentWithDefaultConfiguration(
        $url, $headersClient
    );

$headersServer = $result->getHeader();
HttpHeadersProxifierUtils::sendProxifiedHttpHeaders($headersServer);

$content = $result->getContent();
if(!$result->hasNoError())
{
    if(LogsConfigurationGeneral::hasToSave())
    {
        LoggerUtils::logStrings($result->getErrors());
    }
    if(empty(trim($content)))
    {
        header('Content-Type: text/plain');
        $content = implode(PHP_EOL, $result->getErrors());
        $content .= PHP_EOL;
    }
}
echo $content;
