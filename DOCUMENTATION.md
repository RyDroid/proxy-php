# Documentation

## Configuration

### Order

It can be seen as a cascading configuration.
For each pair key/value, it fetchs the value of the first layer,
then it returns it if it is defined,
otherwise it does the same thing for the next layer.

1. [Query string (GET parameters)](https://en.wikipedia.org/wiki/Query_string)
2. [HTTP cookies](https://en.wikipedia.org/wiki/HTTP_cookie)
3. [PHP session](https://secure.php.net/manual/en/book.session.php)
4. Default specific INI file
5. Default general INI file
6. Default system general INI file
7. Default values (hard-coded in source code)

Everything can be configured through files.
In fact, having write access to files implies to control the instance.
For example, changing file paths (for example for logs or session captured)
can only be done through files.

### INI files for configuration

#### Names of files

- `configuration.ini`: every thing can be configured in this single file
- `proxy.ini`: only for the proxy used
- `session-captured.ini`: only for the potentiel session captured
- `logs.ini`: only for the logs

#### Parsing of INI files

- It works as `key=value`
- Some properties can be configured with more than one key
- One property should be used only once, no order is guaranteed
- Some keys are considered in no section or in at least a specific one
- A key in a section can be linked to an other property in a different section
- There is no error and no warning if a key is not recognized

#### Property and keys

- Proxy (`proxy`)
  - URL (`proxy-url`)
  - Port number (`proxy-port`)
- Session captured (`session-captured`)
  - Save (`save-session`)
    - Set to true if you want to recorder a session to use it later with a tool like CLIF
    - Set to false if you only need a web proxy
  - Create automatically (`session-create-auto`)
  - Save the configuration of the proxy used (`save-proxy`)
  - Directory (`session-captured-directory`)
  - Use system tempory directory (`session-captured-use-tmp`)
  - HTTP
    - Save HTTP cookies (`save-http-cookies`)
    - Save HTTP user-agent (`save-http-user-agent`)
    - Save HTTP referer (`save-http-referer`)
    - Save HTTP encoding (`save-http-encoding`)
    - Save HTTP language (`save-http-language`)
    - Save HTTP ETag (`save-http-etag`)
    - Save HTTP DNT (`save-http-do-not-track`)
- Logs (`logs`)
  - Save (`save-logs`)
  - Format (`logs-format`) (plain-text or write yourself the code for an other format)
  - File name (`logs-file-name`)
  - Directory path (`logs-directory`)
  - Use system tempory directory (`logs-use-tmp-dir`)
  - Can be shown (`show-logs`)
- User Interface (`user-interface`)
  - Show information for debugging (`show-debug`)
  - Show the configuration of the proxy through GET parameters
    (`show-proxy-get-parameters-configuration`)
  - Show the configuration of the proxy through cookies
    (`show-proxy-cookies-configuration`)
  - Use scripts on client (like JavaScript) (`use-client-scripts`)

#### Value of keys

- Potential values are generally obvious
- Boolean values have to be considered in an extensive way
  - Values for true: true, yes, on, oui
  - Values for false: false, no, off, non

## Web API

- `proxy.php` needs the GET parameter `url-to-proxify`
  and returns the ressource proxified.
  Of course, it saved the request in the session,
  if the configuration allows it.

- Session
  - `session-initialize.php`
     starts a session if there is none.
  - `session-destroy.php`
     destroys the current if there is one.
  - `session-reinitialize.php`
    does the job of `session-destroy.php`
    and then the one of `session-initialize.php`.
  
  They support the GET parameter `redirect`.
  If set to "yes", it redirects to the previous page,
  and does not if set to "no".
  The default behaviour may change.
  It may work with a POST parameter,
  but it is not officially supported.

- Data of the session
  - `data-raw.php`
     returns the data of the session
     as it is saved by this program.
  - `data-converted.php`
    returns the data converted in a specific format.
    You have to implement it yourself or ask someone to do it.
    You can also use the JavaScript functions that uses a XSLT.

- Configuration
  - `configuration-get.php`
    returns the current configuration.
    It supports the GET parameter "from"
    that can at least takes this values : "general", "cookies", and "default".
    It supports the GET parameter "format"
    that can at least takes this values : "html" or "xhtml".
  - `configuration-proxy-set.php`
    sets the configuration for a potential proxy.
    It supports GET parameters "proxy-url" and "proxy-port".
    It is not implemented.
  - `configuration-session-set.php`
    sets the configuration for the session captured.
    It supports GET parameters :
    "session-save-state", "session-save-proxy",
    "session-save-cookies", "session-save-user-agent",
    "session-save-language", "session-save-encoding",
    "session-save-etag", "session-save-dnt".
    It is not implemented.
