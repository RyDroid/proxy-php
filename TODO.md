# Things to do

## Back-end

- `//html/body/div[string(@data-image-url)]`
- `configuration.php`: input for auto-creation of a session
- Encoding problem with http://www.boursorama.com/ (2017-05-31)
- Fix CSS problems
  - URL of fonts not proxified with http://www.boursorama.com/ (2017-05-31)
  - Problem with url function of CSS with https://linuxfr.org/ (2017-06-02)
  - Use a CSS parser ?
    - Simple PHP CSS Parser
      https://github.com/intekhabrizvi/cssparser
    - PHP CSS Parser
      https://github.com/sabberworm/PHP-CSS-Parser
    - Horde CSS Parser
      https://packages.debian.org/stretch/php-horde-css-parser
- Unproxify HTTP header referer
- Show or not the configuration (interface.show or UI.show in INI file)
- Add an option use-proxy in configuration
- *Configuration to INI string
  - Generate the example configuration with default values
- Export configuration to INI string
- Add an option to save or not proxy information in export form
- Manage integrity attribute of HTML
  https://developer.mozilla.org/en-US/docs/Web/Security/Subresource_Integrity
- Manage Atom and RSS

## Front-end

- Ways to get the result of a session
  - View it in HTML
  - Download converted XML
    - [CLIF](https://clif.ow2.io/)
      - Separate query string
    - [Apache JMeter](https://jmeter.apache.org/)
    - [Tsung](http://tsung.erlang-projects.org/)
    - [Selenium](http://www.seleniumhq.org/)
  - Download converted [YAML](https://en.wikipedia.org/wiki/YAML)
    - [Taurus](https://gettaurus.org/)
- Show the URL of the page visited and the number of pages visited
- Use an already created CSS theme for the GUI?
  (like [Milligram](http://milligram.io/),
  [KNACSS](https://www.knacss.com/), or
  [Bootstrap](https://getbootstrap.com/))
- Import and export a configuration in INI format

## Others

- Split the GUI and the "API" (2 git repositories)
