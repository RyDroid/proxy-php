# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided this notice is
# preserved.  This file is offered as-is, without any warranty.
# Names of contributors must not be used to endorse or promote products
# derived from this file without specific prior written permission.


# Folders and files
## Folders
SRC_DIR=src
SRC_PHP_DIR=$(SRC_DIR)/include/php
SRC_JS_DIR=$(SRC_DIR)/include/js
SRC_XSLT_DIR=$(SRC_DIR)/include/xslt
CHK_DIR=tests/php
DOC_DIR=doc
## Files
CLIF_XSLT=$(SRC_XSLT_DIR)/clif.xslt

# Commands
KWSTYLE=KWStyle -v -gcc
KWSTYLE_DIR=$(KWSTYLE) -d -R
## PHP
PHPMD=phpmd
PHPCPD=phpcpd
PHPCS=phpcs
PHPUNIT=phpunit
PEAR=pear
## JavaScript
CLOSURE_COMPILER_CHECK=closure-compiler --warning_level VERBOSE
## XML
XMLCHECK=xmllint --noout
SAXONB-XSLT=saxonb-xslt -ext:off -versionmsg:off
SAXON-HE-XSLT=java -jar /usr/share/java/Saxon-HE.jar -warnings:silent
## Debian
APT=apt
DEBUILD_BASE=debuild --lintian
DEBUILD_BASE_NO_SIGN=$(DEBUILD_BASE) -us -uc
DEBUILD_LINTIAN_OPTIONS=--lintian-opts -i
DEBUILD=$(DEBUILD_BASE) $(DEBUILD_LINTIAN_OPTIONS)
DEBUILD_NO_SIGN=$(DEBUILD_BASE_NO_SIGN) $(DEBUILD_LINTIAN_OPTIONS)

# Package and archive
PACKAGE=rydroid-web-proxy
FILES_TO_BUILD=$(SRC_DIR)/* $(CHK_DIR)/* makefile composer.json
FILES_FOR_CI=.travis.yml .gitlab-ci.yml
FILES_FOR_TESTS=$(FILES_FOR_CI) *.xml yamllint.yml tools/
FILES_TXT_TO_ARCHIVE=\
	$(FILES_TO_BUILD) \
	.gitignore .editorconfig \
	$(FILES_FOR_TESTS) \
	*.md licenses/* \
	debian/ docker-compose.yml
FILES_TO_ARCHIVE=$(FILES_TXT_TO_ARCHIVE) logo.svg
INSTALL_MAIN_DIR=$(DESTDIR)/usr/share/$(PACKAGE)
INSTALL_CONF_DIR=$(DESTDIR)/etc/$(PACKAGE)
INSTALL_DATA_DIR=$(DESTDIR)/var/lib/$(PACKAGE)
INSTALL_ICON_DIR=$(DESTDIR)/usr/share/icons


.PHONY: \
	default all build \
	test test-static test-static-xml test-static-yaml test-dynamic \
	read-doc-web doc doc-included doc-web credits-included \
	packages archives dist default-archive \
	zip tar-gz tar-bz2 tar-xz 7z \
	install uninstall \
	clean-git


default: build

all: test packages build

build: doc-included credits-included


check: test

test: test-static test-dynamic

test-except-phpmd: test-static-except-phpmd test-dynamic

test-static: \
	test-static-generic \
	test-static-php test-static-js \
	test-static-xml test-static-yaml

test-static-except-phpmd: test-static-generic test-static-php-except-phpmd

test-static-generic: test-encoding test-kwstyle test-vera++

test-static-php: test-php-syntax test-phpmd test-static-php-except-phpmd

test-static-php-except-phpmd: test-phpcpd test-phpcs

test-encoding:
	@chmod +x ./tools/encoding-tests.sh
	@sh -n ./tools/encoding-tests.sh
	./tools/encoding-tests.sh \
		$(FILES_TXT_TO_ARCHIVE)

test-kwstyle:
	@chmod +x ./tools/kwstyle-files.sh
	@sh -n ./tools/kwstyle-files.sh
	./tools/kwstyle-files.sh $(SRC_DIR)/*.php
	$(KWSTYLE_DIR) $(SRC_DIR)/include/
	$(KWSTYLE_DIR) $(CHK_DIR)/

test-vera++:
	@chmod +x ./tools/vera++-tests.sh
	@sh -n ./tools/vera++-tests.sh
	./tools/vera++-tests.sh $(SRC_DIR)/ $(CHK_DIR)/

test-php-syntax:
	@chmod +x ./tools/php-syntax-check.sh
	@sh -n ./tools/php-syntax-check.sh
	./tools/php-syntax-check.sh $(SRC_DIR)/ $(CHK_DIR)/

test-phpmd: ruleset.xml
	$(PHPMD) $(SRC_DIR)/ text ruleset.xml
	$(PHPMD) $(CHK_DIR)/ text ruleset.xml

test-phpcpd:
	$(PHPCPD) $(SRC_DIR)/ $(CHK_DIR)/

test-phpcs: coding-standard.xml
	$(PHPCS) --encoding=utf-8 --standard=coding-standard.xml \
		$(SRC_DIR)/proxy.php $(SRC_DIR)/session*.php $(CHK_DIR)/

test-static-js: test-closure-compiler

test-closure-compiler:
	$(CLOSURE_COMPILER_CHECK) \
		$(SRC_JS_DIR)/utils/* $(SRC_JS_DIR)/specific/* \
		> /dev/null

test-static-xml:
	$(XMLCHECK) $(SRC_XSLT_DIR)/*

test-static-yaml:
	yamllint -c yamllint.yml \
		.gitlab-ci.yml .travis.yml docker-compose.yml

test-dynamic: test-phpunit test-dynamic-xml

test-phpunit: unit-tests.xml
	@$(PHPUNIT) --atleast-version 5
	$(PHPUNIT) \
		--report-useless-tests \
		--strict-coverage \
		--strict-global-state \
		--disallow-test-output \
		-c unit-tests.xml

test-dynamic-xml: test-xsltproc test-xalan test-saxon

test-xsltproc:
	xsltproc $(CLIF_XSLT) tests/xml/session.xml > /dev/null

test-xalan:
	xalan \
		-xsl $(CLIF_XSLT) \
		-in tests/xml/session.xml \
		> /dev/null

test-saxon:
	saxon-xslt tests/xml/session.xml $(CLIF_XSLT) > /dev/null
	$(SAXONB-XSLT) \
		tests/xml/session.xml $(CLIF_XSLT) > /dev/null
	$(SAXON-HE-XSLT) \
		tests/xml/session.xml -xsl:$(CLIF_XSLT) > /dev/null


packages: archives debian-package

debian-package: $(PACKAGE).deb

$(PACKAGE).deb: doc-included credits-included
	$(DEBUILD)

archives: zip tar-gz tar-bz2 tar-xz 7z

dist: default-archive

default-archive: tar-bz2

zip: $(PACKAGE).zip

$(PACKAGE).zip: $(FILES_TO_ARCHIVE)
	zip $(PACKAGE).zip -r -- \
		$(FILES_TO_ARCHIVE) \
		> /dev/null

tar-gz: $(PACKAGE).tar.gz

$(PACKAGE).tar.gz: $(FILES_TO_ARCHIVE)
	tar -zcvf $(PACKAGE).tar.gz -- \
		$(FILES_TO_ARCHIVE) \
		> /dev/null

tar-bz2: $(PACKAGE).tar.bz2

$(PACKAGE).tar.bz2: $(FILES_TO_ARCHIVE)
	tar -jcvf $(PACKAGE).tar.bz2 -- \
		$(FILES_TO_ARCHIVE) \
		> /dev/null

tar-xz: $(PACKAGE).tar.xz

$(PACKAGE).tar.xz: $(FILES_TO_ARCHIVE)
	tar -cJvf $(PACKAGE).tar.xz -- \
		$(FILES_TO_ARCHIVE) \
		> /dev/null

7z: $(PACKAGE).7z

$(PACKAGE).7z: $(FILES_TO_ARCHIVE)
	7z a -t7z $(PACKAGE).7z \
		$(FILES_TO_ARCHIVE) \
		> /dev/null


read-doc-web: doc-web
	xdg-open $(DOC_DIR)/html/index.html &

doc: doc-web doc-included

doc-web: Doxyfile $(SRC_DIR)/* *.md
	doxygen

doc-included: src/documentation.html

src/documentation.html: DOCUMENTATION.md
	pandoc -f commonmark --self-contained \
		DOCUMENTATION.md -o src/documentation.html


credits-included: src/credits.html

src/credits.html: CREDITS.md
	pandoc -f commonmark --self-contained \
		CREDITS.md -o src/credits.html


install-tools-from-apt: \
	install-needed-tools-from-apt \
	install-analysis-tools-from-apt \
	install-other-tools-from-apt

install-other-tools-from-apt: install-doc-tools-from-apt
	$(APT) install git

install-doc-tools-from-apt:
	$(APT) install doxygen graphviz

install-analysis-tools-from-apt: \
	install-static-analysis-tools-from-apt \
	install-dynamic-analysis-tools-from-apt

install-static-analysis-tools-from-apt: \
	install-static-generic-analysis-tools-from-apt \
	install-static-php-analysis-tools-from-apt \
	install-static-js-analysis-tools-from-apt \
	install-static-xml-analysis-tools-from-apt \
	install-static-yaml-analysis-tools-from-apt

install-static-generic-analysis-tools-from-apt:
	$(APT) install kwstyle vera++

install-static-php-analysis-tools-from-apt: install-php-from-apt
	$(APT) install phpmd phpcpd php-codesniffer

install-static-js-analysis-tools-from-apt:
	$(APT) install closure-compiler

install-static-xml-analysis-tools-from-apt:
	$(APT) install \
		libxml2-utils xsltproc xalan \
		libsaxon-java libsaxonb-java libsaxonhe-java

install-static-yaml-analysis-tools-from-apt:
	$(APT) install yamllint

install-dynamic-analysis-tools-from-apt: install-php-from-apt
	$(APT) install phpunit

install-needed-tools-from-apt: \
	install-php-with-libs-from-apt \
	install-apache-with-php-from-apt
	$(APT) install pandoc

install-pear-from-apt: install-php-from-apt
	$(APT) install php-pear

install-apache-with-php-from-apt: install-php-from-apt
	$(APT) install apache2 libapache2-mod-php

install-php-with-libs-from-apt: install-php-from-apt install-php-libs-from-apt

install-php-libs-from-apt: install-php-needed-libs-from-apt
	$(APT) install php-curl || \
		$(APT) install php7-curl || \
		$(APT) install php7.1-curl || \
		$(APT) install php7.0-curl

install-php-needed-libs-from-apt:
	$(APT) install php-mbstring php-xml || \
		$(APT) install php7-mbstring php7-xml || \
		$(APT) install php7.1-mbstring php7.1-xml || \
		$(APT) install php7.0-mbstring php7.0-xml

install-php-from-apt:
	$(APT) install php || \
		$(APT) install php7 || \
		$(APT) install php7.1 || \
		$(APT) install php7.0


install-phpcs-from-pear:
	if [ `pear list | grep CodeSniffer | wc -l` -eq 0 ]; then \
		$(PEAR) install PHP_CodeSniffer; \
		fi

install-phpcs-from-pyrus: pyrus.phar
	php pyrus.phar install pear/PHP_CodeSniffer

pyrus.phar:
	wget -c 'https://pear2.php.net/pyrus.phar'

install-phpcs-from-web: /usr/bin/phpcs

/usr/bin/phpcs: /usr/share/php/phpcs.phar
	echo '#!/bin/sh' > /usr/bin/phpcs
	echo 'php /usr/share/php/phpcs.phar "$@"' >> /usr/bin/phpcs

/usr/share/php/phpcs.phar: phpcs.phar
	mv phpcs.phar /usr/share/php/

phpcs.phar:
	wget -c 'https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar'

phpmd.phar:
	wget -c 'https://static.phpmd.org/php/latest/phpmd.phar'


install: install-configuration install-service install-desktop

install-service: install-minimal
	@mkdir -p -- '$(DESTDIR)/etc/avahi/services/'
	if test ! -e '$(DESTDIR)/etc/avahi/services/$(PACKAGE).service' -a \
		! -L '$(DESTDIR)/etc/avahi/services/$(PACKAGE).service'; \
		then \
		ln -s -- \
			'$(INSTALL_CONF_DIR)/$(PACKAGE).service' \
			'$(DESTDIR)/etc/avahi/services/'; \
		fi

install-desktop: install-icon install-minimal
	@mkdir -p -- '$(DESTDIR)/usr/share/applications/'
	if test ! -e '$(DESTDIR)/usr/share/applications/$(PACKAGE).desktop' -a \
		! -L '$(DESTDIR)/usr/share/applications/$(PACKAGE).desktop'; \
		then \
		ln -s -- \
			'$(INSTALL_CONF_DIR)/$(PACKAGE).desktop' \
			'$(DESTDIR)/usr/share/applications/'; \
		fi

install-icon:
	@mkdir -p --          '$(INSTALL_ICON_DIR)'
	cp -- logo.svg        '$(INSTALL_ICON_DIR)/$(PACKAGE).svg'
	cp -- src/favicon.png '$(INSTALL_ICON_DIR)/$(PACKAGE).png'
	cp -- src/favicon.ico '$(INSTALL_ICON_DIR)/$(PACKAGE).ico'

install-configuration: install-configuration-web-servers

install-configuration-web-servers: \
	install-configuration-lighttpd \
	install-configuration-apache

install-configuration-lighttpd: install-configuration-common
	if test ! -f '$(DESTDIR)/etc/lighttpd/conf-available/50-$(PACKAGE).conf' -a \
		which lighty-enable-mod >/dev/null 2>&1; \
		then \
		ln -s -- \
			'$(INSTALL_CONF_DIR)/lighttpd.conf' \
			'$(DESTDIR)/etc/lighttpd/conf-available/50-$(PACKAGE).conf'; \
		fi

install-configuration-apache: install-configuration-common
	@mkdir -p -- '$(DESTDIR)/etc/apache2/conf-available'
	ln -sf -- \
		'$(INSTALL_CONF_DIR)/apache.conf' \
		'$(DESTDIR)/etc/apache2/conf-available/$(PACKAGE).conf'

install-configuration-common: prepare-system-data-folder
	@mkdir -p --    '$(INSTALL_CONF_DIR)'
	cp -- install/* '$(INSTALL_CONF_DIR)'
	echo 'session-captured-directory=/var/lib/$(PACKAGE)/' \
		> $(INSTALL_CONF_DIR)/configuration.ini

prepare-system-data-folder:
	@mkdir -p --      '$(INSTALL_DATA_DIR)'
	chown -R www-data '$(INSTALL_DATA_DIR)/'
	chmod -R g+rw     '$(INSTALL_DATA_DIR)/'

install-minimal:
	@mkdir -p -- '$(INSTALL_MAIN_DIR)'
	rsync -avz \
		--exclude data/ \
		--exclude configuration/*.ini \
		--exclude doc/ \
		--exclude cache/ \
		--exclude vendor/ \
		--exclude build/ \
		--exclude $(SRC_DIR)/session*.xml \
		--exclude $(SRC_DIR)/data/*.xml \
		--exclude $(SRC_DIR)/tools/ \
		--exclude $(SRC_DIR)/tests/ \
		-- $(SRC_DIR)/* '$(INSTALL_MAIN_DIR)' \
		> /dev/null
	rm -f -- '$(INSTALL_MAIN_DIR)/session*.xml'

uninstall: \
	uninstall-minimal uninstall-configuration \
	uninstall-service uninstall-icon uninstall-desktop

uninstall-configuration: uninstall-web-servers-configuration uninstall-service

uninstall-service:
	if test -h '$(DESTDIR)/etc/avahi/services/$(PACKAGE).service'; \
		then \
		rm -f -- '$(DESTDIR)/etc/avahi/services/$(PACKAGE).service'; \
		fi

uninstall-desktop:
	if test -h '$(DESTDIR)/usr/share/applications/$(PACKAGE).desktop';
		then \
		rm -f -- '$(DESTDIR)/usr/share/applications/$(PACKAGE).desktop'; \
		fi

uninstall-icon:
	rm -f -- '$(INSTALL_ICON_DIR)/$(PACKAGE).*'

uninstall-web-servers-configuration: \
	uninstall-configuration-common \
	uninstall-lighttpd-configuration \
	uninstall-apache-configuration

uninstall-lighttpd-configuration:
	if test -f '$(DESTDIR)/etc/lighttpd/conf-available/50-$(PACKAGE).conf'; \
		then \
		rm -f -- '$(DESTDIR)/etc/lighttpd/conf-available/50-$(PACKAGE).conf'; \
		if test -h '$(DESTDIR)/etc/lighttpd/conf-enabled/50-$(PACKAGE).conf'; \
			then \
			echo 'Manually deleting lighttpd/$(PACKAGE) configuration link'; \
			rm -f -- '$(DESTDIR)/etc/lighttpd/conf-enabled/50-$(PACKAGE).conf'; \
			fi; \
		fi

uninstall-apache-configuration:
	if test -f '$(DESTDIR)/etc/apache2/conf-available/$(PACKAGE).conf';
		then
		rm -f -- '$(DESTDIR)/etc/apache2/conf-available/$(PACKAGE).conf';
		fi

uninstall-configuration-common:
	rm -rf -- '$(INSTALL_CONF_DIR)'

uninstall-minimal:
	rm -rf -- '$(INSTALL_MAIN_DIR)'


clean: \
	clean-build clean-bin clean-compile-tmp \
	clean-python clean-profiling clean-cmake clean-ide \
	clean-tmp clean-doc clean-latex clean-tests clean-archives
	$(RM) -rf -- \
		$(SRC_DIR)/data/session*.xml $(SRC_DIR)/data/*.lock \
		$(SRC_DIR)/externals composer.lock \
		src/configuration/*.ini \
		url-to-test.txt debian-todo* \
		rydroid-proxy-php* rydroid-web-proxy* ..deb

clean-build: clean-build-release clean-build-debug
	@$(RM) -rf -- build*/ Build*/ builds*/ Builds*/

clean-build-release:
	@$(RM) -rf -- release/ Release/

clean-build-debug:
	@$(RM) -rf -- dbg/ DBG/ debug/ Debug/

clean-bin:
	$(RM) -rf -- \
		*.o *.a *.so *.ko *.lo *.dll *.out \
		bin/ BIN/ binaries/

clean-compile-tmp:
	$(RM) -f -- \
		*.asm *.ihx *.lk *.lst *.map *.mem \
		*.rel *.rst *.s *.sym

clean-python:
	$(RM) -rf -- *.pyc __pycache__

clean-profiling:
	$(RM) -f -- callgrind.out.*

clean-cmake:
	$(RM) -rf -- \
		CMakeLists.txt.user CMakeCache.txt \
		CMakeFiles/ cmake_install.cmake \
		CPackConfig.cmake CPackSourceConfig.cmake \
		ctest.cmake CTest.cmake \
		CTestfile CTestFile \
		CTestfile.cmake CTestFile.cmake \
		CTestTestfile CTestTestFile \
		CTestTestfile.cmake CTestTestFile.cmake

clean-ide: clean-qt-creator clean-codeblocks

clean-qt-creator:
	$(RM) -f -- qmake_makefile *.pro.user

clean-codeblocks:
	$(RM) -f -- *.cbp *.CBP

clean-tmp:
	$(RM) -rf -- \
		*~ .\#* \#* \
		*.swp *.swap *.SWP *.SWAP \
		*.bak *.backup *.BAK *.BACKUP \
		*.sav *.save *.SAV *.SAVE \
		*.autosav *.autosave \
		*.log *.log.* error_log* log/ logs/ \
		.cache/ .thumbnails/ \
		.CACHE/ .THUMBNAILS/

clean-doc:
	@$(RM) -rf -- \
		doc/ Doc/ docs/ Docs/ \
		documentation/ Documentation/

clean-latex:
	$(RM) -f -- \
		*.pdf *.dvi *.ps \
		*.PDF *.DVI *.PS \
		*.acn *.aux *.bcf *.cut *.fls *.glo *.ist *.lof *.nav \
		*.run.xml *.snm *.toc *.vrb *.vrm *.xdy \
		*.fdb_latexmk *-converted-to.* \
		*.synctex *.synctex.xz \
		*.synctex.gz *.synctex.gzip \
		*.synctex.bz2 *.synctex.bzip \
		*.synctex.lz *.synctex.lzma \
		*.synctex.zip *.synctex.7z *.synctex.rar

clean-tests: clean-tests-folders clean-tests-files

clean-tests-folders:
	$(RM) -rf -- tests-gcc/ tests-clang/ tests-llvm/

clean-tests-files:
	$(RM) -rf -- \
		a b c a.* b.* c.* \
		test.c tests.c \
		test.cc tests.cc test.cpp tests.cpp \
		test.java tests.java \
		test.py tests.py \
		test.m tests.m \
		junit* report* TEST*

clean-archives: clean-archives-tar clean-archives-java clean-archives-php clean-archives-os
	$(RM) -f -- \
		*.deb *.rpm *.ipa \
		*.DEB *.RPM *.IPA \
		*.gz *.gzip *.bz2 *.bzip *.lz *.lzma *.xz \
		*.GZ *.GZIP *.BZ2 *.BZIP *.LZ *.LZMA *.XZ \
		*.zip *.7z *.rar \
		*.ZIP *.7Z *.RAR \
		*.iso *.ciso *.img *.gcz *.wbfs \
		*.ISO *.CISO *.IMG *.GCZ *.WBFS

clean-archives-tar:
	$(RM) -f -- *.tar *.tar.* *.tgz *.TGZ *.tbz2 *.TBZ2

clean-archives-java:
	$(RM) -f -- *.jar *.JAR

clean-archives-php:
	$(RM) -f -- *.phar *.PHAR

clean-archives-os: \
	clean-archives-android clean-archives-windows \
	clean-archives-macos clean-archives-ios

clean-archives-android:
	$(RM) -f -- *.apk *.APK

clean-archives-windows:
	$(RM) -f -- *.exe *.EXE *.msi *.MSI

clean-archives-macos:
	$(RM) -f -- *.dmg *.DMG

clean-archives-ios:
	$(RM) -f -- *.ipa *.IPA

clean-git:
	git clean -fdx
