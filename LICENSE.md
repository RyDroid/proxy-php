# Licensing policy

It is [free/libre software](https://www.gnu.org/philosophy/free-sw.html).
Generally a license is specified as a header of a file.
If there is no information about the license in the header,
the license must be considered to be [LGPLv3.0](https://www.gnu.org/licenses/lgpl.html) (or at your option any later version),
except for files with less than 10 lines that are under [Creative Commons 0 v1.0](https://creativecommons.org/publicdomain/zero/1.0/).
If there is an error due to copy-paste or copyleft,
it would be welcome that you report it.

The file `debian/copyright` gives a lot of information
related to the licenses that are used in this project.
If you find an error, it would be welcome that you report it.
