#!/bin/sh

# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided this notice is
# preserved.  This file is offered as-is, without any warranty.
# Names of contributors must not be used to endorse or promote products
# derived from this file without specific prior written permission.


EXIT_SUCCESS=0
EXIT_FAILURE=1

if ! command -v KWStyle > /dev/null
then
    >&2 echo 'There is no command "KWStyle" in the PATH'
    exit $EXIT_FAILURE
fi
cmd='KWStyle -v -gcc'

exit_code=$EXIT_SUCCESS

for file_or_dir in "$@"
do
    if test ! -e "$file_or_dir"
    then
	>&2 echo "$file_or_dir does not exist!"
	exit $EXIT_FAILURE
    else
	files=$(find "$file_or_dir" -name '*' \
		     -and -not -name '*.jpg' \
		     -and -not -name '*.jpeg' \
		     -and -not -name '*.png' \
		     -and -not -name '*.gif' \
		     -and -not -name '*.webp' \
		     -and -not -name '*.ogg' \
		     -and -not -name '*.oga' \
		     -and -not -name '*.ogv' \
		     -and -not -name '*.mkv' \
		     -and -not -name '*.webm' \
		     -and -not -name '*.mp4' \
		     -and -not -name '*.avi' \
		     -and -not -name '*.mp3' \
		     -and -not -name '*.flac' \
		     -and -not -name '*.opus' \
		     -and -not -name '*.7z' \
		     -and -not -name '*.zip' \
		     -and -not -name '*.rar' \
		     -and -not -name '*.tar' \
		     -and -not -name '*.tar.*' \
		     -and -not -name '*.tgz' \
		     -and -not -name '*.iso')
	for file in $files
	do
	    if test ! -d "$file"
	    then
		$cmd "$file"
		if test $? != 0
		then
		    >&2 echo "$file"
	            exit_code=$EXIT_FAILURE
		fi
	    fi
	done
    fi
done

exit $exit_code
