<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HtmlDocumentProxifierImageSrcTest
    extends PHPUnit_Framework_TestCase
{
    public static function
        proxifyDocumentWithEmptyAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><img src="" alt="" /></body></html>'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><img src=" " alt="" /></body></html>'
        );
    }
    
    private static function
        proxifyDocumentWithFilledAttributeType(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><img src="img.svg" alt="" /></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><img src="img.png" alt="" /></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><img src="img.jpg" alt="" /></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><img src="img.jpeg" alt="" /></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><img src="img.gif" alt="" /></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><img src="img.webp" alt="" /></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
    }
    
    private static function
        proxifyDocumentWithFilledAttributePath(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><img src="img.png" alt="" /></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><img src="/img.png" alt="" /></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><img src="/dir/img.png" alt="" /></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
    }
    
    private static function
        proxifyDocumentWithFilledAttributeChildren(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><img src="img.png" alt="" /></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><img src="/img.png" alt="" /></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><p><img src="img.png" alt="" /></p></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><p><img src="/img.png" alt="" /></p></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
    }
    
    private static function
        proxifyDocumentWithFilledAttributeSpace(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            ' <html><body><img src="img.png" alt="" /></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><img src="img.png" alt="" /></body></html> ',
            '/html/body//img[string(@src)]',
            'src'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            ' <html><body><img src="img.png" alt="" /></body></html> ',
            '/html/body//img[string(@src)]',
            'src'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            PHP_EOL.
            ' <html><body><img src="img.png" alt="" /></body></html> ',
            '/html/body//img[string(@src)]',
            'src'
        );
    }
    
    private static function
        proxifyDocumentWithFilledAttributeDoctype(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><img src="img.png" alt="" /></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<!DOCTYPE html>'.
            '<html><body><img src="img.png" alt="" /></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            ' <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" '.
            '"http://www.w3.org/TR/html4/strict.dtd">'.
            '<html><body><img src="img.png" alt="" /></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            ' <!DOCTYPE html>'.
            '<html><body><img src="img.png" alt="" /></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<!DOCTYPE html>'. PHP_EOL.
            '<html><body><img src="img.png" alt="" /></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            PHP_EOL .' <!DOCTYPE html>'.
            '<html><body><img src="img.png" alt="" /></body></html>',
            '/html/body//img[string(@src)]',
            'src'
        );
    }
    
    public static function
        proxifyDocumentWithFilledAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        self::proxifyDocumentWithFilledAttributeType($testCase, $proxifier);
        self::proxifyDocumentWithFilledAttributePath($testCase, $proxifier);
        self::proxifyDocumentWithFilledAttributeChildren($testCase, $proxifier);
        self::proxifyDocumentWithFilledAttributeSpace($testCase, $proxifier);
        self::proxifyDocumentWithFilledAttributeDoctype($testCase, $proxifier);
    }
    
    
    private $proxifier;
    
    
    public function
        __construct()
    {
        $this->proxifier = new HtmlDocumentProxifierImageSrc();
    }
    
    
    public function
        testProxifyDocumentWithEmptyAttribute()
    {
        self::proxifyDocumentWithEmptyAttribute(
            $this, $this->proxifier
        );
    }
    
    public function
        testProxifyDocumentWithFilledAttribute()
    {
        self::proxifyDocumentWithFilledAttribute(
            $this, $this->proxifier
        );
    }
}
