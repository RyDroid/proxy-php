<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class CssStringProxifierImportTest
    extends CssStringProxifierAbstractTest
{
    public static function
        proxifyLineYesWithOneSeparator(
            CssStringProxifierAbstractTest $testCase,
            CssStringProxifierAbstract $proxifier
        )
    {
        $urlStart = (
            $testCase->getUrl() .'?'.
            ProxifierGetParametersUtils::DEFAULT_KEY_TO_PROXIFY .'='.
            $testCase->getUrlProxifiedEncoded()
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('@import \'custom.css\';'),
            '@import \''. $urlStart .'custom.css\';'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('@import "custom.css";'),
            '@import "'. $urlStart .'custom.css";'
        );
    }
    
    public static function
        proxifyLineYesWithOnePath(
            CssStringProxifierAbstractTest $testCase,
            CssStringProxifierAbstract $proxifier
        )
    {
        $urlStart = (
            $testCase->getUrl() .'?'.
            ProxifierGetParametersUtils::DEFAULT_KEY_TO_PROXIFY .'='.
            $testCase->getUrlProxifiedEncoded()
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('@import "custom.css";'),
            '@import "'. $urlStart .'custom.css";'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('@import "./custom.css";'),
            '@import "'. $urlStart .'custom.css";'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('@import "../custom.css";'),
            '@import "'. $urlStart .
            UrlProxifierCodecUtils::getEncodedUrl('../custom.css')
            .'";'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('@import "../../custom.css";'),
            '@import "'. $urlStart .
            UrlProxifierCodecUtils::getEncodedUrl('../../custom.css')
            .'";'
        );
    }
    
    public static function
        proxifyLineYesWithOneMedia(
            CssStringProxifierAbstractTest $testCase,
            CssStringProxifierAbstract $proxifier
        )
    {
        $urlStart = (
            $testCase->getUrl() .'?'.
            ProxifierGetParametersUtils::DEFAULT_KEY_TO_PROXIFY .'='.
            $testCase->getUrlProxifiedEncoded()
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('@import "custom.css" print;'),
            '@import "'. $urlStart .'custom.css" print;'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine(
                '@import "custom.css" screen, projection;'
            ),
            '@import "'. $urlStart .'custom.css" screen, projection;'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine(
                '@import "custom.css" screen and (orientation:landscape);'
            ),
            '@import "'. $urlStart .'custom.css"'.
            ' screen and (orientation:landscape);'
        );
    }
    
    public static function
        proxifyLineYesWithOne(
            CssStringProxifierAbstractTest $testCase,
            CssStringProxifierAbstract $proxifier
        )
    {
        self::proxifyLineYesWithOneSeparator($testCase, $proxifier);
        self::proxifyLineYesWithOnePath($testCase, $proxifier);
        self::proxifyLineYesWithOneMedia($testCase, $proxifier);
    }
    
    public static function
        proxifyLineYesWithTwo(
            CssStringProxifierAbstractTest $testCase,
            CssStringProxifierAbstract $proxifier
        )
    {
        $urlStart = (
            $testCase->getUrl() .'?'.
            ProxifierGetParametersUtils::DEFAULT_KEY_TO_PROXIFY .'='.
            $testCase->getUrlProxifiedEncoded()
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine(
                '@import \'custom1.css\'; @import \'custom2.css\';'
            ),
            '@import \''. $urlStart.'custom1.css'
            .'\'; @import \''. $urlStart .'custom2.css'
            .'\';'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine(
                '@import "custom1.css"; @import "custom2.css";'
            ),
            '@import "'. $urlStart .'custom1.css'
            .'"; @import "'. $urlStart .'custom2.css'
            .'";'
        );
    }
    
    public static function
        proxifyLineYes(
            CssStringProxifierAbstractTest $testCase,
            CssStringProxifierAbstract $proxifier
        )
    {
        self::proxifyLineYesWithOne($testCase, $proxifier);
        self::proxifyLineYesWithTwo($testCase, $proxifier);
    }
    
    
    public function
        __construct()
    {
        parent::__construct(new CssStringProxifierImport());
    }
    
    
    public function
        testProxifyLineYes()
    {
        self::proxifyLineYes($this, $this->getProxifier());
    }
}
