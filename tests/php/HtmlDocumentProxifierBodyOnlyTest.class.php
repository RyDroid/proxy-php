<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HtmlDocumentProxifierBodyOnlyTest
    extends UrlProxifierGenericTest
{
    public static function
        proxifyOneDocumentWithEntity(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier,
            string $html
        )
    {
        $testCase->assertEquals(
            trim($proxifier->proxifyStringWithoutDeclaration($html)),
            $html
        );
    }
    
    public static function
        proxifyMultipleDocumentWithEntity(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier,
            array $htmlStrings
        )
    {
        foreach($htmlStrings as $htmlString)
        {
            self::proxifyOneDocumentWithEntity(
                $testCase, $proxifier, $htmlString
            );
        }
    }
    
    public static function
        proxifyDocumentWithEntity(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        self::proxifyMultipleDocumentWithEntity(
            $testCase, $proxifier,
            DomStringPreparatorEntityTest::HTML_STRINGS_NO_EMPTY_TAG
        );
    }
    
    public static function
        proxifyDocumentWithEmptyAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        HtmlDocumentProxifierLinkTest::proxifyDocumentWithEmptyAttribute(
            $testCase, $proxifier
        );
        
        HtmlDocumentProxifierMediaTest::proxifyDocumentWithEmptyAttribute(
            $testCase, $proxifier
        );
        
        HtmlDocumentProxifierFormTest::proxifyDocumentWithEmptyAttribute(
            $testCase, $proxifier
        );
        
        HtmlDocumentProxifierScriptTest::proxifyDocumentWithEmptyAttribute(
            $testCase, $proxifier
        );
        
        HtmlDocumentProxifierFrameTest::proxifyDocumentWithEmptyAttribute(
            $testCase, $proxifier
        );
        
        HtmlDocumentProxifierBackgroundTest::proxifyDocumentWithEmptyAttribute(
            $testCase, $proxifier
        );
    }
    
    public static function
        proxifyDocumentWithFilledAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        HtmlDocumentProxifierLinkTest::proxifyDocumentWithFilledAttribute(
            $testCase, $proxifier
        );
        
        HtmlDocumentProxifierMediaTest::proxifyDocumentWithFilledAttribute(
            $testCase, $proxifier
        );
        
        HtmlDocumentProxifierFormTest::proxifyDocumentWithFilledAttribute(
            $testCase, $proxifier
        );
        
        HtmlDocumentProxifierScriptTest::proxifyDocumentWithFilledAttribute(
            $testCase, $proxifier, false
        );
        
        HtmlDocumentProxifierFrameTest::proxifyDocumentWithFilledAttribute(
            $testCase, $proxifier
        );
        
        HtmlDocumentProxifierBackgroundTest::proxifyDocumentWithFilledAttribute(
            $testCase, $proxifier
        );
    }
    
    
    public function
        __construct()
    {
        parent::__construct(new HtmlDocumentProxifierBodyOnly());
    }
    
    
    public function
        testProxifyDocumentWithEntity()
    {
        self::proxifyDocumentWithEntity(
            $this, $this->getProxifier()
        );
    }
    
    public function
        testProxifyDocumentWithEmptyAttribute()
    {
        self::proxifyDocumentWithEmptyAttribute(
            $this, $this->getProxifier()
        );
    }
    
    public function
        testProxifyDocumentWithFilledAttribute()
    {
        self::proxifyDocumentWithFilledAttribute(
            $this, $this->getProxifier()
        );
    }
}
