<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HttpHeadersContentTypeStringUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testIsCssFalse()
    {
        $this->assertFalse(
            HttpHeadersContentTypeStringUtils::isCss('')
        );
        $this->assertFalse(
            HttpHeadersContentTypeStringUtils::isCss(' ')
        );
        $this->assertFalse(
            HttpHeadersContentTypeStringUtils::isCss(PHP_EOL)
        );
        $this->assertFalse(
            HttpHeadersContentTypeStringUtils::isCss(
                'Date: Fri, 19 May 2017 13:26:33 GMT'. PHP_EOL .
                'Server: Apache/2.4.25 (Debian)'. PHP_EOL .
                'Last-Modified: Mon, 10 Apr 2017 09:17:18 GMT'. PHP_EOL .
                'ETag: "22411-1f58b-54ccc71182380-gzip"'. PHP_EOL .
                'Accept-Ranges: bytes'. PHP_EOL .
                'Keep-Alive: timeout=5, max=100'. PHP_EOL .
                'Connection: Keep-Alive, Keep-Alive'. PHP_EOL .
                'Vary: Accept-Encoding'. PHP_EOL .
                'Content-Encoding: gzip'. PHP_EOL .
                'Content-Length: 22056'. PHP_EOL
            )
        );
    }
    
    public function
        testIsCssTrue()
    {
        $this->assertTrue(
            HttpHeadersContentTypeStringUtils::isCss(
                'Content-Type: text/css'
            )
        );
        $this->assertTrue(
            HttpHeadersContentTypeStringUtils::isCss(
                PHP_EOL .'Content-Type: text/css'
            )
        );
        $this->assertTrue(
            HttpHeadersContentTypeStringUtils::isCss(
                'Content-Type: text/css'. PHP_EOL
            )
        );
        $this->assertTrue(
            HttpHeadersContentTypeStringUtils::isCss(
                PHP_EOL .'Content-Type: text/css'. PHP_EOL
            )
        );
        $this->assertTrue(
            HttpHeadersContentTypeStringUtils::isCss(
                PHP_EOL .'Content-Type: text/css;charset=UTF-8'. PHP_EOL
            )
        );
        $this->assertTrue(
            HttpHeadersContentTypeStringUtils::isCss(
                'Date: Fri, 19 May 2017 13:26:33 GMT'. PHP_EOL .
                'Server: Apache/2.4.25 (Debian)'. PHP_EOL .
                'Last-Modified: Mon, 10 Apr 2017 09:17:18 GMT'. PHP_EOL .
                'ETag: "22411-1f58b-54ccc71182380-gzip"'. PHP_EOL .
                'Accept-Ranges: bytes'. PHP_EOL .
                'Keep-Alive: timeout=5, max=100'. PHP_EOL .
                'Connection: Keep-Alive, Keep-Alive'. PHP_EOL .
                'Vary: Accept-Encoding'. PHP_EOL .
                'Content-Encoding: gzip'. PHP_EOL .
                'Content-Length: 22056'. PHP_EOL .
                'Content-Type: text/css;charset=UTF-8'. PHP_EOL
            )
        );
    }
}
