<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HtmlDocumentProxifierBackgroundInvalidTest
    extends PHPUnit_Framework_TestCase
{
    public static function
        proxifyDocumentWithEmptyAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        DomDocumentProxifierAttributeTest::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html></html>'
        );
        DomDocumentProxifierAttributeTest::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body></body></html>'
        );
        DomDocumentProxifierAttributeTest::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body background=""></body></html>'
        );
        DomDocumentProxifierAttributeTest::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><div background=""></div></body></html>'
        );
    }
    
    // Alias against long lines
    public static function
        proxifyDocWithEmptyAttr(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        self::proxifyDocumentWithEmptyAttribute($testCase, $proxifier);
    }
    
    public static function
        proxifyDocumentWithFilledAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        DomDocumentProxifierAttributeTest::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><div background="img.png"></div></body></html>',
            '/html/body//*[string(@background)]',
            'background'
        );
        DomDocumentProxifierAttributeTest::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><article>'.
            '<p background="img.png"></p>'
            .'</article></body></html>',
            '/html/body//*[string(@background)]',
            'background'
        );
        DomDocumentProxifierAttributeTest::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><div background="img.jpg"></div></body></html>',
            '/html/body//*[string(@background)]',
            'background'
        );
        DomDocumentProxifierAttributeTest::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><div background="img.jpeg"></div></body></html>',
            '/html/body//*[string(@background)]',
            'background'
        );
        DomDocumentProxifierAttributeTest::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><div background="dir/img.png"></div></body></html>',
            '/html/body//*[string(@background)]',
            'background'
        );
        DomDocumentProxifierAttributeTest::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><div background="/dir/img.png"></div></body></html>',
            '/html/body//*[string(@background)]',
            'background'
        );
        DomDocumentProxifierAttributeTest::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><div background="//dir/img.png"></div></body></html>',
            '/html/body//*[string(@background)]',
            'background'
        );
    }
    
    // Alias against long lines
    public static function
        proxifyDocWithFilledAttr(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        self::proxifyDocumentWithFilledAttribute($testCase, $proxifier);
    }
    
    
    private $proxifier;
    
    
    public function
        __construct()
    {
        $this->proxifier = new HtmlDocumentProxifierBackgroundInvalid();
    }
    
    
    public function
        testProxifyDocumentWithEmptyAttribute()
    {
        self::proxifyDocumentWithEmptyAttribute(
            $this, $this->proxifier
        );
    }
    
    public function
        testProxifyDocumentWithFilledAttribute()
    {
        self::proxifyDocumentWithFilledAttribute(
            $this, $this->proxifier
        );
    }
}
