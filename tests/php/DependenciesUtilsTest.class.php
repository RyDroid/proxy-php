<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class DependenciesUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testMessagesArrayToHtmlListItems()
    {
        $this->assertEmpty(
            DependenciesUtils::messagesArrayToHtmlListItems(array())
        );
        $this->assertEquals(
            DependenciesUtils::messagesArrayToHtmlListItems(
                array('A message')
            ),
            '<li>A message</li>'
        );
        $this->assertEquals(
            DependenciesUtils::messagesArrayToHtmlListItems(
                array('msg1', 'msg2')
            ),
            '<li>msg1</li><li>msg2</li>'
        );
    }
    
    public function
        testMessagesArrayToHtmlUnorderedList()
    {
        $this->assertEmpty(
            DependenciesUtils::messagesArrayToHtmlUnorderedList(array())
        );
        $this->assertEquals(
            DependenciesUtils::messagesArrayToHtmlUnorderedList(
                array('A message')
            ),
            '<ul><li>A message</li></ul>'
        );
        $this->assertEquals(
            DependenciesUtils::messagesArrayToHtmlUnorderedList(
                array('msg1', 'msg2')
            ),
            '<ul><li>msg1</li><li>msg2</li></ul>'
        );
    }
    
    public function
        testMessagesArrayToHtmlOrderedList()
    {
        $this->assertEmpty(
            DependenciesUtils::messagesArrayToHtmlOrderedList(array())
        );
        $this->assertEquals(
            DependenciesUtils::messagesArrayToHtmlOrderedList(
                array('A message')
            ),
            '<ol><li>A message</li></ol>'
        );
        $this->assertEquals(
            DependenciesUtils::messagesArrayToHtmlOrderedList(
                array('msg1', 'msg2')
            ),
            '<ol><li>msg1</li><li>msg2</li></ol>'
        );
    }
    
    
    public function
        testGetErrorsAsArrayMessages()
    {
        $this->assertEmpty(DependenciesUtils::getErrorsAsArrayMessages());
    }
    
    public function
        testGetErrorsAsHtmlUnorderedList()
    {
        $this->assertEmpty(DependenciesUtils::getErrorsAsHtmlUnorderedList());
    }
    
    public function
        testGetErrorsAsHtmlOrderedList()
    {
        $this->assertEmpty(DependenciesUtils::getErrorsAsHtmlOrderedList());
    }
    
    public function
        testGetWarningsAsArrayMessages()
    {
        $this->assertEmpty(DependenciesUtils::getWarningsAsArrayMessages());
    }
    
    public function
        testGetWarningsAsHtmlUnorderedList()
    {
        $this->assertEmpty(DependenciesUtils::getWarningsAsHtmlUnorderedList());
    }
    
    public function
        testGetWarningsAsHtmlOrderedList()
    {
        $this->assertEmpty(DependenciesUtils::getWarningsAsHtmlOrderedList());
    }
}
