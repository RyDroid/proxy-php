<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');
require_once('UrlProxifierGenericTest.class.php');
require_once('StringProxifierTestGeneric.class.php');


final class HtmlDocumentProxifierStyleAttributeTest
    extends UrlProxifierGenericTest
{
    public static function
        proxifyStringNothing(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier)
    {
        StringProxifierTestGeneric::proxifyHtmlStringNothingOne(
            $testCase, $proxifier, ''
        );
        
        StringProxifierTestGeneric::proxifyHtmlStringNothingOne(
            $testCase, $proxifier,
            '<html><head><title>Test</title></head><body>Hi</body></html>'
        );
        
        StringProxifierTestGeneric::proxifyHtmlStringNothingOne(
            $testCase, $proxifier,
            '<html><head><style></style></head><body></body></html>'
        );
        
        StringProxifierTestGeneric::proxifyHtmlStringNothingOne(
            $testCase, $proxifier,
            '<html><head><style type="text/css">/**/</style></head></html>'
        );
    }
    
    public static function
        proxifyStringSuccessOne(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier,
            string $htmlBefore,
            string $htmlAfter
        )
    {
        $result = $proxifier->proxifyHtmlStringWithoutDeclaration($htmlBefore);
        $result = StringProxifierTestGeneric::cleanString($result);
        $testCase->assertEquals($htmlAfter, $result);
    }
    
    public static function
        proxifyStringSuccess(
            UrlProxifierGenericTest $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        $urlStart = (
            $testCase->getUrl() .'?'.
            ProxifierGetParametersUtils::DEFAULT_KEY_TO_PROXIFY .'='.
            $testCase->getUrlProxifiedEncoded()
        );
        
        $htmlStart  = '<html><body><div style="';
        $htmlEnd    = '"></div></body></html>';
        $cssBefore  = 'background:url(back.png);';
        $cssAfter   = 'background:url('. $urlStart .'back.png);';
        $htmlBefore = $htmlStart . $cssBefore . $htmlEnd;
        $htmlAfter  = $htmlStart . $cssAfter  . $htmlEnd;
        self::proxifyStringSuccessOne(
            $testCase, $proxifier, $htmlBefore, $htmlAfter
        );
        
        $htmlStart  = '<html><body style="';
        $htmlEnd    = '"></body></html>';
        $cssBefore  = 'background:url(back.png);';
        $cssAfter   = 'background:url('. $urlStart .'back.png);';
        $htmlBefore = $htmlStart . $cssBefore . $htmlEnd;
        $htmlAfter  = $htmlStart . $cssAfter  . $htmlEnd;
        self::proxifyStringSuccessOne(
            $testCase, $proxifier, $htmlBefore, $htmlAfter
        );
    }
    
    public static function
        proxifyString(
            UrlProxifierGenericTest $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        self::proxifyStringNothing($testCase, $proxifier);
        self::proxifyStringSuccess($testCase, $proxifier);
    }
    
    
    public function
        __construct()
    {
        parent::__construct(new HtmlDocumentProxifierStyleAttribute());
    }
    
    
    public function
        testProxifyStringNothing()
    {
        self::proxifyStringNothing($this, $this->getProxifier());
        
        StringProxifierTestGeneric::proxifyHtmlStringNothingOne(
            $this, $this->getProxifier(),
            '<html><head><style type="text/css">'.
            'background:url(img.png);'
            .'</style></head><body></body></html>'
        );
    }
    
    public function
        testProxifyStringSuccess()
    {
        self::proxifyStringSuccess($this, $this->getProxifier());
    }
}
