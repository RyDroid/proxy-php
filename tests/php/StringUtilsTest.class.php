<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class StringUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testStartsWithFalse()
    {
        $this->assertFalse(StringUtils::startsWith('a',   'b'));
        $this->assertFalse(StringUtils::startsWith('ab',  'b'));
        $this->assertFalse(StringUtils::startsWith('aba', 'b'));
        $this->assertFalse(StringUtils::startsWith('abb', 'b'));
        $this->assertFalse(StringUtils::startsWith('abb', 'ba'));
        $this->assertFalse(StringUtils::startsWith('abb', 'bba'));
        $this->assertFalse(StringUtils::startsWith('aaa', 'A'));
        $this->assertFalse(StringUtils::startsWith('abb', 'A'));
    }
    
    public function
        testStartsWithTrue()
    {
        $this->assertTrue(StringUtils::startsWith('a',   'a'));
        $this->assertTrue(StringUtils::startsWith('aa',  'a'));
        $this->assertTrue(StringUtils::startsWith('aaa', 'a'));
        $this->assertTrue(StringUtils::startsWith('aaa', 'aa'));
        $this->assertTrue(StringUtils::startsWith('aaa', 'aaa'));
        $this->assertTrue(StringUtils::startsWith('abc', 'a'));
        $this->assertTrue(StringUtils::startsWith('abc', 'ab'));
        $this->assertTrue(StringUtils::startsWith('abc', 'abc'));
    }
    
    public function
        testStartsWithInsensitiveFalse()
    {
        $this->assertFalse(StringUtils::startsWithInsensitive('a',   'b'));
        $this->assertFalse(StringUtils::startsWithInsensitive('ab',  'b'));
        $this->assertFalse(StringUtils::startsWithInsensitive('aba', 'b'));
        $this->assertFalse(StringUtils::startsWithInsensitive('abb', 'b'));
        $this->assertFalse(StringUtils::startsWithInsensitive('abb', 'ba'));
        $this->assertFalse(StringUtils::startsWithInsensitive('abb', 'bba'));
    }
    
    public function
        testStartsWithInsensitiveTrue()
    {
        $this->assertTrue(StringUtils::startsWithInsensitive('a',   'a'));
        $this->assertTrue(StringUtils::startsWithInsensitive('aa',  'a'));
        $this->assertTrue(StringUtils::startsWithInsensitive('aaa', 'a'));
        $this->assertTrue(StringUtils::startsWithInsensitive('aaa', 'aa'));
        $this->assertTrue(StringUtils::startsWithInsensitive('aaa', 'aaa'));
        $this->assertTrue(StringUtils::startsWithInsensitive('abc', 'a'));
        $this->assertTrue(StringUtils::startsWithInsensitive('abc', 'ab'));
        $this->assertTrue(StringUtils::startsWithInsensitive('abc', 'abc'));
        $this->assertTrue(StringUtils::startsWithInsensitive('aaa', 'A'));
        $this->assertTrue(StringUtils::startsWithInsensitive('abb', 'A'));
        $this->assertTrue(StringUtils::startsWithInsensitive('abb', 'Ab'));
        $this->assertTrue(StringUtils::startsWithInsensitive('abb', 'AbB'));
        $this->assertTrue(StringUtils::startsWithInsensitive('abb', 'ABb'));
        $this->assertTrue(StringUtils::startsWithInsensitive('abb', 'aBB'));
    }
    
    public function
        testEndsWithFalse()
    {
        $this->assertFalse(StringUtils::endsWith('abc',   'a'));
        $this->assertFalse(StringUtils::endsWith('abc',   'b'));
        $this->assertFalse(StringUtils::endsWith('abc',   'ab'));
        $this->assertFalse(StringUtils::endsWith('abc',   'ba'));
        $this->assertFalse(StringUtils::endsWith('a abc', 'a'));
        $this->assertFalse(StringUtils::endsWith('a abc', ' '));
    }
    
    public function
        testEndsWithTrue()
    {
        $this->assertTrue(StringUtils::endsWith('abc',   'c'));
        $this->assertTrue(StringUtils::endsWith('abc',   'bc'));
        $this->assertTrue(StringUtils::endsWith('abc',   'abc'));
        $this->assertTrue(StringUtils::endsWith(' abc',  'c'));
        $this->assertTrue(StringUtils::endsWith(' abc',  'bc'));
        $this->assertTrue(StringUtils::endsWith(' abc',  'abc'));
        $this->assertTrue(StringUtils::endsWith('z abc', 'abc'));
        $this->assertTrue(StringUtils::endsWith('z abc', ' abc'));
        $this->assertTrue(StringUtils::endsWith('z abc', 'z abc'));
    }
    
    public function
        testIsHexadecimalFalse()
    {
        $this->assertFalse(StringUtils::isHexadecimal('false'));
        $this->assertFalse(StringUtils::isHexadecimal('It is false'));
        $this->assertFalse(StringUtils::isHexadecimal('It is not true'));
    }
    
    public function
        testIsHexadecimalTrue()
    {
        $this->assertTrue(StringUtils::isHexadecimal('0'));
        $this->assertTrue(StringUtils::isHexadecimal('1'));
        $this->assertTrue(StringUtils::isHexadecimal('2'));
        $this->assertTrue(StringUtils::isHexadecimal('3'));
        $this->assertTrue(StringUtils::isHexadecimal('4'));
        $this->assertTrue(StringUtils::isHexadecimal('5'));
        $this->assertTrue(StringUtils::isHexadecimal('6'));
        $this->assertTrue(StringUtils::isHexadecimal('7'));
        $this->assertTrue(StringUtils::isHexadecimal('8'));
        $this->assertTrue(StringUtils::isHexadecimal('9'));
        $this->assertTrue(StringUtils::isHexadecimal('a'));
        $this->assertTrue(StringUtils::isHexadecimal('A'));
        $this->assertTrue(StringUtils::isHexadecimal('b'));
        $this->assertTrue(StringUtils::isHexadecimal('B'));
        $this->assertTrue(StringUtils::isHexadecimal('c'));
        $this->assertTrue(StringUtils::isHexadecimal('C'));
        $this->assertTrue(StringUtils::isHexadecimal('d'));
        $this->assertTrue(StringUtils::isHexadecimal('D'));
        $this->assertTrue(StringUtils::isHexadecimal('e'));
        $this->assertTrue(StringUtils::isHexadecimal('E'));
        $this->assertTrue(StringUtils::isHexadecimal('f'));
        $this->assertTrue(StringUtils::isHexadecimal('F'));
        $this->assertTrue(StringUtils::isHexadecimal('efefef'));
        $this->assertTrue(StringUtils::isHexadecimal('ffffff'));
        $this->assertTrue(StringUtils::isHexadecimal('FFFFFF'));
    }
}
