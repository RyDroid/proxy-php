<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HtmlDocumentProxifierAudioTest
    extends PHPUnit_Framework_TestCase
{
    public static function
        proxifyDocumentWithEmptyAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        DomDocumentProxifierAttributeTest::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><audio src=""></audio></body></html>'
        );
    }
    
    public static function
        proxifyDocumentWithFilledAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        DomDocumentProxifierAttributeTest::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><audio src="song.oga"></audio></body></html>',
            '/html/body//audio[string(@src)]',
            'src'
        );
        
        DomDocumentProxifierAttributeTest::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><audio src="song.ogg"></audio></body></html>',
            '/html/body//audio[string(@src)]',
            'src'
        );
        
        DomDocumentProxifierAttributeTest::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><audio src="song.webma"></audio></body></html>',
            '/html/body//audio[string(@src)]',
            'src'
        );
        
        DomDocumentProxifierAttributeTest::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><audio src="song.opus"></audio></body></html>',
            '/html/body//audio[string(@src)]',
            'src'
        );
        
        DomDocumentProxifierAttributeTest::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><audio src="song.flac"></audio></body></html>',
            '/html/body//audio[string(@src)]',
            'src'
        );
        
        DomDocumentProxifierAttributeTest::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><audio src="song.mp3"></audio></body></html>',
            '/html/body//audio[string(@src)]',
            'src'
        );
        
        DomDocumentProxifierAttributeTest::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><audio src="song.m4a"></audio></body></html>',
            '/html/body//audio[string(@src)]',
            'src'
        );
    }
    
    
    private $proxifier;
    
    
    public function
        __construct()
    {
        $this->proxifier = new HtmlDocumentProxifierAudio();
    }
    
    
    public function
        testProxifyDocumentWithEmptyAttribute()
    {
        self::proxifyDocumentWithEmptyAttribute(
            $this, $this->proxifier
        );
    }
    
    public function
        testProxifyDocumentWithFilledAttribute()
    {
        self::proxifyDocumentWithFilledAttribute(
            $this, $this->proxifier
        );
    }
}
