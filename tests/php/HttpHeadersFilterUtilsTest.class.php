<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HttpHeadersFilterUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testRemoveCookie()
    {
        $headers = HttpHeaders::createFromString('');
        HttpHeadersFilterUtils::removeCookie($headers);
        $this->assertEquals($headers->getNumberOfLines(), 0);
        
        $headers = HttpHeaders::createFromString(
            'Cookie: '.
            'session-save=true; proxy-port=8080; session-save-proxy=false'
        );
        $this->assertEquals($headers->getNumberOfLines(), 1);
        HttpHeadersFilterUtils::removeCookie($headers);
        $this->assertEquals($headers->getNumberOfLines(), 0);
    }
}
