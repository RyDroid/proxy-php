<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class ProxyConfigurationTest
    extends PHPUnit_Framework_TestCase
{
    public function
        hasNotUrl(ProxyConfigurationAbstract $conf)
    {
        $this->assertFalse($conf->hasUrl());
    }
    
    public function
        hasUrl(ProxyConfigurationAbstract $conf)
    {
        $this->assertTrue($conf->hasUrl());
    }
    
    public function
        testHasUrl()
    {
        $this->hasNotUrl(new ProxyConfiguration('', -1));
        $this->hasNotUrl(new ProxyConfiguration('', 80));
        
        $this->hasUrl(new ProxyConfiguration('https://proxy', 80));
        $this->hasUrl(new ProxyConfiguration('https://proxy', 8080));
        $this->hasUrl(new ProxyConfiguration('https://proxy', -1));
    }
    
    public function
        hasNotPort(ProxyConfigurationAbstract $conf)
    {
        $this->assertFalse($conf->hasPort());
    }
    
    public function
        hasPort(ProxyConfigurationAbstract $conf)
    {
        $this->assertTrue($conf->hasPort());
    }
    
    public function
        testHasPort()
    {
        $this->hasNotPort(new ProxyConfiguration('', -1));
        $this->hasNotPort(new ProxyConfiguration('https://proxy', -1));
        
        $this->hasPort(new ProxyConfiguration('', 80));
        $this->hasPort(new ProxyConfiguration('https://proxy', 80));
        $this->hasPort(new ProxyConfiguration('', 8080));
    }
}
