<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class CssStringProxifierUrlTestGeneric
{
    public static function
        proxifyStringWithUrlArgument(
            CssStringProxifierAbstractTest $testCase,
            CssStringProxifierAbstract $proxifier
        )
    {
        $urlStart = (
            $testCase->getUrl() .'?'.
            ProxifierGetParametersUtils::DEFAULT_KEY_TO_PROXIFY .'='.
            $testCase->getUrlProxifiedEncoded()
        );
        $testCase->assertEquals(
            $proxifier->proxifyStringWithUrlArgument('img.png'),
            $urlStart .'img.png'
        );
        $testCase->assertEquals(
            $proxifier->proxifyStringWithUrlArgument('"img.png"'),
            '"'. $urlStart .'img.png"'
        );
        $testCase->assertEquals(
            $proxifier->proxifyStringWithUrlArgument(' "img.png"'),
            ' "'. $urlStart .'img.png"'
        );
        $testCase->assertEquals(
            $proxifier->proxifyStringWithUrlArgument('"img.png" '),
            '"'. $urlStart .'img.png" '
        );
        $testCase->assertEquals(
            $proxifier->proxifyStringWithUrlArgument(' "img.png" '),
            ' "'. $urlStart .'img.png" '
        );
    }
    
    
    private static function
        proxifyLineYesEasyNoSeparator(
            CssStringProxifierAbstractTest $testCase,
            CssStringProxifierAbstract $proxifier
        )
    {
        $urlStart = (
            $testCase->getUrl() .'?'.
            ProxifierGetParametersUtils::DEFAULT_KEY_TO_PROXIFY .'='.
            $testCase->getUrlProxifiedEncoded()
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('background-image:url(img.png);'),
            'background-image:url('. $urlStart .'img.png);'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('background:url(img.png);'),
            'background:url('. $urlStart .'img.png);'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('background: url(img.png);'),
            'background: url('. $urlStart .'img.png);'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('background : url(img.png);'),
            'background : url('. $urlStart .'img.png);'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('background : url(img.png); '),
            'background : url('. $urlStart .'img.png); '
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('background :	url(img.png);'),
            'background :	url('. $urlStart .'img.png);'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('@import url(style.css);'),
            '@import url('. $urlStart .'style.css);'
        );
    }
    
    private static function
        proxifyLineYesEasySeparator(
            CssStringProxifierAbstractTest $testCase,
            CssStringProxifierAbstract $proxifier
        )
    {
        $urlStart = (
            $testCase->getUrl() .'?'.
            ProxifierGetParametersUtils::DEFAULT_KEY_TO_PROXIFY .'='.
            $testCase->getUrlProxifiedEncoded()
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('background:url(\'img.png\');'),
            'background:url(\''. $urlStart .'img.png\');'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('background:url("img.png");'),
            'background:url("'. $urlStart. 'img.png");'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('background:url( "img.png");'),
            'background:url( "'. $urlStart .'img.png");'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('background:url("img.png" );'),
            'background:url("'. $urlStart .'img.png" );'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('background:url( "img.png" );'),
            'background:url( "'. $urlStart .'img.png" );'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('@import url("style.css");'),
            '@import url("'. $urlStart .'style.css");'
        );
    }
    
    public static function
        proxifyLineYesEasy(
            CssStringProxifierAbstractTest $testCase,
            CssStringProxifierAbstract $proxifier
        )
    {
        self::proxifyLineYesEasyNoSeparator($testCase, $proxifier);
        self::proxifyLineYesEasySeparator($testCase, $proxifier);
    }
    
    public static function
        proxifyLineYesPath(
            CssStringProxifierAbstractTest $testCase,
            CssStringProxifierAbstract $proxifier
        )
    {
        $urlStart = (
            $testCase->getUrl() .'?'.
            ProxifierGetParametersUtils::DEFAULT_KEY_TO_PROXIFY .'='.
            $testCase->getUrlProxifiedEncoded()
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('background:url(dir/img.png);'),
            'background:url('. $urlStart .
            UrlProxifierCodecUtils::getEncodedUrl('dir/img.png')
            .');'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('background:url(./dir/img.png);'),
            'background:url('. $urlStart .
            UrlProxifierCodecUtils::getEncodedUrl('dir/img.png')
            .');'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('background:url(../dir/img.png);'),
            'background:url('. $urlStart .
            UrlProxifierCodecUtils::getEncodedUrl('../dir/img.png')
            .');'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('background:url(//server.net/img.png);'),
            'background:url('.
            $testCase->getUrl() .'?'.
            ProxifierGetParametersUtils::DEFAULT_KEY_TO_PROXIFY .'='.
            UrlProxifierCodecUtils::getEncodedUrl('//server.net/img.png')
            .');'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('@import url(../style.css);'),
            '@import url('. $urlStart .
            UrlProxifierCodecUtils::getEncodedUrl('../style.css')
            .');'
        );
    }
    
    private static function
        proxifyLineYesEnd(
            CssStringProxifierAbstractTest $testCase,
            CssStringProxifierAbstract $proxifier
        )
    {
        $urlStart = (
            $testCase->getUrl() .'?'.
            ProxifierGetParametersUtils::DEFAULT_KEY_TO_PROXIFY .'='.
            $testCase->getUrlProxifiedEncoded()
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine(
                'list-style-image:url("fr.png");'
            ),
            'list-style-image:url("'. $urlStart .'fr.png");'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine(
                'list-style-image:url("fr.png")}'
            ),
            'list-style-image:url("'. $urlStart .'fr.png")}'
        );
    }
    
    private static function
        proxifyLineYesFont(
            CssStringProxifierAbstractTest $testCase,
            CssStringProxifierAbstract $proxifier
        )
    {
        $urlStart = (
            $testCase->getUrl() .'?'.
            ProxifierGetParametersUtils::DEFAULT_KEY_TO_PROXIFY .'='.
            $testCase->getUrlProxifiedEncoded()
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine(
                '@font-face {'.
                'font-family: aFont; '.
                'src: url(a_font.woff);}'
            ),
            '@font-face {font-family: aFont; src: url('.
            $urlStart . UrlProxifierCodecUtils::getEncodedUrl('a_font.woff')
            .');}'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine(
                '@font-face {'.
                '  font-family: MyHelvetica;'.
                '  src: local("HelveticaNeue-Bold"),'.
                '       url(MgOpenModernaBold.ttf);'.
                '  font-weight: bold;'.
                '}'
            ),
            '@font-face {'.
            '  font-family: MyHelvetica;'.
            '  src: local("HelveticaNeue-Bold"),'.
            '       url('. $urlStart .'MgOpenModernaBold.ttf);'.
            '  font-weight: bold;'.
            '}'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine(
                '@font-face {'.
                ' font-family: FontAwesome; src:'.
                ' url(font.eot) format(\'embedded-opentype\'),'.
                ' url(font.woff) format(\'woff\');'.
                '}'
            ),
            '@font-face {'.
            ' font-family: FontAwesome; src:'.
            ' url('. $urlStart .'font.eot) format(\'embedded-opentype\'),'.
            ' url('. $urlStart .'font.woff) format(\'woff\');'.
            '}'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine(
                '@font-face {'.
                ' font-family: FontAwesome; src:'.
                ' url(font.eot) format(\'embedded-opentype\'),'.
                ' url(font.woff2) format(\'woff2\'),'.
                ' url(font.woff) format(\'woff\');'.
                '}'
            ),
            '@font-face {'.
            ' font-family: FontAwesome; src:'.
            ' url('. $urlStart .'font.eot) format(\'embedded-opentype\'),'.
            ' url('. $urlStart .'font.woff2) format(\'woff2\'),'.
            ' url('. $urlStart .'font.woff) format(\'woff\');'.
            '}'
        );
    }
    
    private static function
        proxifyLineYesHexadecimalColor(
            CssStringProxifierAbstractTest $testCase,
            CssStringProxifierAbstract $proxifier
        )
    {
        $urlStart = (
            $testCase->getUrl() .'?'.
            ProxifierGetParametersUtils::DEFAULT_KEY_TO_PROXIFY .'='.
            $testCase->getUrlProxifiedEncoded()
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine(
                'background: #ffffff url(img.jpg) no-repeat 0px 0px;'
            ),
            'background: #ffffff url('. $urlStart .
            UrlProxifierCodecUtils::getEncodedUrl('img.jpg')
            .') no-repeat 0px 0px;'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine(
                'background: #fff url(img.jpg) no-repeat 0px 0px;'
            ),
            'background: #fff url('. $urlStart .
            UrlProxifierCodecUtils::getEncodedUrl('img.jpg')
            .') no-repeat 0px 0px;'
        );
    }
    
    private static function
        proxifyLineYesHard(
            CssStringProxifierAbstractTest $testCase,
            CssStringProxifierAbstract $proxifier
        )
    {
        $urlStart = (
            $testCase->getUrl() .'?'.
            ProxifierGetParametersUtils::DEFAULT_KEY_TO_PROXIFY .'='.
            $testCase->getUrlProxifiedEncoded()
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine(
                'background: grey url(img.png) no-repeat 1px center;'
            ),
            'background: grey url('. $urlStart .'img.png'.
            ') no-repeat 1px center;'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine(
                'background-image:'.
                '-o-linear-gradient(transparent,transparent),'.
                'url(ico.png);'
            ),
            'background-image:'.
            '-o-linear-gradient(transparent,transparent),'.
            'url('. $urlStart .'ico.png);'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine('.logo{background-image:url(frwiki.png)}'),
            '.logo{background-image:url('. $urlStart .'frwiki.png)}'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine(
                'background:url(img.png);background:url(img.png);'
            ),
            'background:url('. $urlStart .'img.png);'.
            'background:url('. $urlStart .'img.png);'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine(
                'background : url(img.png); background : url(img.png);'
            ),
            'background : url('. $urlStart .'img.png); '.
            'background : url('. $urlStart .'img.png);'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine(
                '@import url(style1.css); @import url(style2.css);'
            ),
            '@import url('. $urlStart .
            UrlProxifierCodecUtils::getEncodedUrl('style1.css')
            .'); @import url('. $urlStart .
            UrlProxifierCodecUtils::getEncodedUrl('style2.css')
            .');'
        );
    }
    
    private static function
        proxifyLineYesUtf8(
            CssStringProxifierAbstractTest $testCase,
            CssStringProxifierAbstract $proxifier
        )
    {
        $urlStart = (
            $testCase->getUrl() .'?'.
            ProxifierGetParametersUtils::DEFAULT_KEY_TO_PROXIFY .'='.
            $testCase->getUrlProxifiedEncoded()
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine(
                '@import url(img.png);'
            ),
            '@import url('. $urlStart .'img.png);'
        );
        $testCase->assertEquals(
            $proxifier->proxifyLine(
                hex2bin('efbb') . '@import url(img.png);'
            ),
            hex2bin('efbb') . '@import url('. $urlStart .'img.png);'
        );
    }
    
    public static function
        proxifyLineYes(
            CssStringProxifierAbstractTest $testCase,
            CssStringProxifierAbstract $proxifier
        )
   {
        self::proxifyLineYesEasy($testCase, $proxifier);
        self::proxifyLineYesPath($testCase, $proxifier);
        self::proxifyLineYesEnd($testCase, $proxifier);
        self::proxifyLineYesFont($testCase, $proxifier);
        self::proxifyLineYesHexadecimalColor($testCase, $proxifier);
        self::proxifyLineYesHard($testCase, $proxifier);
        self::proxifyLineYesUtf8($testCase, $proxifier);
    }
}
