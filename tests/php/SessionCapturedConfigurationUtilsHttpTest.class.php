<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class SessionCapturedConfigurationUtilsHttpTest
    extends PHPUnit_Framework_TestCase
{
    public function
        createFromMapAndCheckSaveCookiesUndefined(array $aMap)
    {
        $conf = SessionCapturedConfigurationUtils::createFromMap($aMap);
        $this->assertTrue($conf->getHttp()->hasToSaveCookies()->isUndefined());
    }
    
    public function
        createFromMapAndCheckSaveCookiesTrue(array $aMap)
    {
        $conf = SessionCapturedConfigurationUtils::createFromMap($aMap);
        $this->assertTrue($conf->getHttp()->hasToSaveCookies()->isTrue());
    }
    
    public function
        createFromMapAndCheckSaveCookiesFalse(array $aMap)
    {
        $conf = SessionCapturedConfigurationUtils::createFromMap($aMap);
        $this->assertTrue($conf->getHttp()->hasToSaveCookies()->isFalse());
    }
    
    public function
        testCreateFromMapAndCheckSaveCookies()
    {
        $this->createFromMapAndCheckSaveCookiesUndefined(array());
        $this->createFromMapAndCheckSaveCookiesUndefined(
            array('save-cookies' => '')
        );
        
        $this->createFromMapAndCheckSaveCookiesTrue(
            array('save-cookies' => 'true')
        );
        $this->createFromMapAndCheckSaveCookiesTrue(
            array('save-cookies' => 'yes')
        );
        $this->createFromMapAndCheckSaveCookiesTrue(
            array('save-cookies' => 'on')
        );
        
        $this->createFromMapAndCheckSaveCookiesFalse(
            array('save-cookies' => 'false')
        );
        $this->createFromMapAndCheckSaveCookiesFalse(
            array('save-cookies' => 'no')
        );
        $this->createFromMapAndCheckSaveCookiesFalse(
            array('save-cookies' => 'off')
        );
        
        $this->createFromMapAndCheckSaveCookiesTrue(
            array('save-http-cookies' => 'on')
        );
        $this->createFromMapAndCheckSaveCookiesFalse(
            array('save-http-cookies' => 'off')
        );
    }
    
    public function
        testCreateFromMapAndCheckSaveUserAgent()
    {
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array()
        );
        $this->assertTrue(
            $conf->getHttp()->hasToSaveUserAgent()->isUndefined()
        );
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-user-agent' => '')
        );
        $this->assertTrue(
            $conf->getHttp()->hasToSaveUserAgent()->isUndefined()
        );
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-user-agent' => 'true')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveUserAgent()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-user-agent' => 'yes')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveUserAgent()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-user-agent' => 'on')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveUserAgent()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-user-agent' => 'false')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveUserAgent()->isFalse());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-user-agent' => 'no')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveUserAgent()->isFalse());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-user-agent' => 'off')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveUserAgent()->isFalse());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-user-agent' => 'on')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveUserAgent()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-user-agent' => 'off')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveUserAgent()->isFalse());
    }
    
    public function
        testCreateFromMapAndCheckSaveReferer()
    {
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array()
        );
        $this->assertTrue(
            $conf->getHttp()->hasToSaveReferer()->isUndefined()
        );
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-referer' => '')
        );
        $this->assertTrue(
            $conf->getHttp()->hasToSaveReferer()->isUndefined()
        );
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-referer' => 'true')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveReferer()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-referer' => 'yes')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveReferer()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-referer' => 'on')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveReferer()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-referer' => 'false')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveReferer()->isFalse());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-referer' => 'no')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveReferer()->isFalse());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-referer' => 'off')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveReferer()->isFalse());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-referer' => 'on')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveReferer()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-referer' => 'off')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveReferer()->isFalse());
    }
    
    public function
        testCreateFromMapAndCheckSaveEncoding()
    {
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array()
        );
        $this->assertTrue(
            $conf->getHttp()->hasToSaveEncoding()->isUndefined()
        );
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-encoding' => '')
        );
        $this->assertTrue(
            $conf->getHttp()->hasToSaveEncoding()->isUndefined()
        );
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-encoding' => '')
        );
        $this->assertTrue(
            $conf->getHttp()->hasToSaveEncoding()->isUndefined()
        );
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-user-agent' => '')
        );
        $this->assertTrue(
            $conf->getHttp()->hasToSaveEncoding()->isUndefined()
        );
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-etag' => '')
        );
        $this->assertTrue(
            $conf->getHttp()->hasToSaveEncoding()->isUndefined()
        );
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-encoding' => 'true')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveEncoding()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-encoding' => 'true')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveEncoding()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-encoding' => 'yes')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveEncoding()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-encoding' => 'on')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveEncoding()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-encoding' => 'false')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveEncoding()->isFalse());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-encoding' => 'no')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveEncoding()->isFalse());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-encoding' => 'off')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveEncoding()->isFalse());
    }
    
    public function
        testCreateFromMapAndCheckSaveLanguage()
    {
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array()
        );
        $this->assertTrue(
            $conf->getHttp()->hasToSaveLanguage()->isUndefined()
        );
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-language' => '')
        );
        $this->assertTrue(
            $conf->getHttp()->hasToSaveLanguage()->isUndefined()
        );
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-language' => '')
        );
        $this->assertTrue(
            $conf->getHttp()->hasToSaveLanguage()->isUndefined()
        );
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-language' => 'true')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveLanguage()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-language' => 'true')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveLanguage()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-language' => 'yes')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveLanguage()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-language' => 'on')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveLanguage()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-language' => 'false')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveLanguage()->isFalse());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-language' => 'no')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveLanguage()->isFalse());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-language' => 'off')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveLanguage()->isFalse());
    }
    
    public function
        testCreateFromMapAndCheckSaveEtag()
    {
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array()
        );
        $this->assertTrue(
            $conf->getHttp()->hasToSaveEtag()->isUndefined()
        );
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-etag' => '')
        );
        $this->assertTrue(
            $conf->getHttp()->hasToSaveEtag()->isUndefined()
        );
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-etag' => 'true')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveEtag()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-etag' => 'true')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveEtag()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-etag' => 'yes')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveEtag()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-etag' => 'on')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveEtag()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-etag' => 'false')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveEtag()->isFalse());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-etag' => 'no')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveEtag()->isFalse());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-etag' => 'off')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveEtag()->isFalse());
    }
    
    public function
        testCreateFromMapAndCheckSaveDoNotTrack()
    {
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array()
        );
        $this->assertTrue(
            $conf->getHttp()->hasToSaveDoNotTrack()->isUndefined()
        );
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-dnt' => '')
        );
        $this->assertTrue(
            $conf->getHttp()->hasToSaveDoNotTrack()->isUndefined()
        );
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-cookies' => 'true')
        );
        $this->assertTrue(
            $conf->getHttp()->hasToSaveDoNotTrack()->isUndefined()
        );
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-etag' => 'true')
        );
        $this->assertTrue(
            $conf->getHttp()->hasToSaveDoNotTrack()->isUndefined()
        );
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-dnt' => 'true')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveDoNotTrack()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-dnt' => 'yes')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveDoNotTrack()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-dnt' => 'on')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveDoNotTrack()->isTrue());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-dnt' => 'false')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveDoNotTrack()->isFalse());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-dnt' => 'no')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveDoNotTrack()->isFalse());
        
        $conf = SessionCapturedConfigurationUtils::createFromMap(
            array('save-http-dnt' => 'off')
        );
        $this->assertTrue($conf->getHttp()->hasToSaveDoNotTrack()->isFalse());
    }
}
