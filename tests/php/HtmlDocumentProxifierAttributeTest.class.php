<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');
require_once('HtmlDocumentProxifierAttributeUtils.class.php');


final class HtmlDocumentProxifierAttributeTest
    extends PHPUnit_Framework_TestCase
{
    const LINK_ATTRIBUTE = 'href';
    const LINK_QUERY     = '/html/body//a';
    private $linkProxifier;
    
    
    public function
        __construct()
    {
        $this->linkProxifier = new DomDocumentProxifierAttribute(
            self::LINK_ATTRIBUTE, [self::LINK_QUERY]
        );
    }
    
    
    public function
        testProxifyDocumentWithEmptyAttribute()
    {
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $this, $this->linkProxifier,
            '<html><body><a href="">Fake link</a></body></html>'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $this, $this->linkProxifier,
            '<html><body><p><a href="">Fake link</a></p></body></html>'
        );
    }
    
    public function
        testProxifyDocumentWithFilledAttribute()
    {
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $this, $this->linkProxifier,
            '<html><body>'.
            '<a href="https://stallman.org/">Stallman</a> is great!'
            .'</body></html>',
            self::LINK_QUERY, self::LINK_ATTRIBUTE
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $this, $this->linkProxifier,
            '<html><body>'.
            '<p><a href="https://stallman.org/">Stallman</a> is great!</p>'
            .'</body></html>',
            self::LINK_QUERY, self::LINK_ATTRIBUTE
        );
    }
}
