<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HtmlDocumentProxifierUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public static function
        proxifyStringNothingGenericOne(
            PHPUnit_Framework_TestCase $testCase,
            string $content
        )
    {
        $testCase->assertEquals(
            HtmlDocumentProxifierUtils::proxifyString($content), $content
        );
    }
    
    public static function
        proxifyStringNothingGeneric(PHPUnit_Framework_TestCase $testCase)
    {
        self::proxifyStringNothingGenericOne($testCase, '');
        self::proxifyStringNothingGenericOne($testCase, ' ');
        self::proxifyStringNothingGenericOne($testCase, "\t");
        self::proxifyStringNothingGenericOne($testCase, "\n");
        self::proxifyStringNothingGenericOne($testCase, PHP_EOL);
        self::proxifyStringNothingGenericOne(
            $testCase, PHP_EOL .' '. PHP_EOL
        );
        self::proxifyStringNothingGenericOne(
            $testCase, PHP_EOL ."\t". PHP_EOL
        );
        self::proxifyStringNothingGenericOne(
            $testCase, PHP_EOL .' '."\t".' '. PHP_EOL
        );
    }
    
    public static function
        proxifyStringSuccessOneGenericOne(
            PHPUnit_Framework_TestCase $testCase,
            string $content
        )
    {
        $testCase->assertNotEquals(
            HtmlDocumentProxifierUtils::proxifyString($content), $content
        );
    }
    
    public static function
        proxifyStringSuccessOneGeneric(PHPUnit_Framework_TestCase $testCase)
    {
        self::proxifyStringSuccessOneGenericOne(
            $testCase,
            '<html><body><img src="img.png" alt="" /></body></html>'
        );
        self::proxifyStringSuccessOneGenericOne(
            $testCase,
            '<html><body><img src="dir/img.png" alt="" /></body></html>'
        );
        self::proxifyStringSuccessOneGenericOne(
            $testCase,
            '<html><body><img src="/dir/img.png" alt="" /></body></html>'
        );
        self::proxifyStringSuccessOneGenericOne(
            $testCase,
            '<html><body><p><img src="img.png" alt="" /></p></body></html>'
        );
        self::proxifyStringSuccessOneGenericOne(
            $testCase,
            '<html><body><a href="page.html">Link</a></body></html>'
        );
        self::proxifyStringSuccessOneGenericOne(
            $testCase,
            '<html><body><a href="dir/page.html">Link</a></body></html>'
        );
        self::proxifyStringSuccessOneGenericOne(
            $testCase,
            '<html><body><a href="/dir/page.html">Link</a></body></html>'
        );
        self::proxifyStringSuccessOneGenericOne(
            $testCase,
            '<html><body><p><a href="page.html">Link</a></p></body></html>'
        );
    }
    
    
    public function
        testProxifyStringNothing()
    {
        self::proxifyStringNothingGeneric($this);
    }
    
    public function
        testProxifyStringSuccessOne()
    {
        self::proxifyStringSuccessOneGeneric($this);
    }
}
