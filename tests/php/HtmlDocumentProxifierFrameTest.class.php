<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HtmlDocumentProxifierFrameTest
    extends PHPUnit_Framework_TestCase
{
    public static function
        proxifyDocumentWithEmptyAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><frame src="" /></body></html>'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><frame src=" " /></body></html>'
        );
    }
    
    public static function
        proxifyDocumentWithFilledAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><frame src="main.html" /></body></html>',
            '/html/body//frame[string(@src)]',
            'src'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><frame src="main.php" /></body></html>',
            '/html/body//frame[string(@src)]',
            'src'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><frameset><frame src="main.html" /></frameset></html>',
            '/html/frameset/frame[string(@src)]',
            'src'
        );
    }
    
    
    private $proxifier;
    
    
    public function
        __construct()
    {
        $this->proxifier = new HtmlDocumentProxifierFrame();
    }
    
    
    public function
        testProxifyDocumentWithEmptyAttribute()
    {
        self::proxifyDocumentWithEmptyAttribute(
            $this, $this->proxifier
        );
    }
    
    public function
        testProxifyDocumentWithFilledAttribute()
    {
        self::proxifyDocumentWithFilledAttribute(
            $this, $this->proxifier
        );
    }
}
