<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HttpHeadersProxifierLocationTest
    extends PHPUnit_Framework_TestCase
{
    public static function
        proxifyHttpHeadersEmpty(
            PHPUnit_Framework_TestCase $testCase,
            HttpHeadersProxifierAbstract $proxifier
        )
    {
        $headers = HttpHeaders::createEmpty();
        $testCase->assertEquals($proxifier->proxifyHttpHeaders($headers), 0);
        $testCase->assertEquals($headers, HttpHeaders::createEmpty());
    }
    
    public static function
        proxifyHttpHeadersNothing(
            PHPUnit_Framework_TestCase $testCase,
            HttpHeadersProxifierAbstract $proxifier
        )
    {
        $headers = HttpHeaders::createFromString('DNT: 1');
        $headersClone = clone $headers;
        $testCase->assertEquals($proxifier->proxifyHttpHeaders($headers), 0);
        $testCase->assertEquals($headers, $headersClone);
    }
    
    public static function
        proxifyHttpHeadersOne(
            PHPUnit_Framework_TestCase $testCase,
            HttpHeadersProxifierAbstract $proxifier
        )
    {
        $headers = HttpHeaders::createFromString('Location: page.html');
        $headersClone = clone $headers;
        $testCase->assertEquals($proxifier->proxifyHttpHeaders($headers), 1);
        $testCase->assertNotEquals($headers, $headersClone);
    }
    
    public static function
        proxifyHttpHeaders(
            PHPUnit_Framework_TestCase $testCase,
            HttpHeadersProxifierAbstract $proxifier
        )
    {
        self::proxifyHttpHeadersEmpty($testCase, $proxifier);
        self::proxifyHttpHeadersNothing($testCase, $proxifier);
        self::proxifyHttpHeadersOne($testCase, $proxifier);
    }
    
    
    public function
        testProxifyHttpHeaders()
    {
        self::proxifyHttpHeaders($this, new HttpHeadersProxifierLocation());
    }
}
