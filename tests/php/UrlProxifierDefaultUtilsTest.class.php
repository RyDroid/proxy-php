<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class UrlProxifierDefaultUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testSetUrlFalse()
    {
        $this->assertFalse(UrlProxifierDefaultUtils::setUrl(''));
        $this->assertFalse(UrlProxifierDefaultUtils::setUrl(' '));
        $this->assertFalse(UrlProxifierDefaultUtils::setUrl('	'));
    }
    
    public function
        testSetUrlTrue()
    {
        $this->assertTrue(
            UrlProxifierDefaultUtils::setUrl('http://proxy.net')
        );
        $this->assertTrue(
            UrlProxifierDefaultUtils::setUrl('http://proxy.net/')
        );
        $this->assertTrue(
            UrlProxifierDefaultUtils::setUrl('https://proxy.net')
        );
        $this->assertTrue(
            UrlProxifierDefaultUtils::setUrl('https://proxy.net/')
        );
    }
    
    public function
        testGetDefaultUrl()
    {
        $this->assertNotEquals(
            trim(UrlProxifierDefaultUtils::getUrl()), ''
        );
    }
    
    public function
        testSetBaseProxifiedUrlFalse()
    {
        $this->assertFalse(
            UrlProxifierDefaultUtils::setBaseProxifiedUrl('')
        );
        $this->assertFalse(
            UrlProxifierDefaultUtils::setBaseProxifiedUrl(' ')
        );
        $this->assertFalse(
            UrlProxifierDefaultUtils::setBaseProxifiedUrl('	')
        );
    }
    
    public function
        testSetBaseProxifiedUrlTrue()
    {
        $this->assertTrue(
            UrlProxifierDefaultUtils::setBaseProxifiedUrl('http://proxy.net')
        );
        $this->assertTrue(
            UrlProxifierDefaultUtils::setBaseProxifiedUrl('http://proxy.net/')
        );
        $this->assertTrue(
            UrlProxifierDefaultUtils::setBaseProxifiedUrl('https://proxy.net')
        );
        $this->assertTrue(
            UrlProxifierDefaultUtils::setBaseProxifiedUrl('https://proxy.net/')
        );
    }
    
    public function
        testSetBaseProxifiedUrlWithoutFileFalse()
    {
        $this->assertFalse(
            UrlProxifierDefaultUtils::setBaseProxifiedUrlWithoutFile('')
        );
        $this->assertFalse(
            UrlProxifierDefaultUtils::setBaseProxifiedUrlWithoutFile(' ')
        );
        $this->assertFalse(
            UrlProxifierDefaultUtils::setBaseProxifiedUrlWithoutFile('	')
        );
    }
    
    public function
        testSetBaseProxifiedUrlWithoutFileTrue()
    {
        $this->assertTrue(
            UrlProxifierDefaultUtils::setBaseProxifiedUrlWithoutFile(
                'http://proxy.net'
            )
        );
        $this->assertTrue(
            UrlProxifierDefaultUtils::setBaseProxifiedUrlWithoutFile(
                'http://proxy.net/'
            )
        );
        $this->assertTrue(
            UrlProxifierDefaultUtils::setBaseProxifiedUrlWithoutFile(
                'https://proxy.net'
            )
        );
        $this->assertTrue(
            UrlProxifierDefaultUtils::setBaseProxifiedUrlWithoutFile(
                'https://proxy.net/'
            )
        );
        
        $this->assertTrue(
            UrlProxifierDefaultUtils::setBaseProxifiedUrlWithoutFile(
                'https://proxy.net/dir/file'
            )
        );
        $this->assertEquals(
            UrlProxifierDefaultUtils::getBaseProxifiedUrl(),
            'https://proxy.net/dir/'
        );
    }
    
    public function
        testGetBaseProxifiedUrlWithoutDirectory()
    {
        $this->assertTrue(
            UrlProxifierDefaultUtils::setBaseProxifiedUrlWithoutFile(
                'https://proxy.net/'
            )
        );
        $this->assertEquals(
            UrlProxifierDefaultUtils::getBaseProxifiedUrlWithoutDirectory(),
            'https://proxy.net/'
        );
        
        $this->assertTrue(
            UrlProxifierDefaultUtils::setBaseProxifiedUrlWithoutFile(
                'https://proxy.net/dir/'
            )
        );
        $this->assertEquals(
            UrlProxifierDefaultUtils::getBaseProxifiedUrlWithoutDirectory(),
            'https://proxy.net/'
        );
        
        $this->assertTrue(
            UrlProxifierDefaultUtils::setBaseProxifiedUrlWithoutFile(
                'https://proxy.net/dir/file'
            )
        );
        $this->assertEquals(
            UrlProxifierDefaultUtils::getBaseProxifiedUrlWithoutDirectory(),
            'https://proxy.net/'
        );
        
        $this->assertTrue(
            UrlProxifierDefaultUtils::setBaseProxifiedUrlWithoutFile(
                'https://proxy.net/dir1/dir2/file'
            )
        );
        $this->assertEquals(
            UrlProxifierDefaultUtils::getBaseProxifiedUrlWithoutDirectory(),
            'https://proxy.net/'
        );
    }
}
