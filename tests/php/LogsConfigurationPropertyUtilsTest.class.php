<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class LogsConfigurationPropertyUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testGetFileNameFromMapWithoutSection()
    {
        $this->assertEmpty(
            LogsConfigurationPropertyUtils::getFileNameFromMapWithoutSection(
                array()
            )
        );
        $this->assertEmpty(
            LogsConfigurationPropertyUtils::getFileNameFromMapWithoutSection(
                array('save' => 'false')
            )
        );
        $this->assertEmpty(
            LogsConfigurationPropertyUtils::getFileNameFromMapWithoutSection(
                array('save' => 'true')
            )
        );
        $this->assertEmpty(
            LogsConfigurationPropertyUtils::getFileNameFromMapWithoutSection(
                array('directory' => '/tmp/')
            )
        );
        
        $this->assertEquals(
            LogsConfigurationPropertyUtils::getFileNameFromMapWithoutSection(
                array('logs-file-name' => 'logs.txt')
            ),
            'logs.txt'
        );
        $this->assertEquals(
            LogsConfigurationPropertyUtils::getFileNameFromMapWithoutSection(
                array('logs-filename' => 'logs.txt')
            ),
            'logs.txt'
        );
    }
    
    public function
        testGetFileNameFromMapInSection()
    {
        $this->assertEmpty(
            LogsConfigurationPropertyUtils::getFileNameFromMapInSection(
                array()
            )
        );
        $this->assertEmpty(
            LogsConfigurationPropertyUtils::getFileNameFromMapInSection(
                array('save' => 'false')
            )
        );
        $this->assertEmpty(
            LogsConfigurationPropertyUtils::getFileNameFromMapInSection(
                array('save' => 'true')
            )
        );
        $this->assertEmpty(
            LogsConfigurationPropertyUtils::getFileNameFromMapInSection(
                array('directory' => '/tmp/')
            )
        );
        
        $this->assertEquals(
            LogsConfigurationPropertyUtils::getFileNameFromMapInSection(
                array('filename' => 'logs.txt')
            ),
            'logs.txt'
        );
        $this->assertEquals(
            LogsConfigurationPropertyUtils::getFileNameFromMapInSection(
                array('file-name' => 'logs.txt')
            ),
            'logs.txt'
        );
        $this->assertEquals(
            LogsConfigurationPropertyUtils::getFileNameFromMapInSection(
                array('logs-file-name' => 'logs.txt')
            ),
            'logs.txt'
        );
    }
}
