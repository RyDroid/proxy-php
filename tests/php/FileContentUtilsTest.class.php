<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class FileContentUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testSeemToBeHtmlFalse()
    {
        $this->assertFalse(FileContentUtils::seemToBeHtml(''));
        $this->assertFalse(FileContentUtils::seemToBeHtml(' '));
        $this->assertFalse(FileContentUtils::seemToBeHtml('no'));
        $this->assertFalse(FileContentUtils::seemToBeHtml('false'));
        $this->assertFalse(FileContentUtils::seemToBeHtml('true'));
        $this->assertFalse(FileContentUtils::seemToBeHtml('html'));
        $this->assertFalse(FileContentUtils::seemToBeHtml('<!--'));
        $this->assertFalse(FileContentUtils::seemToBeHtml('-->'));
    }
    
    public function
        testSeemToBeHtmlTrue()
    {
        $this->assertTrue(
            FileContentUtils::seemToBeHtml(
                '<html></html>'
            )
        );
        $this->assertTrue(
            FileContentUtils::seemToBeHtml(
                '<HTML></HTML>'
            )
        );
        $this->assertTrue(
            FileContentUtils::seemToBeHtml(
                '<!DOCTYPE html><html></html>'
            )
        );
    }
    
    public function
        testSeemToBeHtmlPartFalse()
    {
        $this->assertFalse(FileContentUtils::seemToBeHtmlPart(''));
        $this->assertFalse(FileContentUtils::seemToBeHtmlPart(' '));
        $this->assertFalse(FileContentUtils::seemToBeHtmlPart('no'));
        $this->assertFalse(FileContentUtils::seemToBeHtmlPart('false'));
        $this->assertFalse(FileContentUtils::seemToBeHtmlPart('true'));
        $this->assertFalse(FileContentUtils::seemToBeHtmlPart('html'));
        $this->assertFalse(FileContentUtils::seemToBeHtmlPart('<!--'));
        $this->assertFalse(FileContentUtils::seemToBeHtmlPart('-->'));
    }
    
    public function
        testSeemToBeHtmlPartTrue()
    {
        $this->assertTrue(
            FileContentUtils::seemToBeHtmlPart(
                '<html></html>'
            )
        );
        $this->assertTrue(
            FileContentUtils::seemToBeHtmlPart(
                '<HTML></HTML>'
            )
        );
        $this->assertTrue(
            FileContentUtils::seemToBeHtmlPart(
                '<a href="page.html">A link</a>'
            )
        );
        $this->assertTrue(
            FileContentUtils::seemToBeHtmlPart(
                '<p>AJAX is cool!</p>'
            )
        );
    }
}
