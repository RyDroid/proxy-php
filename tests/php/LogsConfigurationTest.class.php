<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class LogsConfigurationTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testConstructor()
    {
        $configuration = new LogsConfiguration(
            '', OptionnalBooleanUtils::createUndefined(),
            OptionnalBooleanUtils::createUndefined()
        );
        $this->assertTrue($configuration->hasToSave()->isUndefined());
        $this->assertFalse($configuration->hasToSave()->isDefined());
        $this->assertFalse($configuration->hasToSave()->isFalse());
        $this->assertFalse($configuration->hasToSave()->isTrue());
        
        $configuration = new LogsConfiguration(
            '', OptionnalBooleanUtils::createFalse(),
            OptionnalBooleanUtils::createUndefined()
        );
        $this->assertFalse($configuration->hasToSave()->isUndefined());
        $this->assertTrue($configuration->hasToSave()->isDefined());
        $this->assertTrue($configuration->hasToSave()->isFalse());
        $this->assertFalse($configuration->hasToSave()->isTrue());
        
        $configuration = new LogsConfiguration(
            '', OptionnalBooleanUtils::createTrue(),
            OptionnalBooleanUtils::createUndefined()
        );
        $this->assertFalse($configuration->hasToSave()->isUndefined());
        $this->assertTrue($configuration->hasToSave()->isDefined());
        $this->assertFalse($configuration->hasToSave()->isFalse());
        $this->assertTrue($configuration->hasToSave()->isTrue());
    }
    
    public function
        testSetSave()
    {
        $configuration = LogsConfigurationUtils::createNull();
        
        $configuration->setSave(OptionnalBooleanUtils::createUndefined());
        $this->assertTrue($configuration->hasToSave()->isUndefined());
        $this->assertFalse($configuration->hasToSave()->isDefined());
        $this->assertFalse($configuration->hasToSave()->isFalse());
        $this->assertFalse($configuration->hasToSave()->isTrue());
        
        $configuration->setSave(OptionnalBooleanUtils::createFalse());
        $this->assertFalse($configuration->hasToSave()->isUndefined());
        $this->assertTrue($configuration->hasToSave()->isDefined());
        $this->assertTrue($configuration->hasToSave()->isFalse());
        $this->assertFalse($configuration->hasToSave()->isTrue());
        
        $configuration->setSave(OptionnalBooleanUtils::createTrue());
        $this->assertFalse($configuration->hasToSave()->isUndefined());
        $this->assertTrue($configuration->hasToSave()->isDefined());
        $this->assertFalse($configuration->hasToSave()->isFalse());
        $this->assertTrue($configuration->hasToSave()->isTrue());
    }
    
    public function
        testSetShow()
    {
        $configuration = LogsConfigurationUtils::createNull();
        
        $configuration->setShow(OptionnalBooleanUtils::createUndefined());
        $this->assertTrue($configuration->canBeShown()->isUndefined());
        $this->assertFalse($configuration->canBeShown()->isDefined());
        $this->assertFalse($configuration->canBeShown()->isFalse());
        $this->assertFalse($configuration->canBeShown()->isTrue());
        
        $configuration->setShow(OptionnalBooleanUtils::createFalse());
        $this->assertFalse($configuration->canBeShown()->isUndefined());
        $this->assertTrue($configuration->canBeShown()->isDefined());
        $this->assertTrue($configuration->canBeShown()->isFalse());
        $this->assertFalse($configuration->canBeShown()->isTrue());
        
        $configuration->setShow(OptionnalBooleanUtils::createTrue());
        $this->assertFalse($configuration->canBeShown()->isUndefined());
        $this->assertTrue($configuration->canBeShown()->isDefined());
        $this->assertFalse($configuration->canBeShown()->isFalse());
        $this->assertTrue($configuration->canBeShown()->isTrue());
    }
}
