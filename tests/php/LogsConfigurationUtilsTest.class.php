<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class LogsConfigurationUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testCreateFromMapInSectionAndCheckSave()
    {
        $configuration = LogsConfigurationUtils::createFromMapInSection(
            array('save' => 'false')
        );
        $this->assertTrue($configuration->hasToSave()->isFalse());
        
        $configuration = LogsConfigurationUtils::createFromMapInSection(
            array('save' => 'true')
        );
        $this->assertTrue($configuration->hasToSave()->isTrue());
        
        $configuration = LogsConfigurationUtils::createFromMapInSection(
            array('save' => ' false ')
        );
        $this->assertTrue($configuration->hasToSave()->isFalse());
        
        $configuration = LogsConfigurationUtils::createFromMapInSection(
            array('save' => ' true ')
        );
        $this->assertTrue($configuration->hasToSave()->isTrue());
        
        $configuration = LogsConfigurationUtils::createFromMapInSection(
            array('has-to-save' => 'false')
        );
        $this->assertTrue($configuration->hasToSave()->isFalse());
        
        $configuration = LogsConfigurationUtils::createFromMapInSection(
            array('has-to-save' => 'true')
        );
        $this->assertTrue($configuration->hasToSave()->isTrue());
        
        $configuration = LogsConfigurationUtils::createFromMapInSection(
            array('save-logs' => 'false')
        );
        $this->assertTrue($configuration->hasToSave()->isFalse());
        
        $configuration = LogsConfigurationUtils::createFromMapInSection(
            array('save-logs' => 'true')
        );
        $this->assertTrue($configuration->hasToSave()->isTrue());
    }
    
    public function
        testCreateFromMapAndCheckSave()
    {
        $configuration = LogsConfigurationUtils::createFromMap(
            array('logs' => array('save' => 'false'))
        );
        $this->assertTrue($configuration->hasToSave()->isFalse());
        
        $configuration = LogsConfigurationUtils::createFromMap(
            array('logs' => array('save' => 'true'))
        );
        $this->assertTrue($configuration->hasToSave()->isTrue());
        
        $configuration = LogsConfigurationUtils::createFromMap(
            array('logs' => array('save' => ' false '))
        );
        $this->assertTrue($configuration->hasToSave()->isFalse());
        
        $configuration = LogsConfigurationUtils::createFromMap(
            array('logs' => array('save' => ' true '))
        );
        $this->assertTrue($configuration->hasToSave()->isTrue());
        
        $configuration = LogsConfigurationUtils::createFromMap(
            array('logs' => array('has-to-save' => 'false'))
        );
        $this->assertTrue($configuration->hasToSave()->isFalse());
        
        $configuration = LogsConfigurationUtils::createFromMap(
            array('logs' => array('has-to-save' => 'true'))
        );
        $this->assertTrue($configuration->hasToSave()->isTrue());
        
        $configuration = LogsConfigurationUtils::createFromMap(
            array('logs' => array('save-logs' => 'false'))
        );
        $this->assertTrue($configuration->hasToSave()->isFalse());
        
        $configuration = LogsConfigurationUtils::createFromMap(
            array('logs' => array('save-logs' => 'true'))
        );
        $this->assertTrue($configuration->hasToSave()->isTrue());
        
        $configuration = LogsConfigurationUtils::createFromMap(
            array('save-logs' => 'false')
        );
        $this->assertTrue($configuration->hasToSave()->isFalse());
        
        $configuration = LogsConfigurationUtils::createFromMap(
            array('save-logs' => 'true')
        );
        $this->assertTrue($configuration->hasToSave()->isTrue());
    }
    
    public function
        testCreateFromMapAndCheckFileName()
    {
        $configuration = LogsConfigurationUtils::createFromMap(
            array('logs' => array('file-name' => 'errors1.txt'))
        );
        $this->assertEquals($configuration->getBaseName(), 'errors1.txt');
        
        $configuration = LogsConfigurationUtils::createFromMap(
            array('logs-file-name' => 'errors2.txt')
        );
        $this->assertEquals($configuration->getBaseName(), 'errors2.txt');
        
        $configuration = LogsConfigurationUtils::createFromMap(
            array('logs-filename' => 'errors3.txt')
        );
        $this->assertEquals($configuration->getBaseName(), 'errors3.txt');
        
        $configuration = LogsConfigurationUtils::createFromMap(
            array('logfilename' => 'errors4.txt')
        );
        $this->assertEquals($configuration->getBaseName(), 'errors4.txt');
    }
    
    public function
        testCreateFromMapAndCheckDirectory()
    {
        $configuration = LogsConfigurationUtils::createFromMap(
            array(
                'logs' => array(
                    'file-name' => 'errors.txt',
                    'directory' => '/var/log/'
                )
            )
        );
        $this->assertEquals(
            rtrim($configuration->getDirectoryName(), '/'), '/var/log'
        );
        
        $configuration = LogsConfigurationUtils::createFromMap(
            array(
                'logs' => array(
                    'file-name' => 'errors.txt',
                    'directory' => '/var/log'
                )
            )
        );
        $this->assertEquals(
            rtrim($configuration->getDirectoryName(), '/'), '/var/log'
        );
        
        $configuration = LogsConfigurationUtils::createFromMap(
            array(
                'logs-file-name' => 'errors.txt',
                'logs-directory' => '/var/log'
            )
        );
        $this->assertEquals(
            rtrim($configuration->getDirectoryName(), '/'), '/var/log'
        );
        
        $configuration = LogsConfigurationUtils::createFromMap(
            array(
                'logs' => array(
                    'file-name'=> 'errors.txt',
                    'use-tmp-dir' => 'true'
                )
            )
        );
        $this->assertEquals(
            rtrim($configuration->getDirectoryName(), '/'),
            rtrim(FileSystemUtils::getSystemTemporaryDirectory(), '/')
        );
        
        $configuration = LogsConfigurationUtils::createFromMap(
            array(
                'logs' => array(
                    'filename'     => 'errors.txt',
                    'use-temp-dir' => 'true'
                )
            )
        );
        $this->assertEquals(
            rtrim($configuration->getDirectoryName(), '/'),
            rtrim(FileSystemUtils::getSystemTemporaryDirectory(), '/')
        );
        
        $configuration = LogsConfigurationUtils::createFromMap(
            array(
                'logs' => array(
                    'logs-filename'    => 'errors.txt',
                    'logs-use-tmp-dir' => 'true'
                )
            )
        );
        $this->assertEquals(
            rtrim($configuration->getDirectoryName(), '/'),
            rtrim(FileSystemUtils::getSystemTemporaryDirectory(), '/')
        );
        
        $configuration = LogsConfigurationUtils::createFromMap(
            array(
                'logs' => array(
                    'logs-filename'     => 'errors.txt',
                    'logs-use-temp-dir' => 'true'
                )
            )
        );
        $this->assertEquals(
            rtrim($configuration->getDirectoryName(), '/'),
            rtrim(FileSystemUtils::getSystemTemporaryDirectory(), '/')
        );
    }
    
    public function
        testCreateFromMapInSectionAndCheckShow()
    {
        $configuration = LogsConfigurationUtils::createFromMapInSection(
            array('show' => 'false')
        );
        $this->assertTrue($configuration->canBeShown()->isFalse());
        
        $configuration = LogsConfigurationUtils::createFromMapInSection(
            array('show' => 'true')
        );
        $this->assertTrue($configuration->canBeShown()->isTrue());
        
        $configuration = LogsConfigurationUtils::createFromMapInSection(
            array('show' => ' false ')
        );
        $this->assertTrue($configuration->canBeShown()->isFalse());
        
        $configuration = LogsConfigurationUtils::createFromMapInSection(
            array('show' => ' true ')
        );
        $this->assertTrue($configuration->canBeShown()->isTrue());
        
        $configuration = LogsConfigurationUtils::createFromMapInSection(
            array('can-be-shown' => 'false')
        );
        $this->assertTrue($configuration->canBeShown()->isFalse());
        
        $configuration = LogsConfigurationUtils::createFromMapInSection(
            array('can-be-shown' => 'true')
        );
        $this->assertTrue($configuration->canBeShown()->isTrue());
        
        $configuration = LogsConfigurationUtils::createFromMapInSection(
            array('show-logs' => 'false')
        );
        $this->assertTrue($configuration->canBeShown()->isFalse());
        
        $configuration = LogsConfigurationUtils::createFromMapInSection(
            array('show-logs' => 'true')
        );
        $this->assertTrue($configuration->canBeShown()->isTrue());
    }
}
