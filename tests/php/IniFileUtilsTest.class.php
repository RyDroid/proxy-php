<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class IniFileUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testParseStringWithoutSections()
    {
        $this->assertEquals(
            IniFileUtils::parseStringWithoutSections(''),
            array()
        );
        $this->assertEquals(
            IniFileUtils::parseStringWithoutSections(' '),
            array()
        );
        $this->assertEquals(
            IniFileUtils::parseStringWithoutSections("\t"),
            array()
        );
        $this->assertEquals(
            IniFileUtils::parseStringWithoutSections(PHP_EOL),
            array()
        );
        $this->assertEquals(
            IniFileUtils::parseStringWithoutSections(PHP_EOL . PHP_EOL),
            array()
        );
        
        $this->assertEquals(
            IniFileUtils::parseStringWithoutSections('key=value'),
            array('key' => 'value')
        );
        $this->assertEquals(
            IniFileUtils::parseStringWithoutSections('key="a value"'),
            array('key' => 'a value')
        );
        $this->assertEquals(
            IniFileUtils::parseStringWithoutSections('dir=/var/log'),
            array('dir' => '/var/log')
        );
        $this->assertEquals(
            IniFileUtils::parseStringWithoutSections('dir=/var/log/'),
            array('dir' => '/var/log/')
        );
        $this->assertEquals(
            IniFileUtils::parseStringWithoutSections(
                'param1=value1'. PHP_EOL .'param2=value2'
            ),
            array('param1' =>'value1', 'param2' => 'value2')
        );
    }
    
    public function
        testParseStringWithSections()
    {
        $this->assertEquals(
            IniFileUtils::parseStringWithSections(
                '[section]'. PHP_EOL .'key=value'
            ),
            array('section' => array('key' => 'value'))
        );
        $this->assertEquals(
            IniFileUtils::parseStringWithSections(
                PHP_EOL .'[section]'. PHP_EOL .'key=value'
            ),
            array('section' => array('key' => 'value'))
        );
        $this->assertEquals(
            IniFileUtils::parseStringWithSections(
                '[section]'. PHP_EOL .'key=value'. PHP_EOL
            ),
            array('section' => array('key' => 'value'))
        );
        $this->assertEquals(
            IniFileUtils::parseStringWithSections(
                PHP_EOL .'[section]'. PHP_EOL .'key=value'. PHP_EOL
            ),
            array('section' => array('key' => 'value'))
        );
        $this->assertEquals(
            IniFileUtils::parseStringWithSections(
                'save-cookies=no'
            ),
            array('save-cookies' => 'no')
        );
    }
    
    
    public function
        testArrayToString()
    {
        $this->assertEquals(
            IniFileUtils::arrayToString(array()),
            ''
        );
        $this->assertEquals(
            trim(IniFileUtils::arrayToString(array('key' => 'value'))),
            'key=value'
        );
        $this->assertEquals(
            trim(
                IniFileUtils::arrayToString(
                    array('key1' => 'value1', 'key2' => 'value2')
                )
            ),
            'key1=value1'. PHP_EOL .'key2=value2'
        );
        $this->assertEquals(
            trim(
                IniFileUtils::arrayToString(
                    array('section' => array('key' => 'value'))
                )
            ),
            '[section]'. PHP_EOL .'key=value'
        );
        $this->assertEquals(
            trim(
                IniFileUtils::arrayToString(
                    array(
                        'section' => array(
                            'key1' => 'value1', 'key2' => 'value2'
                        )
                    )
                )
            ),
            '[section]'. PHP_EOL .'key1=value1'. PHP_EOL .'key2=value2'
        );
    }
    
    
    /* TODO it "fails" but why?
    public function
        importExport(array $anArray)
    {
        $resultString = IniFileUtils::arrayToString($anArray);
        $resultArray  = IniFileUtils::parseStringWithSections($resultString);
        $this->assertEquals($anArray, $resultArray);
    }
    
    public function
        testImportExport()
    {
        $this->testImportExport(array('key' => 'value'));
        $this->testImportExport(
            array('param1' => 'value1', 'param2' => 'value2')
        );
    }*/
}
