<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class UrlProtocolStringUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testIsHttp()
    {
        $this->assertTrue(
            UrlProtocolStringUtils::isHttp('http://www.bortzmeyer.org/')
        );
        $this->assertTrue(
            UrlProtocolStringUtils::isHttp(
                'http://www.bortzmeyer.org/cryptage-n-existe-pas.html'
            )
        );
    }
    
    public function
        testIsNotHttp()
    {
        $this->assertFalse(
            UrlProtocolStringUtils::isHttp('https://www.gnu.org/')
        );
        $this->assertFalse(
            UrlProtocolStringUtils::isHttp(
                'https://www.gnu.org/philosophy/proprietary.html'
            )
        );
        $this->assertFalse(UrlProtocolStringUtils::isHttp(''));
        $this->assertFalse(UrlProtocolStringUtils::isHttp('/usr/bin/php'));
    }

    public function
        testIsHttps()
    {
        $this->assertTrue(
            UrlProtocolStringUtils::isHttps('https://www.gnu.org/')
        );
        $this->assertTrue(
            UrlProtocolStringUtils::isHttps(
                'https://www.gnu.org/philosophy/proprietary.html'
            )
        );
        
        $this->assertTrue(
            UrlProtocolStringUtils::isHttps('https://www.wikipedia.org/')
        );
        $this->assertTrue(
            UrlProtocolStringUtils::isHttps(
                'https://en.wikipedia.org/wiki/OpenJDK'
            )
        );
        $this->assertTrue(
            UrlProtocolStringUtils::isHttps(
                'https://en.wikipedia.org/wiki/OpenJDK#History'
            )
        );
    }

    public function
        testIsNotHttps()
    {
        $this->assertFalse(
            UrlProtocolStringUtils::isHttps('http://www.bortzmeyer.org/')
        );
        $this->assertFalse(
            UrlProtocolStringUtils::isHttps(
                'http://www.bortzmeyer.org/cryptage-n-existe-pas.html'
            )
        );
        $this->assertFalse(UrlProtocolStringUtils::isHttps(''));
        $this->assertFalse(UrlProtocolStringUtils::isHttps('/usr/bin/php'));
    }

    public function
        testIsWeb()
    {
        $this->assertTrue(
            UrlProtocolStringUtils::isWeb('http://www.bortzmeyer.org/')
        );
        $this->assertTrue(
            UrlProtocolStringUtils::isWeb(
                'http://www.bortzmeyer.org/cryptage-n-existe-pas.html'
            )
        );
        
        $this->assertTrue(
            UrlProtocolStringUtils::isWeb('https://www.gnu.org/')
        );
        $this->assertTrue(
            UrlProtocolStringUtils::isWeb(
                'https://www.gnu.org/philosophy/proprietary.html'
            )
        );

        $this->assertTrue(
            UrlProtocolStringUtils::isWeb('https://www.wikipedia.org/')
        );
        $this->assertTrue(
            UrlProtocolStringUtils::isWeb(
                'https://en.wikipedia.org/wiki/OpenJDK'
            )
        );
        $this->assertTrue(
            UrlProtocolStringUtils::isWeb(
                'https://en.wikipedia.org/wiki/OpenJDK#History'
            )
        );
    }
}
