<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HtmlStringUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testGetCharsetEmpty()
    {
        $this->assertEquals(HtmlStringUtils::getCharset(''), '');
        $this->assertEquals(
            HtmlStringUtils::getCharset(
                '<html><head>'.
                '<meta charset="" />'
                .'</head></html>'
            ),
            ''
        );
        $this->assertEquals(
            HtmlStringUtils::getCharset(
                '<html><head>'.
                '<meta charset=" " />'
                .'</head></html>'
            ),
            ''
        );
        $this->assertEquals(
            HtmlStringUtils::getCharset(
                '<html><head>'.
                '<meta http-equiv="Content-Type" '.
                'content="text/html;charset=" />'
                .'</head></html>'
            ),
            ''
        );
        $this->assertEquals(
            HtmlStringUtils::getCharset(
                '<html><head>'.
                '<meta http-equiv="Content-Type" '.
                'content="text/html;charset= " />'
                .'</head></html>'
            ),
            ''
        );
        $this->assertEquals(
            HtmlStringUtils::getCharset(
                '<html><head>'.
                '<meta http-equiv="Content-Type" '.
                'content="text/html;charset=	" />'
                .'</head></html>'
            ),
            ''
        );
    }

    public function
        testGetCharsetNotEmpty()
    {
        $this->assertEquals(
            HtmlStringUtils::getCharset(
                '<html><head>'.
                '<meta charset="utf-8" />'
                .'</head></html>'
            ),
            'utf-8'
        );
        $this->assertEquals(
            HtmlStringUtils::getCharset(
                '<html><head>'.
                '<meta charset="utf-8">'
                .'</head></html>'
            ),
            'utf-8'
        );
        $this->assertEquals(
            HtmlStringUtils::getCharset(
                '<html><head>'.
                '<meta http-equiv="Content-Type" '.
                'content="text/html;charset=ISO-8859-1" />'
                .'</head></html>'
            ),
            'ISO-8859-1'
        );
        $this->assertEquals(
            HtmlStringUtils::getCharset(
                '<html><head>'.
                '<meta http-equiv="Content-Type" '.
                'content="text/html;charset=ISO-8859-1">'
                .'</head></html>'
            ),
            'ISO-8859-1'
        );
        $this->assertEquals(
            HtmlStringUtils::getCharset(
                '<html><head>'.
                '<meta http-equiv="Content-Type" '.
                'content="text/html; charset=utf-8" />'
                .'</head></html>'
            ),
            'utf-8'
        );
        $this->assertEquals(
            HtmlStringUtils::getCharset(
                '<html><head>'.
                '<meta http-equiv="content-type" '.
                'content="text/html; charset=utf-8" />'
                .'</head></html>'
            ),
            'utf-8'
        );
        $this->assertEquals(
            HtmlStringUtils::getCharset(
                '<html><head>'.
                '<meta http-equiv="content-type" '.
                'content="text/html; charset=utf-8" />'
                .'</head><body>Test</body></html>'
            ),
            'utf-8'
        );
        $this->assertEquals(
            HtmlStringUtils::getCharset(
                '<html><head><title>Test</title>'.
                '<meta http-equiv="content-type" '.
                'content="text/html; charset=utf-8" />'
                .'</head><body>Test</body></html>'
            ),
            'utf-8'
        );
    }
}
