<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class GenericConfigurationUtilsTest
    extends PHPUnit_Framework_TestCase
{
    const IS_OK_TRUE  = [
        'true', ' true ', 'yes', 'ok', 'of-course', 'of course', 'oui'
    ];
    const IS_OK_FALSE = [
        'false', ' false ', 'truefalse', 'falsetrue', 'no', 'not ok', 'non'
    ];
    
    
    public function
        testIsOkAccordingToString()
    {
        foreach(self::IS_OK_TRUE as $aString)
        {
            $this->assertTrue(
                GenericConfigurationUtils::isOkAccordingToString($aString)
            );
        }
        foreach(self::IS_OK_FALSE as $aString)
        {
            $this->assertFalse(
                GenericConfigurationUtils::isOkAccordingToString($aString)
            );
        }
    }
    
    public function
        testIsBooleanExtendedAccordingToKeyOfMap()
    {
        $this->assertFalse(
            GenericConfigurationUtils::
            isBooleanExtendedAccordingToKeyOfMap(
                array(), ''
            )
        );
        
        $this->assertFalse(
            GenericConfigurationUtils::
            isBooleanExtendedAccordingToKeyOfMap(
                array('guess' => 'clever'), 'key'
            )
        );
        
        $this->assertFalse(
            GenericConfigurationUtils::
            isBooleanExtendedAccordingToKeyOfMap(
                array('key' => array('guess' => 'clever')), 'key'
            )
        );
        
        $this->assertFalse(
            GenericConfigurationUtils::
            isBooleanExtendedAccordingToKeyOfMap(
                array('key' => array('key' => 'true')), 'key'
            )
        );
        
        $this->assertTrue(
            GenericConfigurationUtils::
            isBooleanExtendedAccordingToKeyOfMap(
                array('bool' => 'true'), 'bool'
            )
        );
        
        $this->assertTrue(
            GenericConfigurationUtils::
            isBooleanExtendedAccordingToKeyOfMap(
                array('bool' => 'false'), 'bool'
            )
        );
    }
}
