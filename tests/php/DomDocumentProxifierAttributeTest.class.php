<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');
require_once('DomDocumentProxifierAttributeUtils.class.php');


final class DomDocumentProxifierAttributeTest
    extends PHPUnit_Framework_TestCase
{
    public static function
        proxifyDocumentWithEmptyAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier,
            string $html
        )
    {
        DomDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase, $proxifier, $html,
            new DomDocumentImportExport()
        );
    }
    
    public static function
        proxifyDocumentWithFilledAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier,
            string $html,
            string $xpathQuery,
            string $attribute,
            int $nbElements = 1
        )
    {
        DomDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase, $proxifier, $html, $xpathQuery, $attribute,
            new DomDocumentImportExport(),
            $nbElements
        );
    }
    
    
    public function
        testProxifyDocumentWithEmptyAttribute()
    {
        $proxifier = new DomDocumentProxifierAttribute('url', ['//link']);
        self::proxifyDocumentWithEmptyAttribute(
            $this, $proxifier,
            '<links><link url="" /></links>'
        );
    }
    
    public function
        testProxifyDocumentWithFilledAttribute()
    {
        $query     = '//link';
        $attribute = 'url';
        $proxifier = new DomDocumentProxifierAttribute($attribute, [$query]);
        
        self::proxifyDocumentWithFilledAttribute(
            $this, $proxifier,
            '<links><link url="an-other-document.xml" /></links>',
            $query, $attribute
        );
    }
}
