<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class DebugUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testOn()
    {
        DebugUtils::setOn();
        $this->assertTrue(DebugUtils::isOn());
    }
    
    public function
        testOff()
    {
        DebugUtils::setOff();
        $this->assertFalse(DebugUtils::isOn());
    }
}
