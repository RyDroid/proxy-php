<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class DomStringPreparatorEntityTest
    extends PHPUnit_Framework_TestCase
{
    const HTML_STRINGS_EMPTY_TAG = [
        '<html><body></body></html>',
        '<html><head></head><body></body></html>'
    ];
    
    const HTML_STRINGS_NO_EMPTY_TAG = [
        '<html><body>a</body></html>',
        '<html><body>Alice & Tom</body></html>',
        '<html><body>a&nbsp;a</body></html>',
        '<html><body>&middot; Item</body></html>',
        '<html><body>&bullet; Item</body></html>',
        '<html><body>Apr&egrave; lui</body></html>',
        '<html><body>Il a navigu&eacute;</body></html>',
        '<html><body>Au-del&agrave; des océans</body></html>',
        '<html><body>O&ugrave; est il ?</body></html>',
        '<html><body>Fran&ccedil;aise</body></html>',
        '<html><body>Il c&ocirc;toie des gens.</body></html>'
    ];
    
    
    private $preparator;
    
    
    public function
        __construct()
    {
        $this->preparator = new DomStringPreparatorEntity();
    }
    
    
    public function
        prepareAndUnprepare(string $domString) : string
    {
        return $this->preparator->unprepare(
            $this->preparator->prepare($domString)
        );
    }
    
    public function
        prepareAndUnprepareTestOne(string $domString)
    {
        $this->assertEquals(
            $domString, $this->prepareAndUnprepare($domString)
        );
    }
    
    public function
        prepareAndUnprepareTestMultiple(array $domStrings)
    {
        foreach($domStrings as $domString)
        {
            $this->prepareAndUnprepareTestOne($domString);
        }
    }
    
    public function
        testPrepareAndUnprepare()
    {
        $this->prepareAndUnprepareTestMultiple(self::HTML_STRINGS_EMPTY_TAG);
        $this->prepareAndUnprepareTestMultiple(self::HTML_STRINGS_NO_EMPTY_TAG);
    }
}
