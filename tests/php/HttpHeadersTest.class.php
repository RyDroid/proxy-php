<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HttpHeadersTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testIsEmpty()
    {
        $headers = HttpHeaders::createFromString('');
        $this->assertTrue($headers->isEmpty());
        
        $headers = HttpHeaders::createFromArrayOfLines(array());
        $this->assertTrue($headers->isEmpty());
    }
    
    public function
        testGetNumberOfLinesZero()
    {
        $headers = HttpHeaders::createFromString('');
        $this->assertEquals($headers->getNumberOfLines(), 0);
        
        $headers = HttpHeaders::createFromString(PHP_EOL);
        $this->assertEquals($headers->getNumberOfLines(), 0);
        
        $headers = HttpHeaders::createFromString(' '. PHP_EOL .' ');
        $this->assertEquals($headers->getNumberOfLines(), 0);
        
        $headers = HttpHeaders::createFromString(PHP_EOL . PHP_EOL);
        $this->assertEquals($headers->getNumberOfLines(), 0);
        
        $headers = HttpHeaders::createFromString(PHP_EOL .' '. PHP_EOL);
        $this->assertEquals($headers->getNumberOfLines(), 0);
        
        $headers = HttpHeaders::createFromString(' '. PHP_EOL .' '. PHP_EOL);
        $this->assertEquals($headers->getNumberOfLines(), 0);
        
        $headers = HttpHeaders::createFromString(PHP_EOL .' '. PHP_EOL .' ');
        $this->assertEquals($headers->getNumberOfLines(), 0);
        
        $headers = HttpHeaders::createFromArrayOfLines(array());
        $this->assertEquals($headers->getNumberOfLines(), 0);
        
        $headers = HttpHeaders::createFromArrayOfLines(array(''));
        $this->assertEquals($headers->getNumberOfLines(), 0);
        
        $headers = HttpHeaders::createFromArrayOfLines(array(' '));
        $this->assertEquals($headers->getNumberOfLines(), 0);
        
        $headers = HttpHeaders::createFromArrayOfLines(array(PHP_EOL));
        $this->assertEquals($headers->getNumberOfLines(), 0);
        
        $headers = HttpHeaders::createFromMap(array());
        $this->assertEquals($headers->getNumberOfLines(), 0);
        
        $headers = HttpHeaders::createFromMap(array('' => ''));
        $this->assertEquals($headers->getNumberOfLines(), 0);
    }
    
    public function
        testGetNumberOfLines()
    {
        $headers = HttpHeaders::createFromString('Connection: Keep-Alive');
        $this->assertEquals($headers->getNumberOfLines(), 1);
        
        $headers = HttpHeaders::createFromString(
            'Keep-Alive: timeout=5, max=100'
            . PHP_EOL .
            'Connection: Keep-Alive'
        );
        $this->assertEquals($headers->getNumberOfLines(), 2);
    }
    
    public function
        testCreateEmpty()
    {
        $headers = HttpHeaders::createEmpty();
        $this->assertTrue($headers->isEmpty());
    }
    
    public function
        testGetString()
    {
        $headers = HttpHeaders::createEmpty();
        $this->assertEquals($headers->getString(), '');
    }
    
    public function
        testGetArrayOfLines()
    {
        $headers = HttpHeaders::createEmpty();
        $this->assertEquals($headers->getArrayOfLines(), array());
    }
    
    
    public function
        testGetLineOfKey()
    {
        $headers = HttpHeaders::createEmpty();
        $this->assertEquals($headers->getLineOfKey(''), '');
        
        $headers = HttpHeaders::createEmpty();
        $this->assertEquals($headers->getLineOfKey('Keep-Alive'), '');
        
        $headers = HttpHeaders::createEmpty();
        $this->assertEquals($headers->getLineOfKey('Connection'), '');
        
        $headers = HttpHeaders::createFromString('Connection: Keep-Alive');
        $this->assertEquals($headers->getLineOfKey('Keep-Alive'), '');
        
        $headers = HttpHeaders::createFromString('Connection: Keep-Alive');
        $this->assertEquals(
            $headers->getLineOfKey('Connection'),
            'Connection: Keep-Alive'
        );
        
        $headers = HttpHeaders::createFromString(
            'Keep-Alive: timeout=5, max=100'
            . PHP_EOL .
            'Connection: Keep-Alive'
        );
        $this->assertEquals(
            $headers->getLineOfKey('Keep-Alive'),
            'Keep-Alive: timeout=5, max=100'
        );
        
        $headers = HttpHeaders::createFromString(
            'Keep-Alive: timeout=5, max=100'
            . PHP_EOL .
            'Connection: Keep-Alive'
        );
        $this->assertEquals(
            $headers->getLineOfKey('Connection'),
            'Connection: Keep-Alive'
        );
    }
    
    public function
        testGetPairOfKey()
    {
        $headers = HttpHeaders::createEmpty();
        $pair = $headers->getPairOfKey('');
        $this->assertTrue($pair->isEmpty());
        $pair = $headers->getPairOfKey('Keep-Alive');
        $this->assertTrue($pair->isEmpty());
        $pair = $headers->getPairOfKey('Connection');
        $this->assertTrue($pair->isEmpty());
        
        $headers = HttpHeaders::createFromString('Connection: Keep-Alive');
        $pair = $headers->getPairOfKey('');
        $this->assertTrue($pair->isEmpty());
        $pair = $headers->getPairOfKey('Keep-Alive');
        $this->assertTrue($pair->isEmpty());
        $pair = $headers->getPairOfKey('Connection');
        $this->assertFalse($pair->isEmpty());
        $this->assertEquals($pair->first,  'Connection');
        $this->assertEquals($pair->second, 'Keep-Alive');
    }
    
    public function
        testGetValueOfKey()
    {
        $headers = HttpHeaders::createEmpty();
        $this->assertEquals($headers->getValueOfKey(''), '');
        $this->assertEquals($headers->getValueOfKey('Keep-Alive'), '');
        $this->assertEquals($headers->getValueOfKey('Connection'), '');
        
        $headers = HttpHeaders::createFromString('Connection: Keep-Alive');
        $this->assertEquals($headers->getValueOfKey(''), '');
        $this->assertEquals($headers->getValueOfKey('Keep-Alive'), '');
        $this->assertEquals(
            $headers->getValueOfKey('Connection'), 'Keep-Alive'
        );
        
        $headers = HttpHeaders::createFromString(
            'Keep-Alive: timeout=5, max=100'
            . PHP_EOL .
            'Connection: Keep-Alive'
        );
        $this->assertEquals($headers->getValueOfKey(''), '');
        $this->assertEquals(
            $headers->getValueOfKey('Keep-Alive'), 'timeout=5, max=100'
        );
        $this->assertEquals(
            $headers->getValueOfKey('Connection'), 'Keep-Alive'
        );
    }
    
    public function
        testSetValueOfKey()
    {
        $headers = HttpHeaders::createEmpty();
        $this->assertFalse($headers->setValueOfKey('', ''));
        $this->assertFalse($headers->setValueOfKey('Connection', ''));
        $this->assertFalse($headers->setValueOfKey('', 'Keep-Alive'));
        $this->assertFalse($headers->setValueOfKey('Connection', 'Keep-Alive'));
        
        $headers = HttpHeaders::createFromString(
            'Location: https://windows.com'
        );
        $this->assertFalse($headers->setValueOfKey('', ''));
        $this->assertFalse($headers->setValueOfKey('Connection', ''));
        $this->assertFalse($headers->setValueOfKey('', 'Keep-Alive'));
        $this->assertFalse($headers->setValueOfKey('Connection', 'Keep-Alive'));
        $this->assertTrue(
            $headers->setValueOfKey('Location', 'https://www.gnu.org/')
        );
        $this->assertEquals(
            $headers->getValueOfKey('Location'), 'https://www.gnu.org/'
        );
    }
}
