<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HtmlDocumentProxifierLinkHrefTest
    extends PHPUnit_Framework_TestCase
{
    public static function
        proxifyDocumentWithEmptyAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><a href="">Fake link</a></body></html>'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><p><a href="">Fake link</a></p></body></html>'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><a href=" ">Fake link</a></body></html>'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><a href="   ">Fake link</a></body></html>'
        );
    }
    
    public static function
        proxifyDocumentWithFilledAttributeToKeep(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithAttributeToKeep(
            $testCase,
            $proxifier,
            '<html><body><a href="mailto:net-neutrality@fcc.org">'.
            'Give us a comment about Internet Neutrality'
            .'</a></body></html>',
            '/html/body//a[string(@href)]',
            'href'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithAttributeToKeep(
            $testCase,
            $proxifier,
            '<html><body><a href="tel:0615478233">'.
            'Please, don\'t call'.
            '</a></body></html>',
            '/html/body//a[string(@href)]',
            'href'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithAttributeToKeep(
            $testCase,
            $proxifier,
            '<html><body><a href="magnet:?'.
            'xt=urn:btih:c12fe1c06bba254a9dc9f519b335aa7c1367a88a&amp;dn">'.
            'A magnet link'.
            '</a></body></html>',
            '/html/body//a[string(@href)]',
            'href'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithAttributeToKeep(
            $testCase,
            $proxifier,
            '<html><body><a href="about:config">'.
            'Go to internal values of Firefox/Iceweasel/IceCat'.
            '</a></body></html>',
            '/html/body//a[string(@href)]',
            'href'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithAttributeToKeep(
            $testCase,
            $proxifier,
            '<html><body><a href="javascript:;">'.
            'A link'.
            '</a></body></html>',
            '/html/body//a[string(@href)]',
            'href'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithAttributeToKeep(
            $testCase,
            $proxifier,
            '<html><body><a href="javascript:doSomething();">'.
            'A link'.
            '</a></body></html>',
            '/html/body//a[string(@href)]',
            'href'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithAttributeToKeep(
            $testCase,
            $proxifier,
            '<html><body><a href="view-source:https://example.net/">'.
            'View the source of https://example.net/'.
            '</a></body></html>',
            '/html/body//a[string(@href)]',
            'href'
        );
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithAttributeToKeep(
            $testCase,
            $proxifier,
            '<html><body><a href="ftp://debian.hostiran.ir/debian-cd/">'.
            'Download a Debian CD'.
            '</a></body></html>',
            '/html/body//a[string(@href)]',
            'href'
        );
    }
    
    public static function
        proxifyDocumentWithFilledAttributeToChangeOne(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier,
            string $html
        )
    {
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            $html,
            '/html/body//a[string(@href)]',
            'href'
        );
    }
    
    public static function
        proxifyDocumentWithFilledAttributeToChange(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        self::proxifyDocumentWithFilledAttributeToChangeOne(
            $testCase,
            $proxifier,
            '<html><body><a href="page.html"></a></body></html>'
        );
        
        self::proxifyDocumentWithFilledAttributeToChangeOne(
            $testCase,
            $proxifier,
            '<html><body><a href="page.xhtml"></a></body></html>'
        );
        
        self::proxifyDocumentWithFilledAttributeToChangeOne(
            $testCase,
            $proxifier,
            '<html><body><a href="page.htm"></a></body></html>'
        );
        
        self::proxifyDocumentWithFilledAttributeToChangeOne(
            $testCase,
            $proxifier,
            '<html><body><a href="page.php"></a></body></html>'
        );
        
        self::proxifyDocumentWithFilledAttributeToChangeOne(
            $testCase,
            $proxifier,
            '<html><body><a href="image.png">A PNG image</a></body></html>'
        );
        
        self::proxifyDocumentWithFilledAttributeToChangeOne(
            $testCase,
            $proxifier,
            '<html><body><p><a href="page.html"></a></p></body></html>'
        );
        
        self::proxifyDocumentWithFilledAttributeToChangeOne(
            $testCase,
            $proxifier,
            '<html><body>'.
            '<div><p><a href="page.html"></a></p></div>'
            .'</body></html>'
        );
        
        self::proxifyDocumentWithFilledAttributeToChangeOne(
            $testCase,
            $proxifier,
            '<html><body>'.
            '<div><p><a href="/page.html"></a></p></div>'
            .'</body></html>'
        );
        
        self::proxifyDocumentWithFilledAttributeToChangeOne(
            $testCase,
            $proxifier,
            '<html><body>'.
            '<div><p><a href="dir/page.html"></a></p></div>'
            .'</body></html>'
        );
        
        self::proxifyDocumentWithFilledAttributeToChangeOne(
            $testCase,
            $proxifier,
            '<html><body>'.
            '<p><a href="https://stallman.org/">Stallman</a> is great!</p>'
            .'</body></html>'
        );
        
        self::proxifyDocumentWithFilledAttributeToChangeOne(
            $testCase,
            $proxifier,
            '<html><body>'. PHP_EOL .
            '<a href="page.html">A not closed link tag'
            . PHP_EOL .'</body></html>'
        );
    }
    
    public static function
        proxifyDocumentWithFilledAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        self::proxifyDocumentWithFilledAttributeToKeep(
            $testCase, $proxifier
        );
        self::proxifyDocumentWithFilledAttributeToChange(
            $testCase, $proxifier
        );
    }
    
    
    private $proxifier;
    
    
    public function
        __construct()
    {
        $this->proxifier = new HtmlDocumentProxifierLinkHref();
    }
    
    
    public function
        testXPathQueries()
    {
        foreach(HtmlDocumentProxifierLinkHref::XPATH_QUERIES as $query)
        {
            $this->assertFalse(empty(trim($query)));
        }
    }
    
    public function
        testProxifyDocumentWithEmptyAttribute()
    {
        self::proxifyDocumentWithEmptyAttribute(
            $this, $this->proxifier
        );
    }
    
    public function
        testProxifyDocumentWithFilledAttribute()
    {
        self::proxifyDocumentWithFilledAttribute(
            $this, $this->proxifier
        );
    }
}
