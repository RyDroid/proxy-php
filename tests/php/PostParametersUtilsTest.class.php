<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class PostParametersUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testMapToString()
    {
        $this->assertEquals(
            PostParametersUtils::mapToString(
                array('key' => 'value')
            ),
            'key=value'
        );
        
        $this->assertEquals(
            PostParametersUtils::mapToString(
                array('key1' => 'value1', 'key2' => 'value2')
            ),
            'key1=value1&key2=value2'
        );
    }
}
