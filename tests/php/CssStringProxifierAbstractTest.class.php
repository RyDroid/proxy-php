<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');
require_once('UrlProxifierGenericTest.class.php');


abstract class CssStringProxifierAbstractTest
    extends UrlProxifierGenericTest
{
    public function
        __construct(CssStringProxifierAbstract $proxifier)
    {
        parent::__construct($proxifier);
    }
    
    
    private function
        proxifyLineNo(string $css)
    {
        $proxifier = $this->getProxifier();
        $this->assertEquals($proxifier->proxifyLine($css), $css);
    }
    
    public function
        testProxifyLineNo()
    {
        $this->proxifyLineNo('');
        $this->proxifyLineNo(' ');
        $this->proxifyLineNo('	');
        $this->proxifyLineNo('/**/');
        $this->proxifyLineNo('/* */');
        $this->proxifyLineNo('background:url();');
        $this->proxifyLineNo('background: url();');
        $this->proxifyLineNo('background : url();');
        $this->proxifyLineNo('background : url( );');
        $this->proxifyLineNo('background : url(	);');
        $this->proxifyLineNo('background : url(");');
        $this->proxifyLineNo('background : url( ");');
        $this->proxifyLineNo('background : url(" );');
        $this->proxifyLineNo('background : url( " );');
        $this->proxifyLineNo('content: "url();";');
        $this->proxifyLineNo('background:url();background:url();');
        $this->proxifyLineNo('background:url(); background:url();');
        $this->proxifyLineNo('@charset "UTF-8";');
        $this->proxifyLineNo('@charset "iso-8859-15";');
        $this->proxifyLineNo('@import url(data:image/png;base64,ABCDEFGH);');
    }
}
