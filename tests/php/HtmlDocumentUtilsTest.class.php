<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HtmlDocumentUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testGetCharset()
    {
        $document = new DOMDocument();
        
        $this->assertEmpty(HtmlDocumentUtils::getCharset($document));
        
        $this->assertTrue(
            $document->loadXML(
                '<html><head></head><body></body></html>'
            )
        );
        $this->assertEmpty(HtmlDocumentUtils::getCharset($document));
        
        $this->assertTrue(
            $document->loadXML(
                '<html><head></head><body>charset=utf-8</body></html>'
            )
        );
        $this->assertEmpty(HtmlDocumentUtils::getCharset($document));
        
        $this->assertTrue(
            $document->loadXML(
                '<html><head><meta charset="utf-8" /></head></html>'
            )
        );
        $this->assertEquals(HtmlDocumentUtils::getCharset($document), 'utf-8');
        
        $this->assertTrue(
            $document->loadXML(
                '<html><head>'.
                '<meta http-equiv="Content-Type" '.
                'content="text/html; charset=utf-8" />'
                .'</head><body></body></html>'
            )
        );
        $this->assertEquals(HtmlDocumentUtils::getCharset($document), 'utf-8');
    }
    
    public function
        testHasCharset()
    {
        $document = new DOMDocument();
        
        $this->assertFalse(HtmlDocumentUtils::hasCharset($document));
        
        $this->assertTrue(
            $document->loadXML('<html><head></head><body></body></html>')
        );
        $this->assertFalse(HtmlDocumentUtils::hasCharset($document));
        
        $this->assertTrue(
            $document->loadXML(
                '<html><head></head><body>charset=utf-8</body></html>'
            )
        );
        $this->assertFalse(HtmlDocumentUtils::hasCharset($document));
        
        $this->assertTrue(
            $document->loadXML(
                '<html><head><meta charset="utf-8" /></head></html>'
            )
        );
        $this->assertTrue(HtmlDocumentUtils::hasCharset($document));
        
        $this->assertTrue(
            $document->loadXML(
                '<html><head>'.
                '<meta http-equiv="Content-Type" '.
                'content="text/html; charset=utf-8" />'
                .'</head><body></body></html>'
            )
        );
        $this->assertTrue(HtmlDocumentUtils::hasCharset($document));
    }
    
    
    public function
        testHasElementWithIdentifier()
    {
        $document = new DOMDocument();
        
        $this->assertFalse(
            HtmlDocumentUtils::hasElementWithIdentifier($document, '')
        );
        $this->assertFalse(
            HtmlDocumentUtils::hasElementWithIdentifier($document, 'id')
        );
        
        $this->assertTrue(
            $document->loadHTML(
                '<html><body><div id="main"></div></body></html>'
            )
        );
        $this->assertFalse(
            HtmlDocumentUtils::hasElementWithIdentifier($document, '')
        );
        $this->assertFalse(
            HtmlDocumentUtils::hasElementWithIdentifier($document, 'id')
        );
        $this->assertTrue(
            HtmlDocumentUtils::hasElementWithIdentifier($document, 'main')
        );
        
        $this->assertTrue(
            $document->loadHTML(
                '<html><body><div id="main">id=value</div></body></html>'
            )
        );
        $this->assertFalse(
            HtmlDocumentUtils::hasElementWithIdentifier($document, '')
        );
        $this->assertFalse(
            HtmlDocumentUtils::hasElementWithIdentifier($document, 'id')
        );
        $this->assertFalse(
            HtmlDocumentUtils::hasElementWithIdentifier($document, 'value')
        );
        $this->assertTrue(
            HtmlDocumentUtils::hasElementWithIdentifier($document, 'main')
        );
    }
    
    public function
        testGetFirstNotUsedIdentifier()
    {
        $document = new DOMDocument();
        
        $this->assertTrue(
            $document->loadHTML(
                '<html><body><div id="main"></div></body></html>'
            )
        );
        $this->assertEquals(
            HtmlDocumentUtils::getFirstNotUsedIdentifier(
                $document, array()
            ),
            ''
        );
        $this->assertEquals(
            HtmlDocumentUtils::getFirstNotUsedIdentifier(
                $document, array('main')
            ),
            ''
        );
        $this->assertEquals(
            HtmlDocumentUtils::getFirstNotUsedIdentifier(
                $document, array('void', 'main')
            ),
            'void'
        );
        $this->assertEquals(
            HtmlDocumentUtils::getFirstNotUsedIdentifier(
                $document, array('main', 'void')
            ),
            'void'
        );
        $this->assertEquals(
            HtmlDocumentUtils::getFirstNotUsedIdentifier(
                $document, array('void', 'main', 'null')
            ),
            'void'
        );
    }
}
