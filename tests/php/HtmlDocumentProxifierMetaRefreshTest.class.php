<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');
require_once('DomDocumentProxifierWithXPathAbstractTest.class.php');


final class HtmlDocumentProxifierMetaRefreshTest
    extends DomDocumentProxifierWithXPathAbstractTest
{
    public static function
        proxifyDocWithEmptyAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><head>'.
            '<meta http-equiv="" content="" />'
            .'</head></html>'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><head>'.
            '<meta http-equiv="" content="0" />'
            .'</head></html>'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><head>'.
            '<meta http-equiv="refresh" content="" />'
            .'</head></html>'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><head>'.
            '<meta http-equiv="refresh" content="0" />'
            .'</head></html>'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><head>'.
            '<meta http-equiv="refresh" content="0;url=" />'
            .'</head></html>'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><head>'.
            '<meta http-equiv="refresh" content="0; url=" />'
            .'</head></html>'
        );
    }
    
    private static function
        proxifyDocWithFilledAttributeWithHtmlBeginEnd(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier,
            string $htmlBegin,
            string $htmlEnd
        )
    {
        $urlBefore  = 'https://example.com/';
        $urlEncoded = UrlProxifierCodecUtils::encodeUrl($urlBefore);
        $urlAfter   = $proxifier->getUrl() .'?url='. $urlEncoded;
        $htmlAttrBefore = 'content="0; url='. $urlBefore .'"';
        $htmlAttrAfter  = 'content="0; url='. $urlAfter .'"';
        
        $htmlBefore = $htmlBegin . $htmlAttrBefore . $htmlEnd;
        $htmlAfter  = $htmlBegin . $htmlAttrAfter  . $htmlEnd;
        
        $document = new DOMDocument();
        $testCase->assertTrue($document->loadXML($htmlBefore));
        $testCase->assertTrue($document->loadXML($htmlAfter));
        $htmlAfterParsed = trim($document->saveXML($document->documentElement));
        $testCase->assertEquals($proxifier->proxifyDocument($document), 1);
        $htmlResult = trim($document->saveXML($document->documentElement));
        $testCase->assertEquals($htmlAfterParsed, $htmlResult);
    }
    
    public static function
        proxifyDocWithFilledAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        self::proxifyDocWithFilledAttributeWithHtmlBeginEnd(
            $testCase, $proxifier,
            '<html><head><meta http-equiv="refresh" ',
            ' /></head></html>'
        );
        self::proxifyDocWithFilledAttributeWithHtmlBeginEnd(
            $testCase, $proxifier,
            '<html><head><noscript><meta http-equiv="refresh" ',
            ' /></noscript></head></html>'
        );
    }
    
    
    public function
        __construct()
    {
        parent::__construct(new HtmlDocumentProxifierMetaRefresh());
    }
    
    
    public function
        testGetNoElement()
    {
        $this->getNoElementFromHtml('<html></html>');
        $this->getNoElementFromHtml('<html><head></head></html>');
        $this->getNoElementFromHtml(
            '<html><head><meta charset="UTF-8" /></head></html>'
        );
        $this->getNoElementFromHtml(
            '<html><head>'.
            '<meta http-equiv="" content="" />'
            .'</head></html>'
        );
        $this->getNoElementFromHtml(
            '<html><head>'.
            '<meta http-equiv="" content="5" />'
            .'</head></html>'
        );
        $this->getNoElementFromHtml(
            '<html><head>'.
            '<meta http-equiv="refresh" content="" />'
            .'</head></html>'
        );
        $this->getNoElementFromHtml(
            '<html><head>'.
            '<meta http-equiv="refresh" content="5" />'
            .'</head></html>'
        );
        $this->getNoElementFromHtml(
            '<html><head>'.
            '<meta http-equiv="refresh" content="5;" />'
            .'</head></html>'
        );
        $this->getNoElementFromHtml(
            '<html><head>'.
            '<meta http-equiv="refresh" content="5; " />'
            .'</head></html>'
        );
    }
    
    public function
        testGetOneElement()
    {
        $this->getOneElementFromHtml(
            '<html><head>'.
            '<meta http-equiv="refresh" content="0; '.
            'url=http://example.com/" />'
            .'</head></html>'
        );
        $this->getOneElementFromHtml(
            '<html><head>'.
            '<meta http-equiv="refresh" content="0; '.
            'url=https://example.com/" />'
            .'</head></html>'
        );
        $this->getOneElementFromHtml(
            '<html><head>'.
            '<meta http-equiv="refresh" content="5; '.
            'url=https://example.com/" />'
            .'</head></html>'
        );
    }
    
    
    public function
        testProxifyDocumentWithEmptyAttribute()
    {
        self::proxifyDocWithEmptyAttribute(
            $this, $this->getProxifier()
        );
    }
    
    public function
        testProxifyDocumentWithFilledAttribute()
    {
        self::proxifyDocWithFilledAttribute(
            $this, $this->getProxifier()
        );
    }
}
