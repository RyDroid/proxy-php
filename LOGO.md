# Logo

It is the combination of:

- The logo of [CLIF](https://clif.ow2.io/)
- [The web browser logo from GNOME](https://commons.wikimedia.org/wiki/File:Gnome-web-browser.svg),
  under [GNU LGPL v3 or Creative Commons BY-SA 3.0 US](https://git.gnome.org/browse/adwaita-icon-theme/tree/COPYING)
- [elePHPant](https://en.wikipedia.org/wiki/PHP#Mascot), the mascot of PHP,
  under [GNU GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)+
